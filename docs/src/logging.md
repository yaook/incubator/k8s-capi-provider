# Logging

The Yaook CAPI provider (by default) distinguish logs produced by `k8s-capi-provider` handlers (`kopf` based) from logs
that are produced by underlying `yaook/k8s` scripts.

![](images/logging.png)

## k8s-capi-provider handlers logging

Any `k8s-capi-provider` handler uses `kopf` resource logger, with the messages prefixed with the resource’s namespace/name.
The same `kopf` resource logger is also used for Kubernetes as well as OpenStack client calls and for any messages produced by `k8s-capi-provider` software itself.
The default log level is INFO. If you want to see also DEBUG messages that are produced by `kopf` framework
use the `--verbose` options in `kopf`, see [docs][docs-kopf-verbose] for details.
The `kopf` full-log format is used by default and could be modified by `--log-format` option, see [docs][docs-kopf-log]
for details.

`kopf` full-log format:
```
[{time:YYYY-MM-DD HH:mm:ss,SSS}] {source} [{level}] [{resource-namespace}/{resource-name}] {message}
```

## k8s-capi-provider yaook/k8s scripts logging

Logs produced by underlying `yaook/k8s` scripts use (by default) [loguru][loguru] loggers.
These loggers are registered per cluster. Stdout and stderr logs from scripts are streamed to
log files (info.log, warning.log, error.log) dedicated for the workload cluster. Stderr of `yaook/k8s` scripts is also streamed to the `k8s-capi-provider`
stderr, hence stderr logs are visible also in `k8s-capi-provider` console. The `kopf` full-log format is used for console stderr
logs. Loguru features such as log rotation and retention are used with reasonable defaults.

See :class:`src.settings.ClusterLogging` for `yaook/k8s` scripts logging options and
`k8s-capi-provider` [settings management docs](settings_management.md) for details how to override default settings.

Watch cluster controller logs (cluster ctrl handles terraform and bootstrap layer) with:
```shell
yaookctl logs ctrl cluster -f
```

Watch service controller logs (service ctrl handles service layer) with:
```shell
yaookctl logs ctrl service -f
```

Watch progress of cluster creation (terraform, bootstrap layers) with:
```shell
yaookctl logs cluster example-cluster -f
```

Alternatively, watch service deployment layer logs with:
```shell
yaookctl logs cluster example-cluster -s service -f
```

**Note:**
  - `k8s-capi-provider` consists of two controllers:
    - Cluster controller. It is responsible for infrastructure and bootstrap layers.
    - Service controller. It is responsible for service layers.


See the full list of related `yaookctl logs` options:

```shell
yaookctl logs --help
```

<!-- References -->

[docs-kopf-verbose]: https://kopf.readthedocs.io/en/stable/cli/#cmdoption-verbose
[docs-kopf-log]: https://kopf.readthedocs.io/en/stable/cli/#cmdoption-log-format
[loguru]: https://github.com/Delgan/loguru

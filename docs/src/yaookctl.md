# yaookctl

> ***Note:*** This is a proprietary version of the `yaookctl` tool developed *only* for the `k8s-capi-provider` incubator project. Please note
> that the *official* yaook CLI tool [yaookctl][yaook-yaookctl] is the right choice for you if you want to interact with
> [yaook][yaook] (non incubator) components. This miss-naming will be resolved soon (-:

## Installation

As `yaookctl` is not yet on the Python Package Index (PyPI), you should install it from the source.
Installing it into a [Python3 virtualenv][virtualenv] is recommended.

```shell
python3 -m pip install "git+https://gitlab.com/yaook/incubator/k8s-capi-provider.git@devel#egg=yaookctl&subdirectory=yaookctl"
```

or clone the `k8s-capi-provider` repository and install it as follows:

```shell
git clone https://gitlab.com/yaook/incubator/k8s-capi-provider.git
cd k8s-capi-provider
pip3 install yaookctl/
```

## Shell completion

To enable shell completion, add the following to your shell configuration:

* For bash: `eval "$(_YAOOKCTL_COMPLETE=bash_source yaookctl)"`
* For zsh: `eval "$(_YAOOKCTL_COMPLETE=zsh_source yaookctl)"`
* For fish: `eval "$(_YAOOKCTL_COMPLETE=fish_source yaookctl)"`

See also the upstream documentation on [enabling shell completion][competition].

## Usage

This section contains some handy examples of how to interact with `yaookctl`. For the full list of commands
please visit the [yaookctl documentation](yaookctl_cli.md) and explore current `yaookctl` commands and options.

> ***Note:*** yaookctl tool (mainly) interacts with the management k8s cluster where the
> k8s-capi-provider operates. You need to have a valid `KUBECONFIG` set to use
> yaookctl commands.

Cases:
- [Obtain controller logs](#obtain-controller-logs)
- [Obtain workload cluster LCM logs](#obtain-workload-cluster-lcm-logs)
- [Interact with the workload cluster](#interact-with-the-workload-cluster)
- [Migrate workload cluster from yaook/k8s LCM to yaook/incubator/k8s-capi-provider LCM](#migrate-workload-cluster-from-yaookk8s-lcm-to-yaookincubatork8s-capi-provider-lcm)
- [Migrate workload cluster from yaook/incubator/k8s-capi-provider LCM to yaook/k8s LCM](#migrate-workload-cluster-from-yaookincubatork8s-capi-provider-lcm-to-yaookk8s-lcm)

### Obtain controller logs

Watch the cluster controller logs (cluster controller handles terraform and bootstrap layer) with:
```shell
yaookctl logs ctrl cluster --follow
```

Watch service controller logs (service controller handles service layer) with:
```shell
yaookctl logs ctrl service --follow
```

See [yaookctl logs ctrl](yaookctl_cli.md#yaookctl-logs-ctrl) docs for details.

### Obtain workload cluster LCM logs

Watch workload cluster LCM logs from *cluster* controller POD (terraform, bootstrap layers) with:
```shell
yaookctl logs cluster <cluster-name> --follow
```

Watch workload cluster LCM logs from *service* controller POD (service layer) with:
```shell
yaookctl logs cluster <cluster-name> --source service --follow
```

See [yaookctl logs cluster](yaookctl_cli.md#yaookctl-logs-cluster) docs for details.

### Interact with the workload cluster

Visit [tutorial](tutorial.md#interact-with-workload-cluster) page.

See [yaookctl get](yaookctl_cli.md#yaookctl-get) docs for details.

### Migrate workload cluster from yaook/k8s LCM to yaook/incubator/k8s-capi-provider LCM

> ***Note:*** Please note that the cluster's LCM migration from yaook/k8s to yaook/incubator/k8s-capi-provider is a highly experimental feature.

The workload cluster state files could be extracted from the `yaook/k8s` LCM and used in
the `k8s-capi-provider` LCM. It means that the workload cluster could switch its
LCM tool if needed.

The `yaookctl inject` command creates necessary cluster custom resources (CRs) and secrets
in the management cluster where the `k8s-capi-provider` operates. CRs and secrets
are made based on the workload cluster's state files located in the yaook/k8s dir.

Then the `k8s-capi-provider` LCM can take over the workload cluster.

The `k8s-capi-provider` creates its own ssh/wireguard/gpg identity in a regular
operation (see the [docs](introduction.md#k8s-capi-provider-layer)).
In this specific `inject` case the `k8s-capi-provider` should use the
identities used in the yaook/k8s LCM. Therefore, the `k8s-capi-provider`
identity should not be created by `k8s-capi-provider` when
the cluster is injected from yaook/k8s LCM.

The flag which manages the above is called `MANAGED_PROVIDER_IDENTITY` and the
`yaookctl inject` command sets it to False, i.e. disables
the provider identity management for that particular workload cluster.

Ensure that `k8s-capi-provider` CRDs have been deployed in the management cluster.

> ***Note:*** If the OpenStack authentication method of your yaook/k8s cluster is `v3password` the
> `OS_PASSWORD` env. variable should be defined in the *openrc.sh file.

> ***Attention:*** Currently, the k8s-capi-provider can not inject and processed the yaook/k8s cluster's SSH private key
> which is protected by a passphrase. If your yaook/k8s cluster uses SSH private key protected by passphrase, please check the
> status of the following issue: https://gitlab.com/yaook/incubator/k8s-capi-provider/-/issues/71

```shell
yaookctl inject <cluster-name> \
  --yk8s <yaook/k8s workload cluster directory> \
  --ssh-private-key <path to the SSH private key file used for cluster LCM> \
  --wg-key <path to the wireguard private key file used for cluster LCM> \
  --gpg-home <path to the gpg home directory used for cluster LCM>
```

> ***Note:*** The `yaookctl inject` command should not modify your cluster in any way, but keep in mind that the migration
> is an experimental feature.
>
See [yaookctl inject](yaookctl_cli.md#yaookctl-inject) docs for details.

### Migrate workload cluster from yaook/incubator/k8s-capi-provider LCM to yaook/k8s LCM

> ***Note:*** Please note that the cluster's LCM migration from yaook/incubator/k8s-capi-provider to yaook/k8s is a highly experimental feature.

The workload cluster state files could be extracted from the `k8s-capi-provider` LCM and used in
the `yaook/k8s` LCM. It means that the workload cluster could switch its
LCM tool if needed.

`yaookctl get cluster` command downloads workload cluster state from the
`k8s-capi-provider` persistent storage. The cluster state files could be then
used in `yaook/k8s` LCM.

Follow the below instructions and migrate your cluster:
- Get all workload cluster files from the `k8s-capi-provider` LCM and save them to the $output_dir
  ```shell
  yaookctl get cluster <cluster name> --output $output_dir
  ```
- Initialize the directory as your `yaook/k8s` cluster repository:
  ```shell
  git init $output_dir
  ```
- Clone the `yaook/k8s` repository to a location outside your cluster repository.
  Also, checkout the `yaook/k8s` version used by the `k8s-capi-provider` LCM.
  To check what `yaook/k8s` version is used by `k8s-capi-provider`, visit the
  `k8s-capi-provider` [settings][provider-yk8s-version].

  _You may omit the specific version checkout and simply use the latest `yaook/k8s` version.
  In that case, keep in mind that some incompatibility may occur._

  ```shell
  pushd $somewhere_else
  git clone --no-checkout https://gitlab.com/yaook/k8s.git
  cd k8s
  git checkout fd54cd828d6ea65f16a2a67f0aab9eecb2cf27a5
  export MANAGED_K8S_GIT=${somewhere_else}/k8s
  ```
- Initialize your `yaook/k8s` cluster repository:
  ```shell
  cd $output_dir
  direnv allow
  $somewhere_else/k8s/actions/init.sh
  ```
- Make sure pip is up-to-date and install the python package dependencies:
  ```shell
  python3 -m pip install -U pip
  python3 -m pip install -r managed-k8s/requirements.txt
  ```
- Reconcile your cluster by executing the apply.sh script:
  ```shell
  ./managed-k8s/actions/apply.sh
  ```

> ***Note:*** Executing the apply.sh script should not modify your cluster in any way, but keep in mind that the migration
> is an experimental feature.

See [yaookctl get cluster](yaookctl_cli.md#yaookctl-get-cluster) docs for details.

<!-- References -->

[virtualenv]: https://virtualenv.pypa.io/en/stable
[competition]: https://click.palletsprojects.com/en/8.1.x/shell-completion/#enabling-completion
[yaook]: https://gitlab.com/yaook
[yaook-yaookctl]: https://gitlab.com/yaook/yaookctl
[provider-yk8s-version]: https://gitlab.com/yaook/incubator/k8s-capi-provider/-/blob/devel/k8s_capi_provider/src/settings.py#L97

# YaookCluster LiveCycle

The Yaook CAPI **Cluster Controller** is responsible for managing the lifecycle of a YaookCluster resource,
including its state transitions. This is the only controller that should be writing updates to YaookCluster resource.

## States and transitions

See below the list of possible resource states and the diagram of state transitions.

- Pending
  - YaookCluster resource initial state
  _Note:_ The CRD does not support default values for status subresource. YaookCluster resource state `Pending` is set only when
  the YaookCluster resource waits for its dependencies like OpenStack RC secret.
- Creating
  - `Creating` is the first state a YaookCluster resource is assigned by the Cluster controller after being created
- Updating
  - `Updating` is the YaookCluster resource state when an update request has been sent to the Cluster controller
  - Update could be postponed when the resource has `Reconciling` state. Update will be triggered again after defined delay (defaults to 120s)
- Reconciling
  - `Reconciling` state is set when the periodic reconciliation process starts
  - Reconciliation is invoked only if there were no changes in the YaookCluster resource for the specified duration (idle time), and
    every N seconds after that (interval time) as long as the YaookCluster resource does not change. Defaults to idle time as well as
    interval time is 1800s
  - Reconciling can be executed when the resource is in a `Running` or `Failed` state otherwise is skipped
- Resuming
  - When `k8s-capi-provider` Cluster controller main process is restarted the YaookCluster resource is transmitted to the `Resuming` state
- Running
  - `Running` is the YaookCluster resource state when it has become a Kubernetes cluster and the following underlying `yaook/k8s` stages passed:
     - init
     - apply-terraform
     - apply-stage2
     - apply-stage3
     - optionally upgrade
- Failed
  - `Failed` is the YaookCluster resource state when the system might require user intervention
  - It is possible that the YaookCluster failed state will be self-health in the next reconciliation loop
- Terminating
  - `Terminating` is the YaookCluster resource state when a delete request has been sent to the Cluster controller

![](images/yaook_cluster_livecycle.png)


# YaookService LiveCycle

The Yaook CAPI **Service Controller** is responsible for managing the lifecycle of a YaookService resource,
including its state transitions. This is the only controller that should be writing updates to YaookService resource.

## States and transitions

See below the list of possible resource states and the diagram of state transitions.

- Pending
  - YaookService resource initial state, assigned in the infinite loop when the service controller waits for
    corresponding YaookCluster resource running state. If the corresponding YaookCluster resource is in the running state,
    the creation of the YaookService could start.
- Creating
  - `Creating` is the first state a YaookService resource is assigned by the Service controller after being created
- Updating
  - `Updating` is the YaookService resource state when an update request has been sent to the Service controller
  - Update could be postponed when the resource has `Reconciling` state. Update will be triggered again after defined delay (defaults to 120s)
- Reconciling
  - `Reconciling` state is set when the periodic reconciliation process starts
  - Reconciliation is invoked only if there were no changes in the YaookService resource for the specified duration (idle time), and
    every N seconds after that (interval time) as long as the YaookService resource does not change. Defaults to idle time as well as
    interval time is 300s
  - Reconciling can be executed when the resource is in a `Running` or `Failed` state otherwise is skipped
- Resuming
  - When `k8s-capi-provider` Service controller main process is restarted the YaookService resource is transmitted to the `Resuming` state
- Running
  - `Running` state is set when the following underlying `yaook/k8s` stages passed:
     - init
     - apply-stage4
     - apply-stage5
- Failed
  - `Failed` is the YaookService resource state when the system might require user intervention
  - It is possible that the YaookService failed state will be self-health in the next reconciliation loop
- Terminating
  - `Terminating` is the YaookService resource state when a delete request has been sent to the Service controller

![](images/yaook_service_livecycle.png)

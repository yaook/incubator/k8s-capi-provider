# Quota validation

OpenStack's quotas are validated before workload cluster creation/update which
prevents the OpenStack project system capacities from being exhausted.
Validation implements a "best-effort" strategy. It means, that `k8s-capi-provider`
allows the creation of some workload resources up to quota limits.

:function:`validate_and_update_terraform_config` in [cluster/reconcile.py](https://gitlab.com/yaook/incubator/k8s-capi-provider/-/blob/devel/k8s_capi_provider/src/controllers/cluster/reconcile.py)
validates the following OpenStack's quotas:
- Number of server groups per project
- Number of floating IP addresses allowed per project
- Number of instances allowed per project
- Number of instance cores (VCPUs) allowed per project
- Megabytes of instance RAM allowed per project

## The validation strategy with examples

The Yaook workload cluster (usually) consists of multiple gateways, a control plane
and worker nodes. This function validates the quotas in the following order:

### Gateways

Validate whether the OpenStack project contains enough resources to scale gateways.

!Attention!: Gateway is considered a resource that **should** be scaled to the
desired state. If there is an attempt to scale gateway nodes over the quota
limits the `k8s-capi-provider` does not allow any scaling of gateways nodes
at all (best-effort strategy is not allowed here). If it happens during
cluster creation, it raises a :class:`ProviderPermanentError`
with "Quota exceeded" error message.

Gateway's best-effort strategy is discussed in this [issue](https://gitlab.com/yaook/incubator/k8s-capi-provider/-/issues/72).

Example:
- Existing cluster contains **1** gateway node
- Current OpenStack number of instances allowed is **1**
- There is an attempt to scale gateways to **3** nodes
- `k8s-capi-provider` does not allow any scaling of gateways nodes

### Control plane nodes

Validate whether the OpenStack project contains enough resources to scale the control plane.

!Attention!: Control plane is considered a resource that **should** be scaled to
the desired state. If there is an attempt to scale control plane nodes over the
quota limits the `k8s-capi-provider` does not allow any scaling of control plane
nodes at all (best-effort strategy is not allowed here). If it happens during
cluster creation, it raises a :class:`ProviderPermanentError`
with "Quota exceeded" error message.

The "best-effort" strategy is not implemented for the control plane, because
the number of control plane nodes should be uneven (1,3,5, ...). From the etcd's
raft protocol (and its failure tolerance) point of view, it does not make sense
to have some non-desired number of control plane nodes.

Example:
- Existing cluster contains **1** control plane node
- Current OpenStack number of instances allowed is **1**
- There is an attempt to scale the control plane to **3** nodes
- `k8s-capi-provider` does not allow any scaling of control plane nodes

### Worker nodes
Validate whether the OpenStack project contains enough resources to scale worker nodes.

The "best-effort" strategy is implemented here. If there is an attempt to scale
worker nodes over the quota limits, the `k8s-capi-provider` calculates the
maximum number of replicas, which do not exceed quotas.

Example:
- Existing cluster contains **1** worker node
- Current OpenStack number of instances allowed is **1**
- There is an attempt to scale the worker nodes to **3**
- `k8s-capi-provider` scales worker nodes to **2**

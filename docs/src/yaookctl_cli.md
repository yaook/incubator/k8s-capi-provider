# Commands

- [yaookctl](#yaookctl)
- [yaookctl generate](#yaookctl-generate)
- [yaookctl generate cluster](#yaookctl-generate-cluster)
- [yaookctl generate crd](#yaookctl-generate-crd)
- [yaookctl generate template](#yaookctl-generate-template)
- [yaookctl generate model](#yaookctl-generate-model)
- [yaookctl logs](#yaookctl-logs)
- [yaookctl logs cluster](#yaookctl-logs-cluster)
- [yaookctl logs ctrl](#yaookctl-logs-ctrl)
- [yaookctl get](#yaookctl-get)
- [yaookctl get cluster](#yaookctl-get-cluster)
- [yaookctl get kubeconfig](#yaookctl-get-kubeconfig)
- [yaookctl get config](#yaookctl-get-config)
- [yaookctl get openrc](#yaookctl-get-openrc)
- [yaookctl get wireguard](#yaookctl-get-wireguard)
- [yaookctl inject](#yaookctl-inject)

# yaookctl



CLI tool to manage yaook/incubator/k8s-capi-provider workload clusters.

## Usage

```
Usage: yaookctl [OPTIONS] COMMAND [ARGS]...
```

## Options

* `help`:
  * Type: `boolean`
  * Usage: `--help`
  * Default: `False`

    Show this message and exit.


## Commands

* [`generate` ↪](#yaookctl-generate)

  Generate cluster related files.
* [`get` ↪](#yaookctl-get)

  Get cluster files.
* [`inject` ↪](#yaookctl-inject)

  Inject cluster to k8s-capi-provider LCM.
* [`logs` ↪](#yaookctl-logs)

  Print cluster related logs.

## CLI Help

```
Usage: yaookctl [OPTIONS] COMMAND [ARGS]...

  CLI tool to manage yaook/incubator/k8s-capi-provider workload clusters.

Options:
  --help  Show this message and exit.

Commands:
  generate  Generate cluster related files.
  get       Get cluster files.
  inject    Inject cluster to k8s-capi-provider LCM.
  logs      Print cluster related logs.
```

# yaookctl generate

[↩ Parent](#yaookctl)

Generate cluster related files.

## Usage

```
Usage: yaookctl generate [OPTIONS] COMMAND [ARGS]...
```

## Options

* `help`:
  * Type: `boolean`
  * Usage: `--help`
  * Default: `False`

    Show this message and exit.


## Commands

* [`cluster` ↪](#yaookctl-generate-cluster)

  Generate cluster custom resources (CRs).
* [`crd` ↪](#yaookctl-generate-crd)

  Generate cluster CRDs from config toml template.
* [`model` ↪](#yaookctl-generate-model)

  Generate pydantic data model from CRDs.
* [`template` ↪](#yaookctl-generate-template)

  Generate cluster CRs template from config toml template.

## CLI Help

```
Usage: yaookctl generate [OPTIONS] COMMAND [ARGS]...

  Generate cluster related files.

Options:
  --help  Show this message and exit.

Commands:
  cluster   Generate cluster custom resources (CRs).
  crd       Generate cluster CRDs from config toml template.
  model     Generate pydantic data model from CRDs.
  template  Generate cluster CRs template from config toml template.
```

# yaookctl generate cluster

[↩ Parent](#yaookctl-generate)


Generate cluster CRs from template.
The template file is filled with environment variables from the local environment
OR from variables defined in the environment file.


Template file:
    - Could be defined as URL or a valid path
    - Default template is taken from https://gitlab.com/yaook/incubator/k8s-capi-provider/-/raw/devel/templates/cluster-template.yaml


Environment variables:
    - Could be defined in a file OR in the local environment
    - The sources above are NOT combined
    - See the list of variables below:


    Mandatory variables:
    - {'WG_USER_PUB_KEY', 'WG_USER_IDENT', 'CLUSTER_NAME'}


    Optional variables:
    - {'WORKER_MACHINE_COUNT', 'OPENSTACK_EXTERNAL_NETWORK_NAME', 'OPENSTACK_SSH_KEY_NAME', 'OPENSTACK_CONTROL_PLANE_AZ', 'OPENSTACK_CONTROL_PLANE_IMAGE_NAME', 'OPENSTACK_NODE_IMAGE_NAME', 'OPENSTACK_NODE_AZ', 'OPENSTACK_CONTROL_PLANE_MACHINE_FLAVOR', 'KUBERNETES_VERSION', 'OPENSTACK_NODE_MACHINE_FLAVOR', 'CONTROL_PLANE_MACHINE_COUNT', 'LBAAS_SHARED_SECRET'}


    OpenStack access mandatory variables:
    Please note that only one OpenStack authentication method should be used.


        OpenStack application credential authentication variables:
        - {'OS_APPLICATION_CREDENTIAL_ID', 'OS_AUTH_TYPE', 'OS_AUTH_URL', 'OS_APPLICATION_CREDENTIAL_SECRET'}


        OpenStack password authentication variables:
        - {'OS_IDENTITY_API_VERSION', 'OS_PROJECT_DOMAIN_ID', 'OS_INTERFACE', 'OS_PASSWORD', 'OS_AUTH_URL', 'OS_REGION_NAME', 'OS_USERNAME', 'OS_PROJECT_NAME', 'OS_USER_DOMAIN_NAME'}


## Usage

```
Usage: yaookctl generate cluster [OPTIONS]
```

## Options

* `output`:
  * Type: `path`
  * Usage: `--output
-o`
  * Default: `None`

    The output file path

* `template`:
  * Type: `template`
  * Usage: `--template
-t`
  * Default: `https://gitlab.com/yaook/incubator/k8s-capi-provider/-/raw/devel/templates/cluster-template.yaml`

    URL or path to the template file

* `env_file`:
  * Type: `path`
  * Usage: `--env
-e`
  * Default: `None`

    Path to the environment variables file

* `help`:
  * Type: `boolean`
  * Usage: `--help`
  * Default: `False`

    Show this message and exit.




## CLI Help

```
Usage: yaookctl generate cluster [OPTIONS]

  Generate cluster CRs from template. The template file is filled with
  environment variables from the local environment OR from variables defined
  in the environment file.

  Template file:
      - Could be defined as URL or a valid path
      - Default template is taken from https://gitlab.com/yaook/incubator/k8s-capi-provider/-/raw/devel/templates/cluster-template.yaml

  Environment variables:
      - Could be defined in a file OR in the local environment
      - The sources above are NOT combined
      - See the list of variables below:

      Mandatory variables:
      - {'WG_USER_PUB_KEY', 'WG_USER_IDENT', 'CLUSTER_NAME'}

      Optional variables:
      - {'WORKER_MACHINE_COUNT', 'OPENSTACK_EXTERNAL_NETWORK_NAME', 'OPENSTACK_SSH_KEY_NAME', 'OPENSTACK_CONTROL_PLANE_AZ', 'OPENSTACK_CONTROL_PLANE_IMAGE_NAME', 'OPENSTACK_NODE_IMAGE_NAME', 'OPENSTACK_NODE_AZ', 'OPENSTACK_CONTROL_PLANE_MACHINE_FLAVOR', 'KUBERNETES_VERSION', 'OPENSTACK_NODE_MACHINE_FLAVOR', 'CONTROL_PLANE_MACHINE_COUNT', 'LBAAS_SHARED_SECRET'}

      OpenStack access mandatory variables:
      Please note that only one OpenStack authentication method should be used.

          OpenStack application credential authentication variables:
          - {'OS_APPLICATION_CREDENTIAL_ID', 'OS_AUTH_TYPE', 'OS_AUTH_URL', 'OS_APPLICATION_CREDENTIAL_SECRET'}

          OpenStack password authentication variables:
          - {'OS_IDENTITY_API_VERSION', 'OS_PROJECT_DOMAIN_ID', 'OS_INTERFACE', 'OS_PASSWORD', 'OS_AUTH_URL', 'OS_REGION_NAME', 'OS_USERNAME', 'OS_PROJECT_NAME', 'OS_USER_DOMAIN_NAME'}

Options:
  -o, --output PATH        The output file path
  -t, --template TEMPLATE  URL or path to the template file  [default:
                           https://gitlab.com/yaook/incubator/k8s-capi-
                           provider/-/raw/devel/templates/cluster-
                           template.yaml]
  -e, --env PATH           Path to the environment variables file
  --help                   Show this message and exit.
```

# yaookctl generate crd

[↩ Parent](#yaookctl-generate)

Generate cluster CRDs from config toml template.

## Usage

```
Usage: yaookctl generate crd [OPTIONS]
```

## Options

* `output`:
  * Type: `path`
  * Usage: `--output
-o`
  * Default: `None`

    The output file path

* `toml_config` (REQUIRED):
  * Type: `path`
  * Usage: `--toml-config
-t`
  * Default: `None`

    Path to the config toml template file

* `help`:
  * Type: `boolean`
  * Usage: `--help`
  * Default: `False`

    Show this message and exit.




## CLI Help

```
Usage: yaookctl generate crd [OPTIONS]

  Generate cluster CRDs from config toml template.

Options:
  -o, --output PATH       The output file path
  -t, --toml-config PATH  Path to the config toml template file  [required]
  --help                  Show this message and exit.
```

# yaookctl generate template

[↩ Parent](#yaookctl-generate)

Generate cluster CRs template from config toml template.

## Usage

```
Usage: yaookctl generate template [OPTIONS]
```

## Options

* `output`:
  * Type: `path`
  * Usage: `--output
-o`
  * Default: `None`

    The output file path

* `toml_config` (REQUIRED):
  * Type: `path`
  * Usage: `--toml-config
-t`
  * Default: `None`

    Path to the config toml template file

* `help`:
  * Type: `boolean`
  * Usage: `--help`
  * Default: `False`

    Show this message and exit.




## CLI Help

```
Usage: yaookctl generate template [OPTIONS]

  Generate cluster CRs template from config toml template.

Options:
  -o, --output PATH       The output file path
  -t, --toml-config PATH  Path to the config toml template file  [required]
  --help                  Show this message and exit.
```

# yaookctl generate model

[↩ Parent](#yaookctl-generate)

Generate pydantic data model from CRDs.

## Usage

```
Usage: yaookctl generate model [OPTIONS]
```

## Options

* `output`:
  * Type: `path`
  * Usage: `--output
-o`
  * Default: `k8s_capi_provider/src/models`

    Path to the output directory.

* `crd`:
  * Type: `path`
  * Usage: `--crd
-c`
  * Default: `chart/crds/clusters.yaml`

    Path to the custom resource definition file

* `dry_run`:
  * Type: `boolean`
  * Usage: `--dry-run
-d`
  * Default: `False`

    Don't save a file to the output, use STDOUT instead.

* `help`:
  * Type: `boolean`
  * Usage: `--help`
  * Default: `False`

    Show this message and exit.




## CLI Help

```
Usage: yaookctl generate model [OPTIONS]

  Generate pydantic data model from CRDs.

Options:
  -o, --output PATH  Path to the output directory.  [default:
                     k8s_capi_provider/src/models]
  -c, --crd PATH     Path to the custom resource definition file  [default:
                     chart/crds/clusters.yaml]
  -d, --dry-run      Don't save a file to the output, use STDOUT instead.
  --help             Show this message and exit.
```

# yaookctl logs

[↩ Parent](#yaookctl)

Print cluster related logs.

## Usage

```
Usage: yaookctl logs [OPTIONS] COMMAND [ARGS]...
```

## Options

* `help`:
  * Type: `boolean`
  * Usage: `--help`
  * Default: `False`

    Show this message and exit.


## Commands

* [`cluster` ↪](#yaookctl-logs-cluster)

  Print cluster logs.
* [`ctrl` ↪](#yaookctl-logs-ctrl)

  Print controller logs.

## CLI Help

```
Usage: yaookctl logs [OPTIONS] COMMAND [ARGS]...

  Print cluster related logs.

Options:
  --help  Show this message and exit.

Commands:
  cluster  Print cluster logs.
  ctrl     Print controller logs.
```

# yaookctl logs cluster

[↩ Parent](#yaookctl-logs)

Print cluster logs.

    NAME is the name of the cluster.


## Usage

```
Usage: yaookctl logs cluster [OPTIONS] NAME
```

## Options

* `name` (REQUIRED):
  * Type: `text`
  * Usage: `name`
  * Default: `None`



* `namespace`:
  * Type: `text`
  * Usage: `--namespace
-n`
  * Default: `default`

    Cluster namespace

* `source`:
  * Type: `choice`
  * Usage: `--source
-s`
  * Default: `cluster`

    Log source. The controller which will be fetched

* `log_level`:
  * Type: `choice`
  * Usage: `--log-level
-l`
  * Default: `info`

    Specify log level

* `follow`:
  * Type: `boolean`
  * Usage: `--follow
-f`
  * Default: `False`

    Specify if the logs should be streamed

* `help`:
  * Type: `boolean`
  * Usage: `--help`
  * Default: `False`

    Show this message and exit.




## CLI Help

```
Usage: yaookctl logs cluster [OPTIONS] NAME

  Print cluster logs.

  NAME is the name of the cluster.

Options:
  -n, --namespace TEXT            Cluster namespace  [default: default]
  -s, --source [cluster|service]  Log source. The controller which will be
                                  fetched  [default: cluster]
  -l, --log-level [info|warning|error]
                                  Specify log level  [default: info]
  -f, --follow                    Specify if the logs should be streamed
  --help                          Show this message and exit.
```

# yaookctl logs ctrl

[↩ Parent](#yaookctl-logs)

Print controller logs.

    NAME is the name of the controller.


## Usage

```
Usage: yaookctl logs ctrl [OPTIONS] {cluster|service}
```

## Options

* `name` (REQUIRED):
  * Type: `choice`
  * Usage: `name`
  * Default: `None`



* `follow`:
  * Type: `boolean`
  * Usage: `--follow
-f`
  * Default: `False`

    Specify if the logs should be streamed

* `help`:
  * Type: `boolean`
  * Usage: `--help`
  * Default: `False`

    Show this message and exit.




## CLI Help

```
Usage: yaookctl logs ctrl [OPTIONS] {cluster|service}

  Print controller logs.

  NAME is the name of the controller.

Options:
  -f, --follow  Specify if the logs should be streamed
  --help        Show this message and exit.
```

# yaookctl get

[↩ Parent](#yaookctl)

Get cluster files.

## Usage

```
Usage: yaookctl get [OPTIONS] COMMAND [ARGS]...
```

## Options

* `help`:
  * Type: `boolean`
  * Usage: `--help`
  * Default: `False`

    Show this message and exit.


## Commands

* [`cluster` ↪](#yaookctl-get-cluster)

  Get all cluster files.
* [`config` ↪](#yaookctl-get-config)

  Get cluster config.toml file.
* [`kubeconfig` ↪](#yaookctl-get-kubeconfig)

  Get cluster kubeconfig file.
* [`openrc` ↪](#yaookctl-get-openrc)

  Get cluster OpenRC file.
* [`wireguard` ↪](#yaookctl-get-wireguard)

  Get cluster wireguard config file.

## CLI Help

```
Usage: yaookctl get [OPTIONS] COMMAND [ARGS]...

  Get cluster files.

Options:
  --help  Show this message and exit.

Commands:
  cluster     Get all cluster files.
  config      Get cluster config.toml file.
  kubeconfig  Get cluster kubeconfig file.
  openrc      Get cluster OpenRC file.
  wireguard   Get cluster wireguard config file.
```

# yaookctl get cluster

[↩ Parent](#yaookctl-get)

Get all cluster files.

    NAME is the name of the cluster.


## Usage

```
Usage: yaookctl get cluster [OPTIONS] NAME
```

## Options

* `name` (REQUIRED):
  * Type: `text`
  * Usage: `name`
  * Default: `None`



* `namespace`:
  * Type: `text`
  * Usage: `--namespace
-n`
  * Default: `default`

    Cluster namespace

* `output`:
  * Type: `path`
  * Usage: `--output
-o`
  * Default: `None`

    The output directory path. $PWD/<cluster-name>/ if not defined.

* `help`:
  * Type: `boolean`
  * Usage: `--help`
  * Default: `False`

    Show this message and exit.




## CLI Help

```
Usage: yaookctl get cluster [OPTIONS] NAME

  Get all cluster files.

  NAME is the name of the cluster.

Options:
  -n, --namespace TEXT  Cluster namespace  [default: default]
  -o, --output PATH     The output directory path. $PWD/<cluster-name>/ if not
                        defined.
  --help                Show this message and exit.
```

# yaookctl get kubeconfig

[↩ Parent](#yaookctl-get)

Get cluster kubeconfig file.

    NAME is the name of the cluster.


## Usage

```
Usage: yaookctl get kubeconfig [OPTIONS] NAME
```

## Options

* `name` (REQUIRED):
  * Type: `text`
  * Usage: `name`
  * Default: `None`



* `namespace`:
  * Type: `text`
  * Usage: `--namespace
-n`
  * Default: `default`

    Cluster namespace

* `output`:
  * Type: `path`
  * Usage: `--output
-o`
  * Default: `None`

    The output file path

* `help`:
  * Type: `boolean`
  * Usage: `--help`
  * Default: `False`

    Show this message and exit.




## CLI Help

```
Usage: yaookctl get kubeconfig [OPTIONS] NAME

  Get cluster kubeconfig file.

  NAME is the name of the cluster.

Options:
  -n, --namespace TEXT  Cluster namespace  [default: default]
  -o, --output PATH     The output file path
  --help                Show this message and exit.
```

# yaookctl get config

[↩ Parent](#yaookctl-get)

Get cluster config.toml file.

    NAME is the name of the cluster.


## Usage

```
Usage: yaookctl get config [OPTIONS] NAME
```

## Options

* `name` (REQUIRED):
  * Type: `text`
  * Usage: `name`
  * Default: `None`



* `namespace`:
  * Type: `text`
  * Usage: `--namespace
-n`
  * Default: `default`

    Cluster namespace

* `output`:
  * Type: `path`
  * Usage: `--output
-o`
  * Default: `None`

    The output file path

* `help`:
  * Type: `boolean`
  * Usage: `--help`
  * Default: `False`

    Show this message and exit.




## CLI Help

```
Usage: yaookctl get config [OPTIONS] NAME

  Get cluster config.toml file.

  NAME is the name of the cluster.

Options:
  -n, --namespace TEXT  Cluster namespace  [default: default]
  -o, --output PATH     The output file path
  --help                Show this message and exit.
```

# yaookctl get openrc

[↩ Parent](#yaookctl-get)

Get cluster OpenRC file.

    NAME is the name of the cluster.


## Usage

```
Usage: yaookctl get openrc [OPTIONS] NAME
```

## Options

* `name` (REQUIRED):
  * Type: `text`
  * Usage: `name`
  * Default: `None`



* `namespace`:
  * Type: `text`
  * Usage: `--namespace
-n`
  * Default: `default`

    Cluster namespace

* `output`:
  * Type: `path`
  * Usage: `--output
-o`
  * Default: `None`

    The output file path

* `help`:
  * Type: `boolean`
  * Usage: `--help`
  * Default: `False`

    Show this message and exit.




## CLI Help

```
Usage: yaookctl get openrc [OPTIONS] NAME

  Get cluster OpenRC file.

  NAME is the name of the cluster.

Options:
  -n, --namespace TEXT  Cluster namespace  [default: default]
  -o, --output PATH     The output file path
  --help                Show this message and exit.
```

# yaookctl get wireguard

[↩ Parent](#yaookctl-get)

Get cluster wireguard config file.

    NAME is the name of the cluster.


## Usage

```
Usage: yaookctl get wireguard [OPTIONS] NAME
```

## Options

* `name` (REQUIRED):
  * Type: `text`
  * Usage: `name`
  * Default: `None`



* `identity` (REQUIRED):
  * Type: `text`
  * Usage: `--identity
-i`
  * Default: `None`

    Wireguard peer identity name

* `namespace`:
  * Type: `text`
  * Usage: `--namespace
-n`
  * Default: `default`

    Cluster namespace

* `output`:
  * Type: `path`
  * Usage: `--output
-o`
  * Default: `None`

    The output file path

* `help`:
  * Type: `boolean`
  * Usage: `--help`
  * Default: `False`

    Show this message and exit.




## CLI Help

```
Usage: yaookctl get wireguard [OPTIONS] NAME

  Get cluster wireguard config file.

  NAME is the name of the cluster.

Options:
  -i, --identity TEXT   Wireguard peer identity name  [required]
  -n, --namespace TEXT  Cluster namespace  [default: default]
  -o, --output PATH     The output file path
  --help                Show this message and exit.
```

# yaookctl inject

[↩ Parent](#yaookctl)

Inject cluster to k8s-capi-provider LCM.

    NAME is the name of the cluster.


## Usage

```
Usage: yaookctl inject [OPTIONS] NAME
```

## Options

* `name` (REQUIRED):
  * Type: `text`
  * Usage: `name`
  * Default: `None`



* `namespace`:
  * Type: `text`
  * Usage: `--namespace
-n`
  * Default: `default`

    Cluster namespace

* `yk8s` (REQUIRED):
  * Type: `path`
  * Usage: `--yk8s
-y`
  * Default: `None`

    
The yaook/k8s workload cluster directory path.
yaookctl expects that the following yaook/k8s files and
directories have default filenames:
 - config/*
 - inventory/*
 - terraform/*
 - .envrc
 - openrc file is searched based on the `*openrc.sh` suffix

 If you want to use custom paths for the above, see CLI override options.


* `ssh_private_key` (REQUIRED):
  * Type: `path`
  * Usage: `--ssh-private-key
-ssh`
  * Default: `None`

    Path to the SSH private key file.

* `wg_key` (REQUIRED):
  * Type: `path`
  * Usage: `--wg-key
-wg`
  * Default: `None`

    Path to the wireguard private key file.

* `gpg_home` (REQUIRED):
  * Type: `path`
  * Usage: `--gpg-home
-gpg`
  * Default: `None`

    Path to the gpg home directory.

* `output`:
  * Type: `path`
  * Usage: `--output
-o`
  * Default: `None`

    The output directory path. $PWD/<cluster-name>/ if not defined.

* `toml_config`:
  * Type: `path`
  * Usage: `--toml-config
-t`
  * Default: `None`

    Path to the toml config file. Overrides config.toml file located in yk8s directory.

* `envrc`:
  * Type: `path`
  * Usage: `--envrc
-erc`
  * Default: `None`

    Path to the .envrc file. Overrides .envrc file located in yk8s directory.

* `openrc`:
  * Type: `path`
  * Usage: `--openrc
-orc`
  * Default: `None`

    Path to the openrc file. Overrides openrc file located in yk8s directory.

* `dry_run`:
  * Type: `boolean`
  * Usage: `--dry-run
-d`
  * Default: `False`

    Don't apply the output to the management cluster.

* `help`:
  * Type: `boolean`
  * Usage: `--help`
  * Default: `False`

    Show this message and exit.




## CLI Help

```
Usage: yaookctl inject [OPTIONS] NAME

  Inject cluster to k8s-capi-provider LCM.

  NAME is the name of the cluster.

Options:
  -n, --namespace TEXT          Cluster namespace  [default: default]
  -y, --yk8s PATH               The yaook/k8s workload cluster directory path.
                                yaookctl expects that the following yaook/k8s files and
                                directories have default filenames:
                                 - config/*
                                 - inventory/*
                                 - terraform/*
                                 - .envrc
                                 - openrc file is searched based on the `*openrc.sh` suffix

                                 If you want to use custom paths for the
                                 above, see CLI override options.
                                 [required]
  -ssh, --ssh-private-key PATH  Path to the SSH private key file.  [required]
  -wg, --wg-key PATH            Path to the wireguard private key file.
                                [required]
  -gpg, --gpg-home PATH         Path to the gpg home directory.  [required]
  -o, --output PATH             The output directory path. $PWD/<cluster-
                                name>/ if not defined.
  -t, --toml-config PATH        Path to the toml config file. Overrides
                                config.toml file located in yk8s directory.
  -erc, --envrc PATH            Path to the .envrc file. Overrides .envrc file
                                located in yk8s directory.
  -orc, --openrc PATH           Path to the openrc file. Overrides openrc file
                                located in yk8s directory.
  -d, --dry-run                 Don't apply the output to the management
                                cluster.
  --help                        Show this message and exit.
```

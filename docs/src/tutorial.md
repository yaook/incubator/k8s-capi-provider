# Easy to jump in

Hello k8s-capi-provider user!

This page aims at providing a handy tutorial of the k8s-capi-provider project.
It provides guidance and commands that readers are encouraged to try out by themselves
on a demo environment as described in the next section.

At the end of this tutorial, the reader should be able to interact with k8s-capi-provider
and manage workload Kubernetes clusters with this great tool.

It does not aim at providing an exhaustive list of commands nor all the possible ways how to use them.
It also does not aim the local development environment. If you want to set up one, please visit
[Local Development](local_dev.md) page.

## Prerequisites

- [Docker][docker]
- [KinD][kind]
- [Kubectl][kubectl]
- [Helm][helm]
- [yaookctl][yaookctl]
  ```shell
  python3 -m pip install "git+https://gitlab.com/yaook/incubator/k8s-capi-provider.git@devel#egg=yaookctl&subdirectory=yaookctl"
  ```
- [WireGuard][wireguard]
  - create wg [key](https://yaook.gitlab.io/k8s/usage/initialization.html#wireguard-key)

## Create management cluster

Create KinD cluster:
```shell
kind create cluster
```
Check if it is running:
```shell
$ kubectl cluster-info
Kubernetes control plane is running at https://127.0.0.1:38081
CoreDNS is running at https://127.0.0.1:38081/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy
```

> If you want to use Calico CNI for k8s networking inside the management cluster, then for the proper functioning
> of the k8s-capi-provider you have to allow ip forwarding in the containers(disabled by default).
> See calico cni plugin [configuration][cni-configuration]

## Deploy k8s-capi-provider by Helm

Add k8s-capi-provider helm repository:
```shell
helm repo add k8s-capi-provider https://gitlab.com/api/v4/projects/31045539/packages/helm/stable
```
Install k8s-capi-provider:
```shell
helm install k8s-capi-provider k8s-capi-provider/k8s-capi-provider
```

Helm automatically creates:
- `yaookcluster` and other custom resource definitions (CRDs)
  ```shell
  $ kubectl get crds
  NAME
  yaookclusters.yaook.mk8s.io
  yaookconfigtemplates.yaook.mk8s.io
  yaookcontrolplanes.yaook.mk8s.io
  yaookmachinedeployments.yaook.mk8s.io
  yaookmachinetemplates.yaook.mk8s.io
  yaookservices.yaook.mk8s.io
  ```

- k8s-capi-provider controllers
  ```shell
  $ kubectl get deploy
  NAME                        READY   UP-TO-DATE   AVAILABLE   AGE
  k8s-capi-provider-cluster   1/1     1            1           3m13s
  k8s-capi-provider-service   1/1     1            1           3m13s
  ```

See helm chart [README][helm-readme] for further details or available chart parameters.

## Create workload cluster

Modify [manifests/.example-env][example-env] variables or export it as env variables. Would be great to override at least `CLUSTER_NAME="example-cluster"`, `WG_USER_PUB_KEY="test"` and `WG_USER_IDENT="test@example.com"`. Also, your OpenStack credentials are needed. We support `v3password` and `v3applicationcredential` authentication type.

Then use [yaookctl][yaookctl] CLI tool, to generate workload cluster CRs and save them to the `example-cluster.yaml`:
```shell
yaookctl generate cluster -e manifests/.example-env -o example-cluster.yaml
```
> ***Note:*** See `yaookctl generate cluster --help` for details.

> ***Note:*** If you've exported mandatory variables in your environment, you don't need to use switch *-e/--env*.

yaookctl automatically downloads und uses template file from repo [templates/cluster-template.yaml][cluster-template]

> ***Note:*** If you want to create CRs from scratch (without yaookctl) or you want to see the full list of supported variables, please see [API reference](api_reference.md).

After this step, you should have everything ready for creating workload cluster. Now just apply generated cluster CRs:
```shell
kubectl apply -f example-cluster.yaml
```
```shell
$ kubectl describe yc example-cluster
Events:
  Type     Reason   Age   From  Message
  ----     ------   ----  ----  -------
  Normal   Logging  33s   kopf  create of cluster example-cluster in default namespace started
```
Watch cluster controller logs (cluster ctrl handles terraform and bootstrap layer) with:
```shell
yaookctl logs ctrl cluster -f
```

Watch service controller logs (service ctrl handles service layer) with:
```shell
yaookctl logs ctrl service -f
```

Watch progress of cluster creation (terraform, bootstrap layers) with:
```shell
yaookctl logs cluster example-cluster -f
```

Alternatively, watch service deployment layer logs with:
```shell
yaookctl logs cluster example-cluster -s service -f
```

After some time, your cluster is up and running.
```shell
$ kubectl describe yc example-cluster
Events:
  Type     Reason   Age   From  Message
  ----     ------   ----  ----  -------
  Normal   Logging  31m   kopf  create of cluster example-cluster in default namespace started
  Normal   Logging  10m   kopf  create of cluster example-cluster in default namespace finished
  Normal   Logging  10m   kopf  Handler 'on_action' succeeded.
  Normal   Logging  10m   kopf  Creation is processed: 1 succeeded; 0 failed.
```

You can list all the resources belonging to the category yaook.
 ```shell
$ kubectl get yaook
NAME                                                               AGE
yaookservice.yaook.mk8s.io/example-cluster-service                 3m19s

NAME                                                               AGE
yaookcluster.yaook.mk8s.io/example-cluster                         3m19s

NAME                                                               AGE
yaookconfigtemplate.yaook.mk8s.io/example-cluster                  3m19s

NAME                                                               AGE
yaookcontrolplane.yaook.mk8s.io/example-cluster-control-plane      3m19s

NAME                                                               AGE
yaookmachinedeployment.yaook.mk8s.io/example-cluster-md-0          3m19s

NAME                                                               AGE
yaookmachinetemplate.yaook.mk8s.io/example-cluster-control-plane   3m19s
yaookmachinetemplate.yaook.mk8s.io/example-cluster-md-0            3m19s
```

## Interact with workload cluster

At first, you need to establish wireguard tunnel
- Extract wireguard template config file.  Wireguard peer identity name (-i/--identity) is required.
  The following command save the config file as `wg0.conf`:
  ```shell
  yaookctl get wireguard example-cluster -i test@example.com -o wg0.conf
  ```
- Then you need to insert your private key into `wg0.conf`, e.g.:
  ```shell
  sed -i s#REPLACEME#$(cat ~/.wireguard/wg.key)# wg0.conf
  ```
- Finally, run:
  ```shell
  wg-quick up ./wg0.conf
  ```
Then, you need to extract the kubeconfig of the workload cluster. Save it to the `example-cluster.conf`:
```shell
yaookctl get kubeconfig example-cluster -o example-cluster.conf
```

Finally, you can interact with your workload cluster:
```shell
$ kubectl --kubeconfig example-cluster.conf get nodes
NAME                   STATUS   ROLES                  AGE   VERSION
managed-k8s-master-0   Ready    control-plane,master   35m   v1.22.11
managed-k8s-master-1   Ready    control-plane,master   33m   v1.22.11
managed-k8s-master-2   Ready    control-plane,master   33m   v1.22.11
managed-k8s-worker-0   Ready    <none>                 31m   v1.22.11
managed-k8s-worker-1   Ready    <none>                 31m   v1.22.11
managed-k8s-worker-2   Ready    <none>                 30m   v1.22.11
managed-k8s-worker-3   Ready    <none>                 31m   v1.22.11
```

## Update workload cluster

Follow the same steps as in cluster [creation](#create-workload-cluster):
- Modify env variables and use [yaookctl][yaookctl] tool for generating new manifests, or directly edit `example-cluster.yaml`
- Run `kubectl apply -f example-cluster.yaml`

## Delete workload cluster

You can delete that cluster with:
```shell
kubectl delete yc example-cluster
```

Optionally remove wireguard tunnel with:
```shell
wg-quick down ./wg0.conf
```

<!-- References -->

[kind]: https://kubernetes.io/docs/tasks/tools/#kind
[docker]: https://docs.docker.com/get-docker/
[kubectl]: https://kubernetes.io/docs/tasks/tools/#kubectl
[helm]: https://helm.sh/
[wireguard]: https://www.wireguard.com/
[yaookctl]: https://yaook.gitlab.io/incubator/k8s-capi-provider/yaookctl
[cni-configuration]: https://projectcalico.docs.tigera.io/reference/cni-plugin/configuration
[helm-readme]: https://gitlab.com/yaook/incubator/k8s-capi-provider/-/raw/devel/chart/README.md
[example-env]: https://gitlab.com/yaook/incubator/k8s-capi-provider/-/raw/devel/manifests/.example-env
[cluster-template]: https://gitlab.com/yaook/incubator/k8s-capi-provider/-/raw/devel/templates/cluster-template.yaml

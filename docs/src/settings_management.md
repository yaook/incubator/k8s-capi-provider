# Settings Management

K8s-capi-provider allows users to modify its behavior with a variety of configuration options. It inherits from pydantic :class:`BaseSettings` which enables
enhance settings management, see pydantic's settings management [docs][docs-pydantic] for details.
You could find the current configuration options in [settings.py][settings] file or print the options and their default values by the following:

```shell
python -c "from src.settings import ProviderSettings;import pprint;pp=pprint.PrettyPrinter();pp.pprint(ProviderSettings().dict())"
```

Any configuration option could be overwritten by an environment variable.
The environment variable name is built by concatenating the prefix and field name.
The `provider_` prefix should be used. To reach nested variables the double underscore `__` delimiter should be applied.
For example, to override cluster controller reconciliation interval to 1800s, you could use:

```shell
export provider_cluster_ctrl__reconcile_interval="3600"
```

Default values will still be used if the matching environment variable is not set.
You could validate your override e.g. by the following:

```shell
provider_cluster_ctrl__reconcile_interval="3600" python -c "from src.settings import ProviderSettings;import pprint;pp=pprint.PrettyPrinter();pp.pprint(ProviderSettings().dict())"
```

<!-- References -->

[docs-pydantic]: https://pydantic-docs.helpmanual.io/usage/settings/
[settings]: https://gitlab.com/yaook/incubator/k8s-capi-provider/-/raw/devel/k8s_capi_provider/src/settings.py

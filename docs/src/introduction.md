# Introduction

k8s-capi-provider is [yaook][yaook] incubator project. It is intended to be a
Lifecycle-Management (LCM) tool which provides, operates and customize, highly available,
and flexible kubernetes cluster. It adopts [CAPI][capi] approach and encapsulates
LCM logic from another yaook project [yaook-k8s][yaook-k8s]. It is based on [kopf][kopf] framework.

k8s-capi-provider simplifies the workload cluster state management, integration possibilities,
and provides standard (CAPI-like) [interface](api_reference.md) for workload cluster LCM. It is able to do an LCM of
multiple workload clusters concurrently and do the periodical reconciliation to the desired state.
Its non-monolithic architecture also open possibilities for further improvements.

> ***Hint:*** If you want to get your cluster up and running, the [Tutorial](tutorial.md) is a good place to begin.

## Architecture Overview

![High-level Architecture Overview](images/k8s_capi_provider_architecture.png)
<center>High-level Architecture Overview</center>

---

`k8s-capi-provider` consists of two controllers. Each controller watches yaook custom resources in order to do the
LCM of workload cluster. Yaook custom resources are the single source of truth that describes the desired state
of the workload cluster.

### K8s-capi-provider Controllers

| Type               | Short Description                                                                              |
|:-------------------|:-----------------------------------------------------------------------------------------------|
| Cluster Controller | The cluster controller manages the workload cluster infrastructure and bootstrapping.          |
| Service Controller | The service controller manages the workload cluster services, e.g. cert manager, ingress, etc. |

#### Cluster Controller

The cluster controller watches the `YaookCluster` resource and its linked yaook resources:
- `YaookControlPlane`
- `YaookMachineDeployment`
- `YaookConfigTemplate`
- `YaookMachineTemplate`

Based on the above resources the cluster controller is able to compose workload cluster configuration
to provision the workload cluster infrastructure, boostrap it and when requested also
upgrade the version of workload cluster or remove it from the target OpenStack project.

In the sense of `yaook/k8s` project, the cluster controller executes various`yaook/k8s` actions, based on the
request.

When the workload cluster create or update is triggered, the cluster controller
executes the following:
- [init.sh][initsh] action
- [Harbour Infrastructure][harbour] layer: [apply-terraform.sh][apply-terraformsh] action
- [Trampoline][trampoline] layer: [apply-stage2.sh][apply-stage2sh] action
- [k8s-base][k8s-base] layer: [apply-stage3.sh][apply-stage3sh] action

When the workload cluster upgrade (bump of k8s version) is triggered, the cluster controller
executes the following:
- [init.sh][initsh] action
- [Harbour Infrastructure][harbour] layer: [apply-terraform.sh][apply-terraformsh] action
- [Trampoline][trampoline] layer: [apply-stage2.sh][apply-stage2sh] action
- [k8s-base][k8s-base] layer: [apply-stage3.sh][apply-stage3sh] action
- [upgrade.sh][upgradesh] action

    _Note: As the implementation (currently) does not know whether some regular update is
           requested together with an upgrade or not, we have to execute the whole infrastructure
           and bootstrap stack before the upgrade._

When the workload cluster delete is triggered, the cluster controller
executes the following:
- [init.sh][initsh] action
- [destroy.sh][destroysh] action

Cluster Controller is only controller that is able to:
- Access a target OpenStack project, i.e. it is able to read and use the linked OpenStack RC secret resource
- [Validate OpenStack quotas](quota_validation.md) before `yaook/k8s` actions
- Update the `YaookCluster` resource states and state transitions, see [YaookCluster livecycle](yaook_resources_livecycle.md#yaookcluster-livecycle) docs
for further details

#### Service Controller

The service controller watches the `YaookService` resource.

Based on the above resource the service controller is able to compose service layer configuration
for the workload cluster and provision it.

In the sense of `yaook/k8s` project, the service controller executes following `yaook/k8s` actions:
- [init.sh][initsh] action
- [Kubernetes Service][ksl] layer: [apply-stage4.sh][apply-stage4sh] action
- [Kubernetes Managed Services][kms] layer: [apply-stage5.sh][apply-stage5sh] action

Service Controller is only controller that is able to:
- Update the `YaookService` resource states and state transitions, see [YaookService livecycle](yaook_resources_livecycle.md#yaookservice-livecycle) docs
for further details

### Yaook Custom Resources

| Type                   | Managed By         | API Reference                                  | Short Description                                                                                                                                                                                                                              |
|:-----------------------|:-------------------|:-----------------------------------------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| YaookCluster           | Cluster Controller | [API](api_reference.md#yaookcluster)           | It contains the cluster-wide specific variables (e.g. load-balancing, wireguard) and references to the YaookControlPlane and Secret with OpenStackRC credentials.                                                                              |
| YaookControlPlane      | Cluster Controller | [API](api_reference.md#yaookcontrolplane)      | It contains the control plane specific variables (e.g. nodes count) and references to the YaookMachineTemplate and YaookConfigTemplate. There should be one YaookControlPlane resource per cluster.                                            |
| YaookMachineDeployment | Cluster Controller | [API](api_reference.md#yaookmachinedeployment) | It contains the workload nodes' specific variables (e.g. nodes count, az) and references to the YaookMachineTemplate, YaookConfigTemplate, and YaookCluster. There could be zero or more than one YaookMachineDeployment resource per cluster. |
| YaookMachineTemplate   | Cluster Controller | [API](api_reference.md#yaookmachinetemplate)   | It contains the infrastructure-specific variables (e.g. image, flavor). There should be one dedicated YaookConfigTemplate for YaookMachineDeployment and one dedicated for the YaookControlPlane.                                              |
| YaookConfigTemplate    | Cluster Controller | [API](api_reference.md#yaookconfigtemplate)    | It contains the bootstrap-specific variables. There should be only one (shared) YaookConfigTemplate for YaookMachineDeployment and YaookControlPlane.                                                                                          |
| YaookService           | Service Controller | [API](api_reference.md#yaookservice)           | It contains the service layer specific variables (e.g. cert-manager, ingress) and reference to the YaookCluster. There should be one YaookService resource per cluster.                                                                        |

### Abstraction Layers

![High-level Abstraction Layers Overview](images/k8s_capi_provider_layers.png)
<center>High-level Abstraction Layers Overview</center>

#### handlers layer

Handlers watch the yaook custom resources and react appropriately for their CRUD events.
Periodical reconciliation of workload clusters is also ensures here, by the timer handlers.
The [Kopf][kopf] framework is our silver bullet to simplify the interaction with k8s API.

#### k8s-capi-provider layer

k8s-capi-provider layer creates and manages the environment needed for `yaook/k8s` layer.
The underlying `yaook/k8s` layer expects that the entity which does the LCM of workload clusters
has its own identity for SSH access via Wireguard VPN tunnel. Also, the GPG password
store is used as a secure Ansible variable storage. Hence, our k8s-capi-provider has to
create its own SSH/Wireguard/GPG identities per workload cluster.
In addition, k8s-capi-provider is able to manage multiple workload clusters concurrently,
hence the dedicated Linux network namespace has to be created per workload cluster due to
possible Wireguard VPN network overlap.
The k8s-capi-provider layer manages the above `yaook/k8s` prerequisites and also is responsible for
persistent storage management of k8s-capi-provider identities and any artifacts which are produced
by `yaook/k8s` layer.

k8s-capi-provider layer responsibilities could be summed up as follows:
- Management of workload cluster persistent storage using k8s secrets, see [Cluster persistent storage](#cluster-persistent-storage) for details
- Management of Linux network namespaces per workload cluster
- Management of SSH/Wireguard/GPG identities per workload cluster
- Management of additional Ansible variables needed e.g. for smooth ssh access
- Composing of configuration file and setting the environment variables needed for `yaook/k8s` layer

#### yaook/k8s layer

[yaook-k8s][yaook-k8s] project acts as an underlying layer which is responsible for LCM
of the workload clusters. [yaook-k8s][yaook-k8s] uses Terraform and Ansible stack
to provide a kubeadm-based k8s workload cluster installation and LCM on top of OpenStack.
The source of truth for the [yaook-k8s][yaook-k8s] is a configuration file and
environment variables composed in the k8s-capi-provider layer.

### Cluster persistent storage

Any workload cluster state produced by underlying `yaook/k8s` layer as well as k8s-capi-provider identities
for workload cluster access is stored in persistent storage. The persistent storage is loaded before each
`yaook/k8s` action and it is updated after the action finish. This ensures that the watched workload cluster
is in sync with the state used by k8s-capi-provider, even when the k8s-capi-provider is restarted for any reason.

The regular **kubernetes secrets** are used as a persistent storage (for now). States organized in directory
are stored in `.tar.gz` archives. `.gz` archive is used for standalone state files.

Overall the k8s-capi-provider uses up to 8 kubernetes secrets per workload cluster. They store the following artifacts:
- Configuration directory (includes config.toml, wireguard_ipam.toml, pass_users.toml)
- Ansible inventory directory
- Terraform tfstate file
- Terraform tfvars file
- GnuPG home directory: k8s-capi-provider GPG identity
- SSH private key (provider) file: k8s-capi-provider SSH identity
- Wireguard private key file: k8s-capi-provider Wireguard identity
- Optionally, SSH private key (user) file: user SSH identity. User SSH key is auto-generated when the user does not specify
  own SSH key which should be used to nodes access

<!-- References -->

[yaook]: https://gitlab.com/yaook
[yaook-k8s]: https://gitlab.com/yaook/k8s
[capi]: https://cluster-api.sigs.k8s.io
[kopf]: https://kopf.readthedocs.io
[harbour]: https://yaook.gitlab.io/k8s/design/abstraction-layers.html#harbour-infrastructure-layer
[trampoline]: https://yaook.gitlab.io/k8s/design/abstraction-layers.html#trampoline
[k8s-base]: https://yaook.gitlab.io/k8s/design/abstraction-layers.html#k8s-base
[kms]: https://yaook.gitlab.io/k8s/design/abstraction-layers.html#kms---kubernetes-managed-services
[ksl]: https://yaook.gitlab.io/k8s/design/abstraction-layers.html#ksl---kubernetes-service-layer
[initsh]: https://yaook.gitlab.io/k8s/operation/actions-references.html?highlight=init.sh#initsh
[apply-terraformsh]: https://yaook.gitlab.io/k8s/operation/actions-references.html?highlight=init.sh#apply-terraformsh
[apply-stage2sh]: https://yaook.gitlab.io/k8s/operation/actions-references.html?highlight=init.sh#apply-stage2sh
[apply-stage3sh]: https://yaook.gitlab.io/k8s/operation/actions-references.html?highlight=init.sh#apply-stage3sh
[apply-stage4sh]: https://yaook.gitlab.io/k8s/operation/actions-references.html?highlight=init.sh#apply-stage4sh
[apply-stage5sh]: https://yaook.gitlab.io/k8s/operation/actions-references.html?highlight=init.sh#apply-stage5sh
[upgradesh]: https://yaook.gitlab.io/k8s/operation/actions-references.html?highlight=init.sh#upgradesh
[destroysh]: https://yaook.gitlab.io/k8s/operation/actions-references.html?highlight=init.sh#destroysh

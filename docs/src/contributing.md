# Contributing

Thank you for considering contributing to k8s-capi-provider!

If there is not an open issue for what you want to submit, prefer
opening one for discussion before working on merge request (MR). You can work on any
issue that doesn't have an open MR linked to it or a maintainer assigned
to it. These show up in the sidebar. No need to ask if you can work on
an issue that interests you.

Include the following in your patch:

- Use [Black][black] to format your code. This and other tools will run
automatically if you install [pre-commit][pre-commit]. Please read [Pre-commit](#pre-commit)
- Include tests if your patch adds or changes code. Make sure the test
fails without your patch
- Update any relevant docs pages and docstrings. Docstrings are written following
the [Google Style Python Docstrings][sphinx-google]
- Follow the guidelines on [How to Write a Git Commit Message][git-commit].


## Pre-commit

To run all pre-commit hooks on your code, go to the project's root directory and run

```bash
pre-commit run
```

By default, pre-commit will only run on the files that have been changed,
meaning those that have been staged in git (i.e. after git add your_script.py).

If you want to manually run all pre-commit hooks on a repository, run

```bash
pre-commit run --all-files
```
Visit [pre-commit usage][pre-commit-usage] guide for further details.

_Our recommendation_ is to configure pre-commit to run on the staged files before every commit
(i.e. git commit), by installing it as a git hook using

```bash
pre-commit install
```


## Running the tests

Run the basic test suite with pytest.

```bash
pytest k8s_capi_provider
```

This runs the tests for the current environment, which is usually sufficient.
CI will run the full suite (including slow tests) when you submit your merge request.
You can run the full test suite (including slow tests) as follows:

```bash
pytest k8s_capi_provider --run-slow
```

Slow tests include testing with [KinD][kind] kubernetes cluster.
KinD cluster is session scoped, therefore should be cleaned up after each test.
Cleanup is done automatically (see [conftest.py][conftest]), but do not forget to update
cleanup logic if your test creates some new k8s resource.

It is possible to keep KinD kubernetes cluster alive after the test session is finished
to speed up development process or debugging.

```bash
pytest k8s_capi_provider --run-slow --keep-cluster
```
Could be useful to check operator logs as well:

```bash
pytest k8s_capi_provider --run-slow -s -o log_cli=true
```


<!-- References -->

[git-commit]: https://chris.beams.io/posts/git-commit/
[sphinx-google]: https://www.sphinx-doc.org/en/master/usage/extensions/example_google.html
[black]: https://black.readthedocs.io
[pre-commit]: https://pre-commit.com
[pre-commit-usage]: https://pre-commit.com/#usage
[kind]: https://kubernetes.io/docs/tasks/tools/#kind
[conftest]: https://gitlab.com/yaook/incubator/k8s-capi-provider/-/raw/devel/k8s_capi_provider/tests/conftest.py

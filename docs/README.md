# Yaook k8s-capi-provider handbook

This is a mdbook.

## Table of Contents

See [SUMMARY.md](src/SUMMARY.md).

## How to render

1. Install cargo and rustc from apt
    ```shell
    apt install cargo rustc
    ```
2. Install mdbook and linkcheck
    ```shell
    cargo install mdbook mdbook-linkcheck
    ```
3. Add cargo to the path
    ```shell
    export PATH="$HOME/.cargo/bin:$PATH"
    ```
4. Preview a book by
    ```shell
    mdbook serve docs -p 8000
    ```
5. Open http://127.0.0.1:8000 in your browser

Alternatively, you can render the book and open it without going through `mdbook serve`, but Browsers may be a tad picky about that: `~/.cargo/bin/mdbook build` and then open `book/index.html`.

## Generated docs

### API Reference

When project [CRDs](../chart/crds/clusters.yaml) are changed the [API reference](src/api_reference.md)
docs should be re-generated as follows:

```shell
docker run -u $(id -u):$(id -g) --rm -v ${PWD}:/workdir ghcr.io/fybrik/crdoc:latest --resources /workdir/chart/crds/clusters.yaml --output /workdir/docs/src/api_reference.md
```

### yaookctl

When the [yaookctl](../yaookctl/README.md) CLI is changed the [yaookctl cli docs](src/yaookctl_cli.md) should be re-generated as follows:

```shell
pip install -r docs/helpers/requirements.txt
python docs/helpers/mdclick.py --module=yaookctl.cli --cmd=yaookctl --output=./docs/src/yaookctl_cli.md
```

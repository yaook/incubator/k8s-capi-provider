"""Generate click CLI markdown documentation.

mdclick is a command line tool for creating markdown files for any
python's click CLI projects.

This tool is highly inspired by the
https://github.com/RiveryIO/md-click project.

The current state of upstream md-click project is as follows
(from k8s-capi-provider perspective):
- unresolved bug https://github.com/RiveryIO/md-click/issues/8 (blocker)
- obsolete requirements that are not is sync with `yaookctl` requirements
- missing ability to create one .md file with cross-references to commands/subcommands
- missing table of contents

TODO: Create an upstream PR/fork-project with the fixes/improvements above

"""
import importlib
from pathlib import Path
from typing import Any, Generator, List, NamedTuple, Optional

import click

CMD_TEMPLATE = """
# {command_name}

{parent}

{description}

## Usage

```
{usage}
```

## Options

{options}

{commands}

## CLI Help

```
{help}
```
"""


class Parameter(NamedTuple):
    """Command parameter definition.

    see :class:`click.core.Parameter`
    """

    name: str
    type_name: str
    default: Any
    usage: str
    required: bool
    prompt: Optional[str] = None
    help: Optional[str] = None


class CommandMetadata(NamedTuple):
    """Command metadata definition."""

    name: str
    help: str
    usage: str
    parent_reference: str
    params: List[Parameter]
    subcommands: Optional[Any]
    description: Optional[str]


def get_cmd_metadata(
    cmd: click.core.Command,
    parent: Optional[click.core.Context] = None,
    command_name: Optional[str] = None,
) -> Generator[CommandMetadata, None, None]:
    """Get commands metadata recursively.

    Args:
        cmd (click.core.Command): Command from witch the metadata will be parsed.
        parent (Context, optional): Parent command context.
        command_name (str, optional): Command name.

    Yields:
        CommandMetadata: Command metadata.
    """
    ctx = click.core.Context(cmd, info_name=cmd.name, parent=parent)

    parent_reference = (
        f"#{command_name.replace(' ', '-').lower()}" if command_name else ""
    )

    command_name = f"{command_name + ' ' if command_name else ''}{str(cmd.name)}"

    subcommands = cmd.to_info_dict(ctx).get("commands", {})
    subcommands_names = getattr(cmd, "commands", {})

    # Update each command with the filename of the command's .md
    for subcommand in subcommands_names:
        subcommands[subcommand][
            "link"
        ] = f"#{command_name.replace(' ', '-').lower()}-{subcommand}"

    params = [
        Parameter(
            name=param.name or "",
            type_name=param.type.name,
            default=param.default,
            usage="\n".join(param.opts),
            required=param.required,
            prompt=param.prompt if isinstance(param, click.core.Option) else None,
            help=param.help if isinstance(param, click.core.Option) else None,
        )
        for param in cmd.get_params(ctx)
    ]
    yield CommandMetadata(
        name=command_name,
        help=cmd.get_help(ctx),
        usage=cmd.get_usage(ctx),
        description=cmd.help,
        parent_reference=parent_reference,
        params=params,
        subcommands=subcommands,
    )

    for sub in subcommands_names.values():
        yield from get_cmd_metadata(sub, ctx, command_name)


def create_docs(base_command, output):
    """Dump help usage files from Click Help files into md."""
    toc = ["# Commands\n"]
    markdown_docs = []
    for meta in get_cmd_metadata(base_command):

        toc.append(f"- [{meta.name}](#{meta.name.replace(' ','-').lower()})")
        markdown_docs.append(
            CMD_TEMPLATE.format(
                command_name=meta.name,
                parent=f"[↩ Parent]({meta.parent_reference})"
                if meta.parent_reference
                else "",
                description=meta.description,
                usage=meta.usage,
                options="\n".join(
                    [
                        f"* `{param.name}`{' (REQUIRED)' if param.required else ''}: \n"
                        f"  * Type: `{param.type_name}` \n"
                        f"  * Usage: `{param.usage}`\n"
                        f"  * Default: `{param.default}`\n\n"
                        f"    {param.help or ''}\n"
                        f"    {param.prompt or ''}"
                        for param in meta.params
                    ]
                ),
                commands="## Commands\n\n"
                + "\n".join(
                    [
                        f"* [`{cmd_name}` ↪]({opt.get('link')})\n"
                        "\n"
                        f"  {opt.get('short_help') or opt.get('help') or ''}"
                        for cmd_name, opt in meta.subcommands.items()
                    ]
                )
                if meta.subcommands
                else "",
                help=meta.help,
            )
        )
        # Create the docs
        with open(output, "w") as md_file:
            md_file.write("\n".join(toc) + "\n")

            for markdown_doc in markdown_docs:
                md_file.write(markdown_doc)


@click.command()
@click.option(
    "--module", "-m", help="The base command module path to import", required=True
)
@click.option("--cmd", "-c", help="The base command function to import", required=True)
@click.option(
    "--output",
    "-o",
    help="The output filename to write the md file",
    required=True,
    type=click.Path(),
)
def md_docs(module, cmd, output):
    """Create md docs file from click CLI."""
    click.secho(f"Creating MD docs from {module}.{cmd}", fg="green")

    try:
        # Import the module
        module_ = importlib.import_module(module)
    except (ModuleNotFoundError, NameError) as e:
        raise click.ClickException(f"Could not find module: {module}. Error: {str(e)}")

    try:
        # Import the base command (group of command) function inside the module
        command_ = getattr(module_, cmd)
    except AttributeError:
        raise click.ClickException(f"Could not find command {cmd} on module {module}")

    try:
        create_docs(command_, output=Path(output))
        click.secho(f"MD docs have been successfully saved to {output}", fg="green")
    except Exception as e:
        raise click.ClickException(f"Dumps command failed: {str(e)}")


if __name__ == "__main__":
    md_docs()

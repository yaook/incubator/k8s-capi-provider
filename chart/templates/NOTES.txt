k8s-capi-provider has been installed.
Check its status by running:

    kubectl --namespace {{ template "chart.namespace" . }} get pods

{{/* Helpers variables */}}

{{/*
Expand the name of the chart.
*/}}
{{- define "chart.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "chart.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Allow the release namespace to be overridden for multi-namespace deployments in combined charts
*/}}
{{- define "chart.namespace" -}}
{{- if .Values.namespaceOverride -}}
{{- .Values.namespaceOverride -}}
{{- else -}}
{{- .Release.Namespace -}}
{{- end -}}
{{- end -}}

{{/*
Generate basic labels
*/}}
{{- define "chart.labels" -}}
{{- if .Values.commonLabels -}}
{{- toYaml .Values.commonLabels -}}
{{- end -}}
{{- end -}}

{{/*
Selector labels
*/}}
{{- define "chart.selectorLabels" -}}
app.kubernetes.io/name: {{ include "chart.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the cluster-ctrl service account to use
*/}}
{{- define "chart.serviceAccountNameCluster" -}}
{{- if .Values.clusterCtrl.rbacAuthorization.serviceAccount.enabled }}
{{- default (include "chart.fullname" .) .Values.clusterCtrl.rbacAuthorization.serviceAccount.name }}-cluster
{{- else }}
{{- default "default" .Values.clusterCtrl.rbacAuthorization.serviceAccount.name }}-cluster
{{- end }}
{{- end }}

{{/*
Create the name of the service-ctrl service account to use
*/}}
{{- define "chart.serviceAccountNameService" -}}
{{- if .Values.serviceCtrl.rbacAuthorization.serviceAccount.enabled }}
{{- default (include "chart.fullname" .) .Values.serviceCtrl.rbacAuthorization.serviceAccount.name }}-service
{{- else }}
{{- default "default" .Values.serviceCtrl.rbacAuthorization.serviceAccount.name }}-service
{{- end }}
{{- end }}

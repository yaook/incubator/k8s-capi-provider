"""A setuptools based setup module.

https://packaging.python.org/guides/distributing-packages-using-setuptools/
"""

import pathlib

from setuptools import find_packages, setup

here = pathlib.Path(__file__).parent.resolve()

# Get the long description from the README file
long_description = (here / "README.md").read_text(encoding="utf-8")

install_requires = [
    "click~=8.1",
    "requests~=2.28",
    "pyyaml~=6.0",
    "datamodel-code-generator~=0.13",
    "loguru~=0.6",
    "kubernetes~=24.2",
]

setup(
    name="yaookctl",
    description="The Yaook command-line tool yaookctl"
    " allows you to manage workload Kubernetes clusters",
    long_description=long_description,
    long_description_content_type="text/markdown",
    version="0.1.0",
    license="Apache License 2.0",
    author="Yaook Authors",
    url="https://gitlab.com/yaook/incubator/k8s-capi-provider/-/tree/devel/yaookctl",
    python_requires=">=3.8",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "Topic :: Utilities",
        "License :: OSI Approved :: Apache Software License",
        "Programming Language :: Python :: 3.8",
        "Operating System :: POSIX :: Linux",
    ],
    keywords="yaook, mk8s, cli",
    packages=find_packages(),
    install_requires=install_requires,
    entry_points={
        "console_scripts": [
            "yaookctl = yaookctl.cli:yaookctl",
        ],
    },
)

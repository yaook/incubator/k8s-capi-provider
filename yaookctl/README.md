# yaookctl

Welcome to the yaookctl tool!

This covers a very first implementation of k8s-capi-provider yaookctl CLI.

## Installation

As yaookctl is not yet on the Python Package Index (PyPI), therefore
you should install it from source.
Installing it into a [Python3 virtualenv][virtualenv] is recommended.

```shell
python3 -m pip install "git+https://gitlab.com/yaook/incubator/k8s-capi-provider.git@devel#egg=yaookctl&subdirectory=yaookctl"
```

## Documentation

Documentation is currently available at https://yaook.gitlab.io/incubator/k8s-capi-provider/yaookctl.

The documentation is created with [mdBook](https://github.com/rust-lang/mdBook) and located under [docs/](docs/).
Built instructions for the documentation can be found in the [respective README](docs/README.md).

<!-- References -->

[virtualenv]: https://virtualenv.pypa.io/en/stable

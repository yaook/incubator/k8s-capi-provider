#!/usr/bin/env python3

"""This script prints logs of managed clusters.

.. code-block:: bash
    cluster <cluster name>  [--namespace <cluster namespace>]
        [--ctrl <source of logs i.e. controller>] [--log-level <log level>]
        [--follow]
"""
import click
from kubernetes.client.rest import ApiException
from kubernetes.stream import stream
from urllib3.exceptions import HTTPError

from ...utils import kubernetes_client, with_client_v1


def exec_commands(
    commands: list,
    pod_name: str,
    pod_namespace: str,
    follow: bool = False,
    timeout: int = 3600,
):
    """Execute commands in the given POD.

    Args:
        commands (list): Commands to be execute.
        pod_name (str): Pod name where the command will be executed.
        pod_namespace (str): Pod namespace where the command will be executed.
        follow (bool, optional): Whether the logs will be streamed or not.
            Defaults to False.
        timeout (int, optional): Readline timeout. Defaults to 3600s [1h].

    """
    try:
        with kubernetes_client() as client:
            resp = stream(
                client.connect_get_namespaced_pod_exec,
                pod_name,
                pod_namespace,
                command=commands,
                stdout=True,
                stderr=True,
                stdin=False,
                tty=False,
                _preload_content=not follow,
            )
    except (ApiException, HTTPError) as e:
        raise click.ClickException(f"Unable to read cluster logs, error: {str(e)}")

    if follow:
        while resp.is_open():
            click.echo(resp.readline_stdout(timeout=timeout))

        resp.close()
    else:
        click.echo(resp)


@click.command(
    context_settings=dict(help_option_names=["-h", "--help"]),
    short_help="Print cluster logs.",
)
@click.argument(
    "name",
)
@click.option(
    "--namespace",
    "-n",
    default="default",
    show_default=True,
    help="Cluster namespace",
)
@click.option(
    "--source",
    "-s",
    default="cluster",
    show_default=True,
    type=click.Choice(["cluster", "service"], case_sensitive=False),
    help="Log source. The controller which will be fetched",
)
@click.option(
    "--log-level",
    "-l",
    default="info",
    show_default=True,
    type=click.Choice(["info", "warning", "error"], case_sensitive=False),
    help="Specify log level",
)
@click.option(
    "--follow",
    "-f",
    type=click.BOOL,
    is_flag=True,
    show_default=True,
    default=False,
    help="Specify if the logs should be streamed",
)
@with_client_v1
def cluster(ctx, client, name, namespace, source, log_level, follow):
    """Print cluster logs.

    NAME is the name of the cluster.
    """
    try:
        ctrl = client.list_pod_for_all_namespaces(
            label_selector=f"app=k8s-capi-provider-{source}"
        )
    except (ApiException, HTTPError) as e:
        raise click.ClickException(f"Unable to read cluster logs, error: {e.reason}")

    try:
        ctrl_obj, *_ = ctrl.items
    except ValueError:
        raise click.ClickException(
            "Unable to read cluster logs, error: Controller pod not found"
        )

    commands = [
        "/bin/sh",
        "-c",
        f"{'tail -F' if follow else 'cat'} "
        f"/app/clusters/{namespace}/{name}/logs/{log_level}.log",
    ]

    exec_commands(
        commands,
        ctrl_obj.metadata.name,
        ctrl_obj.metadata.namespace,
        follow,
    )


if __name__ == "__main__":
    cluster()

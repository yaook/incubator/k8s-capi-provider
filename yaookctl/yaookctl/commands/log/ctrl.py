#!/usr/bin/env python3

"""This script prints logs of k8s-capi-provider controllers.

.. code-block:: bash
    ctrl <controller name> [--follow]
"""
import click
from kubernetes import watch
from kubernetes.client.rest import ApiException
from urllib3.exceptions import HTTPError

from ...utils import with_client_v1


@click.command(
    context_settings=dict(help_option_names=["-h", "--help"]),
    short_help="Print controller logs.",
)
@click.argument(
    "name",
    type=click.Choice(["cluster", "service"], case_sensitive=False),
)
@click.option(
    "--follow",
    "-f",
    type=click.BOOL,
    is_flag=True,
    show_default=True,
    default=False,
    help="Specify if the logs should be streamed",
)
@with_client_v1
def ctrl(ctx, client, name, follow):
    """Print controller logs.

    NAME is the name of the controller.
    """
    try:
        _ctrl = client.list_pod_for_all_namespaces(
            label_selector=f"app=k8s-capi-provider-{name}"
        )
    except (ApiException, HTTPError) as e:
        raise click.ClickException(f"Unable to read controller logs, error: {e.reason}")

    try:
        ctrl_obj, *_ = _ctrl.items
    except ValueError:
        raise click.ClickException(
            "Unable to read controller logs, error: Controller pod not found",
        )

    if follow:
        k8s_watch = watch.Watch()
        try:
            for line in k8s_watch.stream(
                client.read_namespaced_pod_log,
                name=ctrl_obj.metadata.name,
                namespace=ctrl_obj.metadata.namespace,
            ):
                click.echo(line)

        except (ApiException, HTTPError) as e:
            raise click.ClickException(
                f"Unable to read controller logs, error: {e.reason}"
            )

    else:
        try:
            resp = client.read_namespaced_pod_log(
                name=ctrl_obj.metadata.name, namespace=ctrl_obj.metadata.namespace
            )
        except (ApiException, HTTPError) as e:
            raise click.ClickException(
                f"Unable to read controller logs, error: {e.reason}"
            )

        click.echo(resp)


if __name__ == "__main__":
    ctrl()

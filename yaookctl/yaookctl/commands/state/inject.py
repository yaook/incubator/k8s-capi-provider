#!/usr/bin/env python3

"""This script injects cluster state to k8s-capi-provider LCM.

The cluster state files could be extracted from yaook/k8s LCM and used in
k8s-capi-provider LCM. It means that the workload cluster could switch its
LCM tool if needed. This script creates necessary yaook cluster CRs/secrets
based on provided yaook/k8s state files.

This script does the following:

1. Generates cluster manifests based on the yaook/k8s cluster files.
   The following yaook/k8s cluster files are used as a source:
   - config.toml
   - .envrc
   - openrc
2. Generates cluster state secrets based on the yaook/k8s cluster
   files. The following yaook/k8s cluster dirs are used as a source:
   - terraform state and vars
   - ansible inventory
   - config directory
   Secrets are used as a persistent storage backend of k8s-capi-provider LCM.
3. Applies cluster CRs (step 1.) and secrets (step 2.) to the management
   cluster where k8s-capi-provider lives.
   Then, the k8s-capi-provider takes over the workload cluster.

.. code-block:: bash
    inject.py --yk8s <path to the yaook/k8s cluster directory>
        --toml-config <custom config toml file>
        --envrc <custom .envrc file>
        --openrc <custom openrc file>
        [--output <output directory>]
        [--dry-run]

"""
import base64
import gzip
import re
import shutil
import tarfile
from itertools import zip_longest
from pathlib import Path
from typing import List, NamedTuple, Union

import click
import toml
import yaml
from kubernetes.client import CustomObjectsApi
from kubernetes.client.rest import ApiException
from urllib3.exceptions import HTTPError

from ...utils import kubernetes_client
from .utils import States

CR_APIVERSION = "yaook.mk8s.io/v1"

CLUSTER_KIND = "YaookCluster"
CONTROL_PLANE_KIND = "YaookControlPlane"
MACHINE_DEPLOYMENT_KIND = "YaookMachineDeployment"
MACHINE_TEMPLATE_KIND = "YaookMachineTemplate"
CONFIG_TEMPLATE_KIND = "YaookConfigTemplate"
SERVICE_KIND = "YaookService"

DEFAULT_AZS = ["AZ1", "AZ2", "AZ3"]

DEFAULT_CONTROL_PLANE_COUNT = 3
DEFAULT_CONTROL_PLANE_MACHINE_FLAVOR = "M"
DEFAULT_CONTROL_PLANE_IMAGE_NAME = "Ubuntu 20.04 LTS x64"

DEFAULT_WORKER_COUNT = 4
DEFAULT_WORKER_MACHINE_FLAVOR = "M"
DEFAULT_WORKER_IMAGE_NAME = "Ubuntu 20.04 LTS x64"

# The provider identity should not be created by k8s-capi-provider when
# the cluster was injected (by `yaookctl inject`) from yaook/k8s LCM.
# Then, the origin yaook/k8s ssh/gpg/wg identities (keys) are
# used as a provider one and should not be created again.
# MANAGED_PROVIDER_IDENTITY=False disables the provider identity management.
MANAGED_PROVIDER_IDENTITY = False

ARCHIVE_FOLDER = "archives"
CLUSTER_MANIFEST_FILENAME = "cluster.yaml"
CLUSTER_STATES_FILENAME = "states.yaml"


# FIXME: LCM schema keys taken from CRD generator.
#   Use single source of truth here, or better, allow any .envrc
#   key=value pair in YaookCluster.spec.lcm section, if possible.
LCM_SCHEMA_KEYS = [
    "MANAGED_PROVIDER_IDENTITY",
    "wg_conf_name",
    "wg_user",
    "MANAGED_K8S_RELEASE_THE_KRAKEN",
    "WG_USAGE",
    "WG_COMPANY_USERS",
    "PASS_COMPANY_USERS",
    "SSH_COMPANY_USERS",
    "TF_VAR_keypair",
]
KEY_FILER = []
KEY_FILER.extend(LCM_SCHEMA_KEYS)

# Pattern to extract env key=value pairs
RE_ENV = re.compile(
    r"^(?:export[^\S\r\n]+)?(?P<key>[\w\-\.]+)\=[\"\']?(?P<value>(.*?))[\"\']?$",
    re.MULTILINE | re.UNICODE | re.IGNORECASE,
)


class OverrideArchiveMember(NamedTuple):
    """Data structure defines archive member which overrides default archive member."""

    name: str
    path: Path


class ProviderIdentityMember(NamedTuple):
    """Data structure defines provider identity member."""

    name: str
    path: Path


def compress_artifact(
    artifact: Path, output: Path, override: OverrideArchiveMember = None
) -> Path:
    """Compress an artifact.

    Creates .tar.gz archive from an existing directory or
    .gz archive from file.

    Args:
        artifact (Path): Path to the artifact that should be compressed.
        output (Path): Output path.
        override (OverrideArchiveMember, optional): Archive member that
            overrides the default one.

    Returns:
        Path: Compressed artifact filename

    """
    output.parent.mkdir(exist_ok=True)
    if artifact.is_file():
        output = output.with_name(output.name + ".gz")

        with open(artifact, "rb") as file:
            with gzip.open(output, "wb") as gzipfile:
                shutil.copyfileobj(file, gzipfile)

    elif artifact.is_dir():
        output = output.with_name(output.name + ".tar.gz")

        with tarfile.open(output, "w:gz") as tar:
            for item in artifact.glob("*"):
                if override and override.name == item.name:
                    tar.add(
                        str(override.path), arcname=f"{artifact.name}/{override.name}"
                    )
                else:
                    tar.add(str(item), arcname=f"{artifact.name}/{item.name}")

    return output


def load_file(
    filename: str,
    is_toml: bool = False,
    is_pairs: bool = False,
    filter_keys: bool = False,
) -> Union[dict, str]:
    """Load file.

    Args:
        filename (str): File to be loaded.
        is_toml (bool): Whether the file is in toml format.
        is_pairs (bool): Whether the file content should be parsed.
        filter_keys (bool): Whether the keys should be filtered,
            based on `KEY_FILER` key list.

    Returns:
        Union[dict, str]: Loaded file content.

    """
    try:
        with open(filename) as fd:
            if is_toml:
                return toml.load(fd)

            if is_pairs:
                return parse_pairs(fd.read(), filter_keys=filter_keys)

            return fd.read()

    except FileNotFoundError:
        raise click.ClickException(
            f"{filename} not found. Please define another file path."
        )


def get_azs(azs: list, replicas: int) -> list:
    """Get availability zones based on replica count.

    Extends number of availability zone items in list based
    on replica count. Useful when the default azs should be
    applied.

    Args:
        azs (list): Availability zone list.
        replicas (int): Number of replicas.

    Returns:
        list: Availability zone list which match replica count.

    """
    return (azs * replicas)[:replicas]


def get_worker_flavors(replicas: int) -> list:
    """Get worker flavours list based on replica count.

    Args:
        replicas (int): Number of replicas.

    Returns:
        list: Flavour list which match replica count.

    """
    return [DEFAULT_WORKER_MACHINE_FLAVOR] * replicas


def get_worker_images(replicas: int):
    """Get worker images list based on replica count.

    Args:
        replicas (int): Number of replicas.

    Returns:
        list: Image list which match replica count.

    """
    return [DEFAULT_WORKER_IMAGE_NAME] * replicas


def strtobool(val: str) -> Union[bool, str]:
    """Convert a string representation to bool.

    True value it 'true'; false value is 'false'.
    Returns 'val' is anything else.

    Args:
        val (str): String to be converted to bool.

    Returns:
        bool, str: Converted string if value it 'true' or 'false'.

    """
    if val.lower() == "true":
        return True

    elif val.lower() == "false":
        return False

    return val


def parse_values(_dict: dict) -> dict:
    """Parse nested dictionary values.

    It applies :func:`strtobool` to nested dictionary
        values.

    Args:
        _dict (dict): Dictionary to be parsed.

    Returns:
        dict: Parsed dictionary.

    """
    if isinstance(_dict, dict):
        for key, value in _dict.items():
            if isinstance(value, dict):
                _dict[key] = parse_values(value)
            elif isinstance(value, str):
                _dict[key] = strtobool(value)
            else:
                _dict[key] = value

    return _dict


def get_key(config: dict, key: str) -> dict:
    """Get key from config and parse its value.

    It applies :func:`parse_values` to the `config`.
    It removes empty values and related keys from
    the config.

    Args:
        config (dict): Config, that may contain the requested key.
        key (str): Requested config key.

    Returns:
        dict: Parsed config value.

    """
    parsed = {}
    for key, value in config.get(key, {}).items():

        if value not in ({}, [], "", None):
            parsed[key] = parse_values(value)

    return parsed


def parse_pairs(content: str, filter_keys: bool = False) -> dict:
    r"""Parse env key=value pairs from content.

    Example:
         .. code:: python

         content = \"\"\"
         foo=buzz
         export foo
         export foo_buzz=1
         \"\"\"
         assert parse_pairs(content) == {'foo': 'buzz', 'foo_buzz': '1'}

    Args:
        content (str): Content to be parsed.
        filter_keys (bool): Whether the keys should be filtered,
            based on `KEY_FILER` key list.

    Returns:
        dict: Parsed key=value pairs.

    """
    pairs = {}

    for entry in RE_ENV.finditer(content):
        if filter_keys and entry.group("key") not in KEY_FILER:
            continue

        pairs.update({entry.group("key"): strtobool(entry.group("value"))})

    return pairs


def get_machine_cr(name: str, config: dict) -> List[dict]:
    """Generate machine CRs.

    Generate machine deployment CR and corresponding machine
    template CR per each worker node. The same logic is used in
    the daiteap web platform backend.

    Args:
        name (str): Cluster name.
        config (dict): Cluster config.toml configuration.

    Returns:
        List[dict]: List of machine deployment CRs and corresponding
            machine template CRs.

    """
    # Worker variables
    if not (worker_count := config.get("terraform", {}).get("workers")):
        worker_count = DEFAULT_WORKER_COUNT

    if not (worker_azs := config.get("terraform", {}).get("worker_azs")):
        worker_azs = get_azs(
            azs=config.get("terraform", {}).get("azs", DEFAULT_AZS),
            replicas=worker_count,
        )

    if not (worker_flavors := config.get("terraform", {}).get("worker_flavors")):
        worker_flavors = get_worker_flavors(replicas=worker_count)

    if not (worker_images := config.get("terraform", {}).get("worker_images")):
        worker_images = get_worker_images(replicas=worker_count)

    # Check whether azs, flavours and images have the same length as worker count
    for list_name, list_len in (
        ("azs", len(worker_azs)),
        ("flavors", len(worker_flavors)),
        ("images", len(worker_images)),
    ):
        if worker_count != list_len:
            raise click.ClickException(
                f"{list_name} length {list_len} not equal worker count {worker_count}."
                " Lengths must be the same"
            )

    # Generate machine deployment CR and corresponding machine template
    # CR per each worker node
    machine_deployment_crs: list = []
    machine_template_crs: list = []

    for no, az, flavor, image in zip(
        range(worker_count), worker_azs, worker_flavors, worker_images
    ):
        machine_deployment_crs.append(
            {
                "apiVersion": CR_APIVERSION,
                "kind": MACHINE_DEPLOYMENT_KIND,
                "metadata": {"name": f"{name}-md-{no}"},
                "spec": {
                    "clusterName": name,
                    "replicas": 1,
                    "template": {
                        "spec": {
                            "clusterName": name,
                            "bootstrap": {
                                "configRef": {
                                    "kind": CONFIG_TEMPLATE_KIND,
                                    "apiVersion": CR_APIVERSION,
                                    "name": name,
                                }
                            },
                            "infrastructureRef": {
                                "kind": MACHINE_TEMPLATE_KIND,
                                "apiVersion": CR_APIVERSION,
                                "name": f"{name}-md-{no}",
                            },
                            "failureDomain": az,
                        }
                    },
                },
            }
        )

        machine_template_crs.append(
            {
                "apiVersion": CR_APIVERSION,
                "kind": MACHINE_TEMPLATE_KIND,
                "metadata": {"name": f"{name}-md-{no}"},
                "spec": {
                    "flavor": flavor,
                    "image": image,
                },
            }
        )

    return machine_deployment_crs + machine_template_crs


def get_cluster_cp_crs(name: str, config: dict, envrc: dict) -> List[dict]:
    """Generate cluster and control plane CRs.

    Args:
        name (str): Cluster name.
        config (dict): Cluster config.toml configuration.
        envrc (dict): Cluster envrc configuration.

    Returns:
        List[dict]: List of cluster CR, control plane CR and corresponding
            config template CR.

    """
    # Control plane variables
    if not (control_plane_count := config.get("terraform", {}).get("masters")):
        control_plane_count = DEFAULT_CONTROL_PLANE_COUNT

    if not (control_plane_azs := config.get("terraform", {}).get("master_azs")):
        control_plane_azs = get_azs(
            azs=config.get("terraform", {}).get("azs", DEFAULT_AZS),
            replicas=control_plane_count,
        )

    # k8s-capi-provider supports only single flavor/image for the whole control plane
    if control_plane_flavors := config.get("terraform", {}).get("master_flavors"):
        control_plane_flavor = control_plane_flavors[0]
    else:
        control_plane_flavor = DEFAULT_CONTROL_PLANE_MACHINE_FLAVOR

    if control_plane_images := config.get("terraform", {}).get("master_images"):
        control_plane_image = control_plane_images[0]
    else:
        control_plane_image = DEFAULT_CONTROL_PLANE_IMAGE_NAME
    # LCM variables
    lcm = envrc
    lcm.update({"MANAGED_PROVIDER_IDENTITY": MANAGED_PROVIDER_IDENTITY})
    cluster_cr = {
        "apiVersion": CR_APIVERSION,
        "kind": CLUSTER_KIND,
        "metadata": {"name": name},
        "spec": {
            "terraform": get_key(config, "terraform"),
            "wireguard": get_key(config, "wireguard"),
            "ipsec": get_key(config, "ipsec"),
            "cah-users": get_key(config, "cah-users"),
            "passwordstore": get_key(config, "passwordstore"),
            "miscellaneous": get_key(config, "miscellaneous"),
            "load-balancing": get_key(config, "load-balancing"),
            # special fields that aren't part of config.toml
            "controlPlaneRef": {
                "kind": CONTROL_PLANE_KIND,
                "apiVersion": CR_APIVERSION,
                "name": f"{name}-control-plane",
            },
            "controlPlaneAvailabilityZones": control_plane_azs,
            "openStackRC": {"secretRef": f"{name}-openrc"},
            "lcm": lcm,
        },
    }

    control_plane_cr = {
        "apiVersion": CR_APIVERSION,
        "kind": CONTROL_PLANE_KIND,
        "metadata": {"name": f"{name}-control-plane"},
        "spec": {
            "replicas": control_plane_count,
            "version": config["kubernetes"]["version"],
            "machineTemplate": {
                "infrastructureRef": {
                    "kind": MACHINE_TEMPLATE_KIND,
                    "apiVersion": CR_APIVERSION,
                    "name": f"{name}-control-plane",
                }
            },
            "bootstrap": {
                "configRef": {
                    "kind": CONFIG_TEMPLATE_KIND,
                    "apiVersion": CR_APIVERSION,
                    "name": name,
                }
            },
        },
    }

    machine_template_control_plane_cr = {
        "apiVersion": CR_APIVERSION,
        "kind": MACHINE_TEMPLATE_KIND,
        "metadata": {"name": f"{name}-control-plane"},
        "spec": {
            "flavor": control_plane_flavor,
            "image": control_plane_image,
        },
    }

    config_template_cr = {
        "apiVersion": CR_APIVERSION,
        "kind": CONFIG_TEMPLATE_KIND,
        "metadata": {"name": name},
        "spec": {
            "kubernetes": get_key(config, "kubernetes"),
            "ch-k8s-lbaas": get_key(config, "ch-k8s-lbaas"),
            "node-scheduling": get_key(config, "node-scheduling"),
            "testing": get_key(config, "testing"),
        },
    }

    return [
        cluster_cr,
        control_plane_cr,
        machine_template_control_plane_cr,
        config_template_cr,
    ]


def get_service_cr(name: str, config: dict) -> dict:
    """Generate service CR.

    Args:
        name (str): Cluster name.
        config (dict): Cluster config.toml configuration.

    Returns:
        dict: Service CR.

    """
    return {
        "apiVersion": CR_APIVERSION,
        "kind": SERVICE_KIND,
        "metadata": {"name": f"{name}-service"},
        "spec": {
            "clusterName": name,
            "k8s-service-layer": get_key(config, "k8s-service-layer"),
        },
    }


def get_openrc_secret(name: str, openrc: dict) -> dict:
    """Generate openrc secret.

    Note: Only one OpenStack authentication method should be used.
      The authentication method is defined by `OS_AUTH_TYPE` variable, but
      be aware that the `OS_PROJECT_NAME` or `OS_PROJECT_ID` should not be
      specified when `v3applicationcredential` is used.

    Args:
        name (str): Cluster name.
        openrc (dict): OpenRC configuration.

    Returns:
        dict: OpenRC secret.

    """
    if openrc.get("OS_AUTH_TYPE") == "v3applicationcredential":
        auth = "v3applicationcredential"

    elif openrc.get("OS_AUTH_TYPE") == "v3password" or not (
        openrc.get("OS_USERNAME") is None
    ):
        auth = "v3password"

    else:
        raise click.ClickException("OpenStack authentication method is not defined")

    return {
        "apiVersion": "v1",
        "kind": "Secret",
        "metadata": {
            "name": f"{name}-openrc",
            "labels": {
                "auth": auth,
            },
        },
        "type": "Opaque",
        "stringData": openrc,
    }


def apply_manifests(manifests: List, namespace: str) -> None:
    """Apply manifests.

    It expects that the manifests bundle contains only `yaaok`
    custom resources or secrets.

    Args:
        manifests (list): List of manifests.
        namespace (str): Manifest namespace.

    """
    for manifest in manifests:
        name = manifest["metadata"]["name"]

        if manifest["apiVersion"].startswith("yaook"):
            cr_plural = manifest["kind"].lower() + "s"
            try:
                with kubernetes_client(client=CustomObjectsApi) as client:
                    client.create_namespaced_custom_object(
                        namespace=namespace,
                        plural=cr_plural,
                        version="v1",
                        group="yaook.mk8s.io",
                        body=manifest,
                    )
            except (ApiException, HTTPError) as e:
                # Conflict
                if hasattr(e, "status") and e.status == 409:  # type:ignore
                    try:
                        with kubernetes_client(client=CustomObjectsApi) as client:
                            client.patch_namespaced_custom_object(
                                name=name,
                                namespace=namespace,
                                plural=cr_plural,
                                version="v1",
                                group="yaook.mk8s.io",
                                body=manifest,
                            )
                    except (ApiException, HTTPError) as patch_e:
                        raise click.ClickException(
                            f"Unable to patch CR {name}," f" error: {str(patch_e)}"
                        )
                else:
                    raise click.ClickException(
                        f"Unable to create CR {name}," f" error: {str(e)}"
                    )
        else:
            try:
                with kubernetes_client() as client:
                    client.create_namespaced_secret(namespace=namespace, body=manifest)
            except (ApiException, HTTPError) as e:
                # Conflict
                if hasattr(e, "status") and e.status == 409:  # type:ignore
                    try:
                        with kubernetes_client() as client:
                            client.patch_namespaced_secret(
                                name=name, namespace=namespace, body=manifest
                            )
                    except (ApiException, HTTPError) as patch_e:
                        raise click.ClickException(
                            f"Unable to patch secret {name}," f" error: {str(patch_e)}"
                        )
                else:
                    raise click.ClickException(
                        f"Unable to create secret {name}," f" error: {str(e)}"
                    )
        click.echo(
            f"Cluster {manifest['kind'].lower()} manifest {name}"
            " has been successfully applied"
        )


@click.command(
    context_settings=dict(help_option_names=["-h", "--help"]),
    short_help="Inject cluster to k8s-capi-provider LCM.",
)
@click.argument(
    "name",
)
@click.option(
    "--namespace",
    "-n",
    default="default",
    show_default=True,
    help="Cluster namespace",
)
@click.option(
    "--yk8s",
    "-y",
    required=True,
    type=click.Path(exists=True, dir_okay=True),
    help="""
    \b
    The yaook/k8s workload cluster directory path.
    yaookctl expects that the following yaook/k8s files and
    directories have default filenames:
     - config/*
     - inventory/*
     - terraform/*
     - .envrc
     - openrc file is searched based on the `*openrc.sh` suffix

     If you want to use custom paths for the above, see CLI override options.
     """,
)
@click.option(
    "--ssh-private-key",
    "-ssh",
    required=True,
    type=click.Path(exists=True),
    help="Path to the SSH private key file.",
)
@click.option(
    "--wg-key",
    "-wg",
    required=True,
    type=click.Path(exists=True),
    help="Path to the wireguard private key file.",
)
@click.option(
    "--gpg-home",
    "-gpg",
    required=True,
    type=click.Path(exists=True),
    help="Path to the gpg home directory.",
)
@click.option(
    "--output",
    "-o",
    type=click.Path(),
    help="The output directory path. $PWD/<cluster-name>/ if not defined.",
)
@click.option(
    "--toml-config",
    "-t",
    type=click.Path(exists=True),
    help="Path to the toml config file."
    " Overrides config.toml file located in yk8s directory.",
)
@click.option(
    "--envrc",
    "-erc",
    type=click.Path(exists=True),
    help="Path to the .envrc file. Overrides .envrc file located in yk8s directory.",
)
@click.option(
    "--openrc",
    "-orc",
    type=click.Path(exists=True),
    help="Path to the openrc file. Overrides openrc file located in yk8s directory.",
)
@click.option(
    "--dry-run",
    "-d",
    type=click.BOOL,
    is_flag=True,
    show_default=True,
    default=False,
    help="Don't apply the output to the management cluster.",
)
def inject(
    name,
    namespace,
    yk8s,
    output,
    toml_config,
    envrc,
    openrc,
    ssh_private_key,
    wg_key,
    gpg_home,
    dry_run,
):
    """Inject cluster to k8s-capi-provider LCM.

    NAME is the name of the cluster.
    """
    states = States()
    if not output:
        # Use the $PWD/<cluster-name> as a output dir
        output = Path().absolute() / name
        output.mkdir(exist_ok=True)

    # Load file paths
    config_file = (
        toml_config
        if toml_config
        else Path(yk8s) / states.config_toml.path / "config.toml"
    )
    envrc_file = envrc if envrc else Path(yk8s) / states.envrc.path
    if openrc:
        openrc_file = openrc
    else:
        openrc_files = Path(yk8s).glob("*openrc.sh")
        try:
            openrc_file = next(openrc_files)
            click.echo(f"Detected openrc file {openrc_file}. Will be used in inject.")
        except StopIteration:
            raise click.ClickException(
                f"yaookctl did not find any *openrc.sh file in"
                f" {Path(yk8s).absolute()} directory."
                " Use --openrc option to define the openrc file."
            )

    # Generate cluster manifests
    config_content = load_file(config_file, is_toml=True)
    envrc_content = load_file(envrc_file, is_pairs=True, filter_keys=True)
    openrc_content = load_file(openrc_file, is_pairs=True)
    crs = [
        *get_cluster_cp_crs(name, config_content, envrc_content),
        *get_machine_cr(name, config_content),
        get_service_cr(name, config_content),
        get_openrc_secret(name, openrc_content),
    ]
    output_manifest = output / CLUSTER_MANIFEST_FILENAME
    with open(output_manifest, "w") as fd:
        yaml.dump_all(crs, fd)

    click.echo(f"Cluster manifests have been successfully saved to {output_manifest}")

    # Generate cluster state secrets
    secrets = []
    for artifact, override in zip_longest(
        [
            states.config_toml,
            states.inventory,
            states.terraform_tfstate,
            states.terraform_tfvars,
            ProviderIdentityMember(name="provider-ssh-key", path=Path(ssh_private_key)),
            ProviderIdentityMember(name="wg-key", path=Path(wg_key)),
            ProviderIdentityMember(name="gnupg", path=Path(gpg_home)),
        ],
        [
            OverrideArchiveMember(name="config.toml", path=Path(config_file)),
        ],
    ):
        if isinstance(artifact, ProviderIdentityMember):
            output_path = output / ARCHIVE_FOLDER / artifact.name
            artifact_path = artifact.path
        else:
            output_path = output / ARCHIVE_FOLDER / artifact.path
            artifact_path = Path(yk8s) / artifact.path

        compressed_file = compress_artifact(
            artifact=artifact_path,
            output=output_path,
            override=override,
        )

        with open(compressed_file, "rb") as fd:
            encoded_string = base64.b64encode(fd.read()).decode()

        secrets.append(
            {
                "apiVersion": "v1",
                "kind": "Secret",
                "metadata": {"name": f"{name}-{artifact.name}"},
                "type": "Opaque",
                "data": {compressed_file.name: encoded_string},
            }
        )

    output_state_manifest = output / CLUSTER_STATES_FILENAME
    with open(output_state_manifest, "w") as fd:
        yaml.dump_all(secrets, fd)

    click.echo(
        f"Cluster state secrets have been successfully saved to {output_state_manifest}"
    )

    if not dry_run:
        apply_manifests(secrets + crs, namespace)


if __name__ == "__main__":
    inject()

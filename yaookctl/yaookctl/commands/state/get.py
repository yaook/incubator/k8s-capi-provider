#!/usr/bin/env python3

"""This script gets cluster state from k8s-capi-provider persistent storage.

The cluster state files could be used in yaook/k8s LCM. It means that the
workload cluster could switch its LCM tool if needed.

Get all cluster files:

.. code-block:: bash
    get cluster <cluster name> [--namespace <cluster namespace>]
        [--output <output directory>]

Get cluster kubeconfig file:

.. code-block:: bash
    get kubeconfig <cluster name> [--namespace <cluster namespace>]
        [--output <output filename>]

Get cluster config.toml file:

.. code-block:: bash
    get config <cluster name> [--namespace <cluster namespace>]
        [--output <output filename>]

Get cluster OpenRC file:

.. code-block:: bash
    get openrc <cluster name> [--namespace <cluster namespace>]
        [--output <output filename>]

Get cluster wireguard config file:

.. code-block:: bash
    get wireguard <cluster name> --identity <peer-identity>
        [--namespace <cluster namespace>] [--output <output filename>]

"""
import gzip
import io
import os
import tarfile
from pathlib import Path

import click

from .utils import States


def uncompress_data(
    key: str, data: bytes, target_path: Path = None, member: str = None
):
    """Uncompress data.

    Function could uncompress .tar.gz, gz archives or only selected file
    (Member) from .tar.gz archive.

    target_path has to be defined if while tar.gz archives should be uncompressed.

    Args:
        key (str): Data key (name).
        data (bytes): Compressed data.
        target_path (Path, optional): Target path for uncompressed data.
        member (str, optional): Extract only a defined member
            from the archive.

    Raises:
        click.ClickException: If compression failed.

    """
    if key.endswith(".tar.gz"):
        with tarfile.open(fileobj=io.BytesIO(data)) as tar:
            if member:
                try:
                    file_obj = tar.extractfile(tar.getmember(member))
                except KeyError as e:
                    raise click.ClickException(
                        f"Unable to get {member} from archive, error: {e}"
                    )
                if file_obj:
                    if target_path:
                        try:
                            with open(target_path, "wb") as file:
                                file.write(file_obj.read())
                        except OSError as e:
                            raise click.ClickException(
                                f"Unable to extract archive to {target_path},"
                                f" error: {e}"
                            )
                    else:
                        click.echo(file_obj.read())

            else:
                assert target_path, "target_path should be defined"
                os.makedirs(os.path.dirname(target_path), exist_ok=True)
                try:
                    tar.extractall(
                        path=os.path.dirname(target_path),
                    )
                except OSError as e:
                    raise click.ClickException(
                        f"Unable to extract archive to {target_path}, error: {e}"
                    )

    elif key.endswith(".gz"):
        decompressed = gzip.decompress(data)

        if target_path:
            os.makedirs(os.path.dirname(target_path), exist_ok=True)
            try:
                with open(target_path, "wb") as file:
                    file.write(decompressed)
            except OSError as e:
                raise click.ClickException(
                    f"Unable to extract archive to {target_path}, error: {e}"
                )

        else:
            click.echo(decompressed)


@click.group(context_settings=dict(help_option_names=["-h", "--help"]))
def get():
    """Get cluster files."""


@get.command(
    context_settings=dict(help_option_names=["-h", "--help"]),
    short_help="Get all cluster files.",
)
@click.argument(
    "name",
)
@click.option(
    "--namespace",
    "-n",
    default="default",
    show_default=True,
    help="Cluster namespace",
)
@click.option(
    "--output",
    "-o",
    type=click.Path(),
    help="The output directory path." " $PWD/<cluster-name>/ if not defined.",
)
def cluster(name, namespace, output):
    """Get all cluster files.

    NAME is the name of the cluster.
    """
    if not output:
        # Use the $PWD/<cluster-name> as a output dir
        output = Path().absolute() / name

    for state in States():
        target_path = output / state.path

        if state.string_data:
            with open(target_path, "w") as fd:
                for _, data in state.get_data(name, namespace, output):
                    fd.write(data)
        else:
            try:
                key, data = next(state.get_data(name, namespace, output))
            except StopIteration:
                # Non required state
                continue

            uncompress_data(key, data, target_path=target_path)

        click.echo(f"{target_path.name} has been successfully saved to {target_path}")


@get.command(
    context_settings=dict(help_option_names=["-h", "--help"]),
    short_help="Get cluster kubeconfig file.",
)
@click.argument(
    "name",
)
@click.option(
    "--namespace",
    "-n",
    default="default",
    show_default=True,
    help="Cluster namespace",
)
@click.option("--output", "-o", type=click.Path(), help="The output file path")
def kubeconfig(name, namespace, output):
    """Get cluster kubeconfig file.

    NAME is the name of the cluster.
    """
    key, data = next(States().inventory.get_data(name, namespace, output))
    uncompress_data(key, data, target_path=output, member="inventory/.etc/admin.conf")
    if output:
        click.echo(f"Kubeconfig file has been successfully saved to {output}")


@get.command(
    context_settings=dict(help_option_names=["-h", "--help"]),
    short_help="Get cluster config.toml file.",
)
@click.argument(
    "name",
)
@click.option(
    "--namespace",
    "-n",
    default="default",
    show_default=True,
    help="Cluster namespace",
)
@click.option("--output", "-o", type=click.Path(), help="The output file path")
def config(name, namespace, output):
    """Get cluster config.toml file.

    NAME is the name of the cluster.
    """
    key, data = next(States().config_toml.get_data(name, namespace, output))
    uncompress_data(key, data, target_path=output, member="config/config.toml")
    if output:
        click.echo(f"Config.toml file has been successfully saved to {output}")


@get.command(
    context_settings=dict(help_option_names=["-h", "--help"]),
    short_help="Get cluster OpenRC file.",
)
@click.argument(
    "name",
)
@click.option(
    "--namespace",
    "-n",
    default="default",
    show_default=True,
    help="Cluster namespace",
)
@click.option("--output", "-o", type=click.Path(), help="The output file path")
def openrc(name, namespace, output):
    """Get cluster OpenRC file.

    NAME is the name of the cluster.
    """
    if output:
        with open(output, "w") as fd:
            for _, data in States().openrc.get_data(name, namespace, output):
                fd.write(data)

        click.echo(f"OpenRC file has been successfully saved to {output}")

    else:
        for _, data in States().openrc.get_data(name, namespace):
            click.echo(data, nl=False)


@get.command(
    context_settings=dict(help_option_names=["-h", "--help"]),
    short_help="Get cluster wireguard config file.",
)
@click.argument(
    "name",
)
@click.option("--identity", "-i", required=True, help="Wireguard peer identity name")
@click.option(
    "--namespace",
    "-n",
    default="default",
    show_default=True,
    help="Cluster namespace",
)
@click.option("--output", "-o", type=click.Path(), help="The output file path")
def wireguard(name, identity, namespace, output):
    """Get cluster wireguard config file.

    NAME is the name of the cluster.
    """
    key, data = next(States().inventory.get_data(name, namespace, output))
    uncompress_data(
        key,
        data,
        target_path=output,
        member=f"inventory/.etc/wireguard/wg_{identity}.conf",
    )
    if output:
        click.echo(f"Wireguard config file has been successfully saved to {output}")


if __name__ == "__main__":
    get()

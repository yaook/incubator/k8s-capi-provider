"""State* commands utils module."""
import base64
from pathlib import Path
from typing import Callable, Generator, NamedTuple, Optional, Tuple

import click
from kubernetes.client import CustomObjectsApi
from kubernetes.client.rest import ApiException
from urllib3.exceptions import HTTPError

from ...utils import kubernetes_client


def envrc_extend(
    name: str, output=None, *args, **kwargs
) -> Generator[Tuple[str, str], None, None]:
    """Extend envrc file.

    Callback yields env. variables that should be
    injected to the envrc file. Variables
    ensure that yaook/k8s will be able to do a LCM
    of exported cluster.

    Args:
        name (str): Cluster name.
        output (Path, optional): Output path.

    Yields:
        tuple(str, bytes):  Key placeholder and envrc env. variables.

    """
    yield "", f"""
# Wireguard: Absolute path to your k8s-capi-provider private wireguard key.
wg_private_key_file={Path(Path(output) / ".wireguard" / "wg.key").absolute()}
export wg_private_key_file
# Wireguard: k8s-capi-provider username in the wg-user repository
wg_user={"-".join((name, "k8s-wg@example.com"))}
export wg_user
# OpenStack: k8s-capi-provider merged keypair name uses to bootstrap instances.
TF_VAR_keypair={"-".join((name, "k8s-merged"))}
export TF_VAR_keypair
# Ansible: k8s-capi-provider ssh private key path
AFLAGS="--extra-vars ansible_ssh_common_args=-oIdentitiesOnly=yes --extra-vars ansible_ssh_private_key_file={Path(Path(output) / ".ssh" / "provider_id_rsa").absolute()}"  # noqa: E501
export AFLAGS
# Passwordstore: k8s-capi-provider gpg home directory
PASSWORD_STORE_GPG_OPTS="--homedir {Path(Path(output) / ".gnupg").absolute()}"
export PASSWORD_STORE_GPG_OPTS
# Source cluster OpenRC file
source {Path(Path(output) / "openrc.sh").absolute()}
"""


def data_decode(
    key: str, value: bytes, *args, **kwargs
) -> Generator[Tuple[str, bytes], None, None]:
    """Decode data value.

    Callback yields decoded data value.

    Args:
        key (str): Data key.
        value (bytes): Data value to be decoded.

    Yields:
        tuple(str, bytes): Data key and decoded value.

    """
    yield key, base64.b64decode(value)


def data_export(
    key: str, value: str, *args, **kwargs
) -> Generator[Tuple[str, str], None, None]:
    r"""Export data as env. variable.

    Callback yields data as env. variable.
    Example:
         .. code:: python

         assert list(data_export("key", "value")) == \
         [('key', 'key="value"\n'), ('key', 'export key\n')]


    Args:
        key (str): Data key.
        value (bytes): Data value.

    Yields:
        tuple(str, str): Data key and env. variable.

    """
    if isinstance(value, bool):
        value = str(value).lower()

    yield key, f'{key}="{value}"\n'
    yield key, f"export {key}\n"


def data_export_decode(
    key: str, value: bytes, *args, **kwargs
) -> Generator[Tuple[str, str], None, None]:
    r"""Decode and export data as env. variable.

    Callback yields data as env. variable.
    Example:
         .. code:: python

         assert list(data_export_decode("key", b"dmFsdWU=")) == \
         [('key', 'key="value"\n'), ('key', 'export key\n')]


    Args:
        key (str): Data key.
        value (bytes): Data value to be decoded.

    Yields:
        tuple(str, str): Data key and decoded env. variable.

    """
    value_decoded = base64.b64decode(value).decode()

    if value_decoded in ("True", "False"):
        value_decoded = value_decoded.lower()

    yield key, f'{key}="{value_decoded}"\n'
    yield key, f"export {key}\n"


class StateLCM(NamedTuple):
    """Data structure represents cluster state resource stored in cluster CR."""

    name: str
    path: Path
    required: bool = True  # Whether the state is mandatory
    string_data: bool = False  # Whether the state contains string data
    extend_data: Optional[Callable] = None  # Extend data callback function
    mangle_data: Optional[Callable] = data_decode  # Mangle data callback function

    def get_data(
        self, cluster_name: str, cluster_namespace: str, output: Path = None
    ) -> Generator[tuple, None, None]:
        """Get state data from cluster CR persistent storage.

        Args:
            cluster_name (str): Cluster name.
            cluster_namespace (str): Cluster namespace.
            output (Path, optional): Output path.

        Yields:
            tuple(str, bytes): Key and corresponding decoded bytes.

        """
        name = cluster_name
        namespace = cluster_namespace

        try:
            with kubernetes_client(client=CustomObjectsApi) as client:
                obj = client.get_namespaced_custom_object(
                    name=name,
                    namespace=namespace,
                    plural="yaookclusters",
                    version="v1",
                    group="yaook.mk8s.io",
                )
        except (ApiException, HTTPError) as e:
            if self.required:
                raise click.ClickException(
                    f"Unable to read state {namespace}/{name}"
                    f" of {cluster_name} cluster,"
                    f" error: {str(e)}"
                )

            return

        for key, value in obj["spec"]["lcm"].items():
            if self.mangle_data:
                yield from self.mangle_data(key=key, value=value)
            else:
                yield key, value

        if self.extend_data:
            yield from self.extend_data(name=name, namespace=namespace, output=output)


class StateSecret(NamedTuple):
    """Data structure represents cluster state resource stored in secret."""

    name: str
    path: Path
    required: bool = True  # Whether the secret is mandatory
    string_data: bool = False  # Whether the secret contains string data
    extend_data: Optional[Callable] = None  # Extend data callback function
    mangle_data: Optional[Callable] = data_decode  # Mangle data callback function

    def get_data(
        self, cluster_name: str, cluster_namespace: str, output: Path = None
    ) -> Generator[tuple, None, None]:
        """Get state data from secret persistent storage.

        Args:
            cluster_name (str): Cluster name.
            cluster_namespace (str): Cluster namespace.
            output (Path, optional): Output path.

        Persistent storage (secret) resource should be located
        in the same namespace as cluster resource.

        Yields:
            tuple(str, bytes): Key and corresponding decoded bytes.

        """
        name = f"{cluster_name}-{self.name}"
        namespace = cluster_namespace
        try:
            with kubernetes_client() as client:
                obj = client.read_namespaced_secret(name, namespace)
        except (ApiException, HTTPError) as e:
            if self.required:
                raise click.ClickException(
                    f"Unable to read state {namespace}/{name}"
                    f" of {cluster_name} cluster,"
                    f" error: {str(e)}"
                )

            return

        for key, value in obj.data.items():
            if self.mangle_data:
                yield from self.mangle_data(key=key, value=value)
            else:
                yield key, value

        if self.extend_data:
            yield from self.extend_data(name=name, namespace=namespace, output=output)


class States(NamedTuple):
    """Data structure covers list of cluster persistence."""

    inventory: StateSecret = StateSecret(
        name="inventory",
        path=Path("inventory"),
    )
    config_toml: StateSecret = StateSecret(
        name="config-toml",
        path=Path("config"),
    )
    terraform_tfstate: StateSecret = StateSecret(
        name="terraform-tfstate",
        path=Path("terraform/terraform.tfstate"),
    )
    terraform_tfvars: StateSecret = StateSecret(
        name="terraform-tfvars",
        path=Path("terraform/config.tfvars.json"),
    )
    gnupg: StateSecret = StateSecret(
        name="gnupg",
        path=Path(".gnupg"),
    )
    wg_key: StateSecret = StateSecret(
        name="wg-key",
        path=Path(".wireguard/wg.key"),
    )
    provider_ssh_key: StateSecret = StateSecret(
        name="provider-ssh-key",
        path=Path(".ssh/provider_id_rsa"),
    )
    user_ssh_key: StateSecret = StateSecret(
        name="user-ssh-key",
        path=Path(".ssh/user_id_rsa"),
        required=False,
    )
    openrc: StateSecret = StateSecret(
        name="openrc",
        path=Path("openrc.sh"),
        mangle_data=data_export_decode,
        string_data=True,
    )
    envrc: StateLCM = StateLCM(
        name="envrc",
        path=Path(".envrc"),
        string_data=True,
        mangle_data=data_export,
        extend_data=envrc_extend,
    )

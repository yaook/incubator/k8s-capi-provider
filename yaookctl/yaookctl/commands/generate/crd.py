#!/usr/bin/env python3

"""This script generates cluster CRDs from config toml template file.

Yaook/k8s config toml template file acts as a single source of truth for
k8s-capi-provider CRDs. It contains (almost :/) all possible
yaook/k8s configuration options.

.. code-block:: bash
    crd --toml-config <config toml template file> [-o <output file>]
"""
import re
import tempfile
from typing import Any, Dict, Generator, Iterator, List

import click
import toml
import yaml

CRD_GROUP = "yaook.mk8s.io"
CRD_VERSION = "v1"
CRD_CATEGORIES = ["yaook"]

OBJECT_REFERENCE = {
    "type": "object",
    "properties": {
        "apiVersion": {"type": "string"},
        "kind": {"type": "string"},
        "name": {"type": "string"},
    },
}

CLUSTER_PLURAL = "yaookclusters"
CLUSTER_SINGULAR = "yaookcluster"
CLUSTER_KIND = "YaookCluster"
CLUSTER_SHORTNAMES = ["yc"]
CLUSTER_STATUS = {
    "type": "object",
    "properties": {
        "phase": {"type": "string"},
        "ready": {"type": "boolean"},
        "infrastructureReady": {"type": "boolean"},
        "bootstrapReady": {"type": "boolean"},
        "serviceReady": {"type": "boolean"},
        "failureReason": {"type": "string"},
        "failureMessage": {"type": "string"},
        "gateway": {
            "type": "object",
            "properties": {
                "azs": {"type": "array", "items": {"type": "string"}},
                "flavor": {"type": "string"},
            },
        },
        "openstack": {
            "type": "object",
            "properties": {
                "instances": {
                    "type": "integer",
                    "description": "The number of instances",
                },
                "vcpus": {
                    "type": "integer",
                    "description": "The number of used virtual CPUs",
                },
                "ram": {
                    "type": "integer",
                    "description": "The amount of used RAM, in MiB",
                },
                "securityGroups": {
                    "type": "integer",
                    "description": "The number of used security groups",
                },
                "floatingIps": {
                    "type": "integer",
                    "description": "The number of used floating IP addresses",
                },
                "serverGroups": {
                    "type": "integer",
                    "description": "The number of used server groups",
                },
            },
        },
    },
}
CLUSTER_ADDITIONAL_PRINTER_COLUMNS = [
    {
        "name": "Status",
        "type": "string",
        "description": "Cluster livecycle phase",
        "jsonPath": ".status.phase",
    },
    {
        "name": "Ready",
        "type": "boolean",
        "description": "Cluster overall status",
        "jsonPath": ".status.ready",
    },
    {
        "name": "InfrastructureReady",
        "type": "boolean",
        "description": "Cluster infrastructure status",
        "jsonPath": ".status.infrastructureReady",
    },
    {
        "name": "BootstrapReady",
        "type": "boolean",
        "description": "Cluster bootstrap status",
        "jsonPath": ".status.bootstrapReady",
    },
    {
        "name": "ServiceReady",
        "type": "boolean",
        "description": "Cluster service status",
        "jsonPath": ".status.serviceReady",
    },
    {
        "name": "Age",
        "type": "date",
        "jsonPath": ".metadata.creationTimestamp",
    },
]

MACHINE_TEMPLATE_PLURAL = "yaookmachinetemplates"
MACHINE_TEMPLATE_SINGULAR = "yaookmachinetemplate"
MACHINE_TEMPLATE_KIND = "YaookMachineTemplate"
MACHINE_TEMPLATE_SHORTNAMES = ["ymt"]
MACHINE_TEMPLATE_REQUIRED: List[str] = []
MACHINE_TEMPLATE_PROPERTIES = {
    "flavor": {"type": "string", "default": "M"},
    "image": {"type": "string", "default": "Ubuntu 20.04 LTS x64"},
    "providerID": {"type": "string"},
}
MACHINE_TEMPLATE_SPEC = {
    "type": "object",
    "properties": MACHINE_TEMPLATE_PROPERTIES,
    "required": MACHINE_TEMPLATE_REQUIRED,
}
MACHINE_TEMPLATE_STATUS = {
    "type": "object",
    "properties": {
        "ready": {"type": "boolean"},
    },
}

MACHINE_DEPLOYMENT_PLURAL = "yaookmachinedeployments"
MACHINE_DEPLOYMENT_SINGULAR = "yaookmachinedeployment"
MACHINE_DEPLOYMENT_KIND = "YaookMachineDeployment"
MACHINE_DEPLOYMENT_SHORTNAMES = ["ymd"]
MACHINE_DEPLOYMENT_REQUIRED = ["clusterName", "template"]
MACHINE_DEPLOYMENT_PROPERTIES = {
    "clusterName": {"type": "string"},
    "replicas": {"type": "integer"},
    "template": {
        "type": "object",
        "properties": {
            "spec": {
                "type": "object",
                "properties": {
                    "clusterName": {"type": "string"},
                    "bootstrap": {
                        "type": "object",
                        "properties": {
                            "configRef": OBJECT_REFERENCE,
                        },
                    },
                    "infrastructureRef": OBJECT_REFERENCE,
                    "failureDomain": {"type": "string"},
                },
                "required": ["bootstrap", "clusterName", "infrastructureRef"],
            }
        },
    },
}
MACHINE_DEPLOYMENT_SPEC = {
    "type": "object",
    "properties": MACHINE_DEPLOYMENT_PROPERTIES,
    "required": MACHINE_DEPLOYMENT_REQUIRED,
}
MACHINE_DEPLOYMENT_STATUS = {
    "type": "object",
    "properties": {
        "replicas": {"type": "integer"},
        "flavor": {"type": "string"},
    },
}
MACHINE_DEPLOYMENT_ADDITIONAL_PRINTER_COLUMNS = [
    {
        "name": "Replicas",
        "type": "integer",
        "description": "Total number of machines targeted by this MachineDeployment",
        "jsonPath": ".spec.replicas",
    },
    {
        "name": "Ready",
        "type": "integer",
        "description": "Total number of ready MachineDeployment machines",
        "jsonPath": ".status.replicas",
    },
    {
        "name": "Age",
        "type": "date",
        "jsonPath": ".metadata.creationTimestamp",
    },
]

CONTROL_PLANE_PLURAL = "yaookcontrolplanes"
CONTROL_PLANE_SINGULAR = "yaookcontrolplane"
CONTROL_PLANE_KIND = "YaookControlPlane"
CONTROL_PLANE_SHORTNAMES = ["ycp"]
CONTROL_PLANE_REQUIRED = [
    "bootstrap",
    "machineTemplate",
    "version",
]
CONTROL_PLANE_PROPERTIES = {
    "replicas": {"type": "integer"},
    "version": {"type": "string"},
    "machineTemplate": {
        "type": "object",
        "properties": {
            "infrastructureRef": OBJECT_REFERENCE,
        },
        "required": ["infrastructureRef"],
    },
    "kubeadmConfigSpec": {"type": "object", "default": {}},
    "bootstrap": {
        "type": "object",
        "properties": {
            "configRef": OBJECT_REFERENCE,
        },
    },
}
CONTROL_PLANE_SPEC = {
    "type": "object",
    "properties": CONTROL_PLANE_PROPERTIES,
    "required": CONTROL_PLANE_REQUIRED,
}
CONTROL_PLANE_STATUS = {
    "type": "object",
    "properties": {
        "replicas": {"type": "integer"},
        "flavor": {"type": "string"},
    },
}
CONTROL_PLANE_ADDITIONAL_PRINTER_COLUMNS = [
    {
        "name": "Replicas",
        "type": "integer",
        "description": "Total number of machines targeted by this control plane",
        "jsonPath": ".spec.replicas",
    },
    {
        "name": "Ready",
        "type": "integer",
        "description": "Total number of ready control plane machines",
        "jsonPath": ".status.replicas",
    },
    {
        "name": "Age",
        "type": "date",
        "jsonPath": ".metadata.creationTimestamp",
    },
]

SERVICE_PLURAL = "yaookservices"
SERVICE_SINGULAR = "yaookservice"
SERVICE_KIND = "YaookService"
SERVICE_SHORTNAMES = ["ys"]
SERVICE_STATUS = {
    "type": "object",
    "properties": {
        "phase": {"type": "string"},
        "ready": {"type": "boolean"},
        "failureReason": {"type": "string"},
        "failureMessage": {"type": "string"},
    },
}
SERVICE_ADDITIONAL_PRINTER_COLUMNS = [
    {
        "name": "Status",
        "type": "string",
        "description": "Cluster livecycle phase",
        "jsonPath": ".status.phase",
    },
    {
        "name": "Ready",
        "type": "boolean",
        "description": "Cluster overall status",
        "jsonPath": ".status.ready",
    },
    {
        "name": "Age",
        "type": "date",
        "jsonPath": ".metadata.creationTimestamp",
    },
]

CONFIG_TEMPLATE_PLURAL = "yaookconfigtemplates"
CONFIG_TEMPLATE_SINGULAR = "yaookconfigtemplate"
CONFIG_TEMPLATE_KIND = "YaookConfigTemplate"
CONFIG_TEMPLATE_SHORTNAMES = ["yct"]
CONFIG_TEMPLATE_STATUS = {
    "type": "object",
    "properties": {
        "ready": {"type": "boolean"},
        "dataSecretName": {"type": "string"},
    },
}

LCM_SCHEMA = {
    "type": "object",
    "required": [],
    "properties": {
        # True if the ssh/gpg/wg identities will be created by k8s-capi-provider.
        # The provider identity should not be created by k8s-capi-provider when
        # the cluster was injected (by `yaookctl inject`) from yaook/k8s LCM.
        # Then, the origin yaook/k8s ssh/gpg/wg identities (private keys) are
        # used as a provider one and should not be created again.
        "MANAGED_PROVIDER_IDENTITY": {"type": "boolean", "default": True},
        "wg_conf_name": {"type": "string", "default": "wg0"},
        "wg_user": {"type": "string", "default": "firstnamelastname"},
        # FIXME: We agreed that for web-platform integration phase 1
        #  will be MANAGED_K8S_RELEASE_THE_KRAKEN sets to True.
        #  This should be changed and hardened for production env.
        #  as is requested in #7.
        "MANAGED_K8S_RELEASE_THE_KRAKEN": {"type": "boolean", "default": True},
        "WG_USAGE": {"type": "boolean", "default": True},
        "WG_COMPANY_USERS": {"type": "boolean", "default": False},
        "PASS_COMPANY_USERS": {"type": "boolean", "default": False},
        "SSH_COMPANY_USERS": {"type": "boolean", "default": False},
        "TF_VAR_keypair": {
            "type": "string",
            "default": "firstnamelastname-hostname-gendate",
        },
    },
}

OPENSTACK_SCHEMA = {
    "type": "object",
    "required": ["secretRef"],
    "properties": {
        "secretRef": {
            "type": "string",
        }
    },
}


def get_openapi_type(value: Any) -> str:
    """Get OpenAPI type mapping.

    Args:
        value (Any): Value that will be mapped to the OpenApi type.

    Returns:
        str: OpenAPI type
    """
    value_type = type(value)

    if value_type == dict:
        return "object"

    if value_type == list:
        return "array"

    if value_type == int:
        return "integer"

    if value_type == float:
        return "number"

    if value_type == bool:
        return "boolean"

    if value_type == str:
        return "string"

    raise NotImplementedError


def dict_paths(in_dict: dict, keys_buffer: list = None) -> Generator[list, None, None]:
    """Get dict path from nested keys.

    Example:
         .. code:: python

         d = {"foo": {"foo_deep": {"foo_deeper": 1}}, "bazz": 2}
         assert list(dict_paths(d)) == [['foo', 'foo_deep', 'foo_deeper'], ['bazz']]

     Args:
         in_dict (dict): Dict to get path from.
         keys_buffer (list, optional): Dict keys path buffer.

     Yields:
         list: Dict keys path.

    """
    keys_buffer = keys_buffer[:] if keys_buffer else []
    if isinstance(in_dict, dict):
        for key, value in in_dict.items():
            if isinstance(value, dict):
                for dict_gen in dict_paths(value, keys_buffer + [key]):
                    yield dict_gen
            elif isinstance(value, list) or isinstance(value, tuple):
                for item in value:
                    for dict_gen in dict_paths(item, keys_buffer + [key]):
                        yield dict_gen
            else:
                yield keys_buffer + [key]
    else:
        yield keys_buffer


def to_schema(
    config_uncommented: dict, config_defaults_paths, paths_buffer=None
) -> dict:
    """Generate OpenAPI v3 schema from dictionary.

    Iterates over nested dictionary structure and optionally set default values as
    follows:
      1. TODO

    Args:
        config_uncommented (dict):
        config_defaults_paths (list):
        paths_buffer (list, optional):

    Returns:
        dict: OpenAPI v3 schema
    """
    schema: Dict = dict()
    paths_buffer = paths_buffer[:] if paths_buffer else []
    schema["type"] = get_openapi_type(config_uncommented)

    if isinstance(config_uncommented, dict) and len(config_uncommented) > 0:
        schema["properties"] = {}
        for _property, value in config_uncommented.items():
            paths_buffer.append(_property)
            schema["properties"][_property] = to_schema(
                value, config_defaults_paths, paths_buffer=paths_buffer
            )
            paths_buffer.pop()

    elif isinstance(config_uncommented, list) and len(config_uncommented) > 0:
        same_type = all(
            [
                isinstance(config_uncommented[0], type(item))
                for item in config_uncommented
            ]
        )

        if same_type:
            schema["items"] = to_schema(
                config_uncommented[0], config_defaults_paths, paths_buffer=paths_buffer
            )
        else:
            schema["items"] = []
            # FIXME: Handle also nested lists with mixed types, e.g. [[1, ], ["a", 1]]
            for item in config_uncommented:
                schema["items"].append(
                    to_schema(item, config_defaults_paths, paths_buffer=paths_buffer)
                )

        # Set defaults on items level
        if paths_buffer in config_defaults_paths:
            schema["default"] = config_uncommented

    elif isinstance(config_uncommented, list) and len(config_uncommented) == 0:
        schema["items"] = {"type": "string"}

    else:
        # Set defaults on type level
        if paths_buffer in config_defaults_paths:
            schema["default"] = config_uncommented

    return schema


def uncomment_toml_lines(lines: list) -> Iterator[str]:
    """Uncomment TOML lines.

    We have to consider optional yaook/k8s variables in k8s-capi-provider CRDs.
    Therefore, we have to uncomment those variables if config.template.toml and
    process them as an optional variables in k8s-capi-provider CRDs.

    ! We assumed that commented variable key does not contain whitespaces.
    ! We assumed that commented variable key has two or more characters.

    Args:
        lines (list): Toml lines

    Returns:
        Iterator(str): Uncommented toml lines
    """
    keys_buffer: List = []

    for line in lines:
        # Remove the whitespaces around equal sign
        line = re.sub(r"\s*=\s*", "=", line)

        match_table = re.match(r"^[\[]+.*[\]]+", line)
        match_optional = re.match(
            r"^#(\s?[\[]+.{2,}[\]]+)|^#\s?([^#=$ \n]{2,})=([^#\n]*)(#(.+))?", line
        )

        if match_table or (match_optional and match_optional.group(1)):
            keys_buffer = []

        if match_optional:
            table, key, value, _, comment = match_optional.groups()
            table = table.strip() if table else None
            key = key.strip() if key else None
            value = value.strip() if value else None
            # FIXME: Comments could be used in CRD description
            # comment = comment.strip() if comment else None

            if key in keys_buffer:
                continue

            if table:
                line = table

            if key:
                if value in ("null", "...", None):
                    value = "'replace-me'"
                if value in ("[..]",):
                    value = ["replace-me"]
                line = f"{key}={value}"
                keys_buffer.append(key)

        yield line + "\n" if "\n" not in line else line


def get_crd(
    kind: str,
    plural: str,
    singular: str,
    short_names: list,
    spec: dict,
    status: dict = None,
    adds: list = None,
) -> dict:
    """Get CRD based on given arguments.

    Args:
        kind (str): Resource kind.
        plural (str): Resource plural.
        singular (str): Resource singular.
        short_names (list): Resource short names.
        spec (dict): Resource spec.
        status (dict, optional): Resource status.
        adds (list, optional): Resource additionalPrinterColumns.

    Returns:
        dict: Generated CRD.
    """
    return {
        "apiVersion": "apiextensions.k8s.io/v1",
        "kind": "CustomResourceDefinition",
        "metadata": {"name": plural + "." + CRD_GROUP},
        "spec": {
            "scope": "Namespaced",
            "group": CRD_GROUP,
            "names": {
                "kind": kind,
                "plural": plural,
                "singular": singular,
                "shortNames": short_names,
                "categories": CRD_CATEGORIES,
            },
            "versions": [
                {
                    "name": CRD_VERSION,
                    "served": True,
                    "storage": True,
                    "subresources": {"status": {}} if status else None,
                    "additionalPrinterColumns": adds if adds else None,
                    "schema": {
                        "openAPIV3Schema": {
                            "type": "object",
                            "required": ["spec"],
                            "properties": {
                                "spec": spec,
                                "status": status,
                            }
                            if status
                            else {"spec": spec},
                        }
                    },
                }
            ],
        },
    }


def get_crds(config_schema: dict) -> tuple:
    """Get cluster related CRDs generated based on given config schema.

    Args:
        config_schema (dict): Config schema to be applied to CRDs.

    Returns:
        tuple: Cluster related CRDs.

    """
    config_template_spec = {
        "type": "object",
        "properties": {
            "kubernetes": config_schema["properties"]["kubernetes"],
            "ch-k8s-lbaas": config_schema["properties"]["ch-k8s-lbaas"],
            "node-scheduling": config_schema["properties"]["node-scheduling"],
            "testing": config_schema["properties"]["testing"],
        },
        "required": [],
    }
    service_spec = {
        "type": "object",
        "properties": {
            "k8s-service-layer": config_schema["properties"]["k8s-service-layer"],
            # special fields that aren't part of config.toml
            "clusterName": {"type": "string"},
        },
        "required": ["clusterName"],
    }
    cluster_spec: Dict[str, Any] = {
        "type": "object",
        "properties": {
            "terraform": config_schema["properties"]["terraform"],
            "wireguard": config_schema["properties"]["wireguard"],
            "ipsec": config_schema["properties"]["ipsec"],
            "cah-users": config_schema["properties"]["cah-users"],
            "passwordstore": config_schema["properties"]["passwordstore"],
            "miscellaneous": config_schema["properties"]["miscellaneous"],
            "load-balancing": config_schema["properties"]["load-balancing"],
            # special fields that aren't part of config.toml
            "controlPlaneRef": OBJECT_REFERENCE,
            "controlPlaneAvailabilityZones": {
                "type": "array",
                "items": {"type": "string"},
            },
            "openStackRC": OPENSTACK_SCHEMA,
            "lcm": LCM_SCHEMA,
            "controlPlaneEndpoint": {
                "type": "object",
                "properties": {
                    "host": {"type": "string"},
                    "port": {"type": "integer"},
                },
            },
        },
        "required": ["openStackRC"],
    }
    cluster_spec["properties"]["passwordstore"]["properties"]["additional_users"][
        "items"
    ]["properties"].update({"pub_key": {"type": "string"}})
    cluster_spec["properties"]["terraform"]["properties"].update(
        {
            "public_network": {"type": "string"},
            "subnet_cidr": {"type": "string", "default": "172.30.154.0/24"},
            "gateway_image_name": {"type": "string", "default": "Debian 11 (bullseye)"},
            "gateway_flavor": {"type": "string", "default": "XS"},
            "azs": {
                "type": "array",
                "items": {"type": "string"},
                "default": ["AZ1", "AZ2", "AZ3"],
                "description": "If 'enable_az_management=true' defines which"
                " availability zones of your cloud to use to distribute the spawned"
                " server for better HA. Additionally the count of the array will define"
                " how many gateway server will be spawned. The naming of the elements"
                " doesn't matter if 'enable_az_management=false'.",
            },
        }
    )

    return (
        get_crd(
            CLUSTER_KIND,
            CLUSTER_PLURAL,
            CLUSTER_SINGULAR,
            CLUSTER_SHORTNAMES,
            cluster_spec,
            CLUSTER_STATUS,
            CLUSTER_ADDITIONAL_PRINTER_COLUMNS,
        ),
        get_crd(
            CONTROL_PLANE_KIND,
            CONTROL_PLANE_PLURAL,
            CONTROL_PLANE_SINGULAR,
            CONTROL_PLANE_SHORTNAMES,
            CONTROL_PLANE_SPEC,
            CONTROL_PLANE_STATUS,
            CONTROL_PLANE_ADDITIONAL_PRINTER_COLUMNS,
        ),
        get_crd(
            MACHINE_DEPLOYMENT_KIND,
            MACHINE_DEPLOYMENT_PLURAL,
            MACHINE_DEPLOYMENT_SINGULAR,
            MACHINE_DEPLOYMENT_SHORTNAMES,
            MACHINE_DEPLOYMENT_SPEC,
            MACHINE_DEPLOYMENT_STATUS,
            MACHINE_DEPLOYMENT_ADDITIONAL_PRINTER_COLUMNS,
        ),
        get_crd(
            MACHINE_TEMPLATE_KIND,
            MACHINE_TEMPLATE_PLURAL,
            MACHINE_TEMPLATE_SINGULAR,
            MACHINE_TEMPLATE_SHORTNAMES,
            MACHINE_TEMPLATE_SPEC,
            MACHINE_TEMPLATE_STATUS,
        ),
        get_crd(
            CONFIG_TEMPLATE_KIND,
            CONFIG_TEMPLATE_PLURAL,
            CONFIG_TEMPLATE_SINGULAR,
            CONFIG_TEMPLATE_SHORTNAMES,
            config_template_spec,
            CONFIG_TEMPLATE_STATUS,
        ),
        get_crd(
            SERVICE_KIND,
            SERVICE_PLURAL,
            SERVICE_SINGULAR,
            SERVICE_SHORTNAMES,
            service_spec,
            SERVICE_STATUS,
            SERVICE_ADDITIONAL_PRINTER_COLUMNS,
        ),
    )


@click.command(
    context_settings=dict(help_option_names=["-h", "--help"]),
    short_help="Generate cluster CRDs from config toml template.",
)
@click.option("--output", "-o", type=click.Path(), help="The output file path")
@click.option(
    "--toml-config",
    "-t",
    required=True,
    type=click.Path(exists=True),
    help="Path to the config toml template file",
)
def crd(toml_config, output):
    """Generate cluster CRDs from config toml template."""
    with tempfile.NamedTemporaryFile("w") as tmp:
        with open(toml_config, "r") as config_file:
            for uncommented_line in uncomment_toml_lines(config_file.readlines()):
                tmp.write(uncommented_line)

        tmp.flush()
        config_uncommented = toml.load(tmp.name)
        config_defaults_paths = list(dict_paths(toml.load(toml_config)))

    config_schema = to_schema(config_uncommented, config_defaults_paths)
    crds = get_crds(config_schema)

    if output:
        with open(output, "w") as crd_file:
            yaml.dump_all(crds, crd_file)
    else:
        click.echo(yaml.dump_all(crds))


if __name__ == "__main__":
    crd()

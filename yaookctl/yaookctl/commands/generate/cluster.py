#!/usr/bin/env python3

"""This script generates cluster custom resources (CRs).

   It reads CRs templates from URL or path and fill them with env. variables.

   See script help for details.

.. code-block:: bash
    cluster [--template <yaml template file url or path>]
        [--output <output file>] [--env <environment file>]
"""
import os
import re
import sys
from pathlib import Path
from typing import Any, Optional, Tuple

import click
import requests
import yaml

# Pattern to read .env file
from requests import Response

RE_DOTENV = re.compile(
    r"^(?!\d+)(?P<key>[\w\-\.]+)\=[\"\']?(?P<value>(.*?))[\"\']?$",
    re.MULTILINE | re.UNICODE | re.IGNORECASE,
)
# Pattern to extract env variables
RE_VARIABLES = re.compile(
    r"\${(?P<variable>(.*?))(\|(?P<default>.*?))?}",
    re.MULTILINE | re.UNICODE | re.IGNORECASE | re.VERBOSE,
)
# Pattern to remove comments
RE_COMMENTS = re.compile(r"(^#.*\n)", re.MULTILINE | re.UNICODE | re.IGNORECASE)

# FIXME: Provide a single source of truth for the fields below.
#   They should be propagated to the all yaookctl "corners" from one place.
#   Currently, the toml2cr.py file and yaookctl.py file manage their own
#   copies of fields below.
FIELDS_MANDATORY = {
    "CLUSTER_NAME",
    "WG_USER_PUB_KEY",
    "WG_USER_IDENT",
}
# Optional fields have default values. This script
#   log a warning message if some of those are not defined and then
#   the default is applied.
FIELDS_OPTIONAL = {
    "OPENSTACK_SSH_KEY_NAME",
    "OPENSTACK_EXTERNAL_NETWORK_NAME",
    "LBAAS_SHARED_SECRET",
    "KUBERNETES_VERSION",
    "CONTROL_PLANE_MACHINE_COUNT",
    "OPENSTACK_CONTROL_PLANE_AZ",
    "OPENSTACK_CONTROL_PLANE_MACHINE_FLAVOR",
    "OPENSTACK_CONTROL_PLANE_IMAGE_NAME",
    "WORKER_MACHINE_COUNT",
    "OPENSTACK_NODE_AZ",
    "OPENSTACK_NODE_MACHINE_FLAVOR",
    "OPENSTACK_NODE_IMAGE_NAME",
}
# Only one OpenStack authentication method should be used.
#   The authentication method is defined by `OS_AUTH_TYPE` variable, but
#   be aware that the `OS_PROJECT_NAME` or `OS_PROJECT_ID` should not be
#   specified when `v3applicationcredential` is used.

FIELDS_MANDATORY_V3APPCRED = {
    "OS_AUTH_URL",
    "OS_AUTH_TYPE",
    "OS_APPLICATION_CREDENTIAL_ID",
    "OS_APPLICATION_CREDENTIAL_SECRET",
}
FIELDS_MANDATORY_V3PASS = {
    "OS_AUTH_URL",
    "OS_PROJECT_NAME",
    "OS_USER_DOMAIN_NAME",
    "OS_PROJECT_DOMAIN_ID",
    "OS_USERNAME",
    "OS_PASSWORD",
    "OS_REGION_NAME",
    "OS_INTERFACE",
    "OS_IDENTITY_API_VERSION",
}
FIELDS_V3APPCRED_EXCLUDE = {
    "OS_PROJECT_NAME",
    "OS_PROJECT_ID",
}


def validate_response(response: Response):
    """Validate request response."""
    if not response.status_code == 200:
        message = f"URL is invalid, status code {response.status_code}"
        raise ValueError(message)

    if "plain" not in response.headers["content-type"]:
        message = (
            f"Invalid content type {response.headers['content-type']},"
            " expected text/plain"
        )
        raise ValueError(message)


def validate_keys(
    keys: set, is_v3pass: bool, is_v3app_cred: bool, template: bool = False
):
    """Validate presence of provided keys."""
    where = "template" if template else ".env"

    if mandatory := FIELDS_MANDATORY.difference(keys):
        click.echo(f"Missing mandatory variables {list(mandatory)} in {where}!")
        sys.exit(1)

    if template:  # Optional fields should be present only in template
        if optional := FIELDS_OPTIONAL.difference(keys):
            click.echo(f"Missing optional variables {list(optional)} in {where}!")
            sys.exit(1)

    if is_v3pass:
        if mandatory := FIELDS_MANDATORY_V3PASS.difference(keys):
            click.echo(f"Missing mandatory variables {list(mandatory)} in {where}!")
            sys.exit(1)

    if is_v3app_cred:
        if mandatory := FIELDS_MANDATORY_V3APPCRED.difference(keys):
            click.echo(f"Missing mandatory variables {list(mandatory)} in {where}!")
            sys.exit(1)

    if not is_v3pass and not is_v3app_cred:
        click.echo("At least one OpenStack authentication method should be used!")
        sys.exit(1)

    if is_v3pass and is_v3app_cred:
        click.echo("Only one OpenStack authentication method should be used!")
        sys.exit(1)

    v3app_cred_exclude = FIELDS_V3APPCRED_EXCLUDE.intersection(keys)
    if is_v3app_cred and v3app_cred_exclude:
        click.echo(
            f"{list(v3app_cred_exclude)} should not be"
            " specified when `v3applicationcredential` is used!"
        )
        sys.exit(1)


class Template(object):
    """Template variables and validation."""

    def __init__(
        self, source: str, is_v3pass: bool = False, is_v3app_cred: bool = False
    ):
        self.source = source
        self.is_v3pass, self.is_v3app_cred = is_v3pass, is_v3app_cred
        self.content = self.get_template_content()
        self.variables = self.get_variables()
        self.variables_keys = set(self.variables.keys())

    def remove_unused_secret(self, content: str) -> str:
        """Remove unused secret from template content."""
        is_v3pass = self.is_v3pass
        is_v3app_cred = self.is_v3app_cred
        yaml_filled = yaml.safe_load_all(content)
        yaml_resources = []
        for yaml_resource in yaml_filled:

            if yaml_resource["kind"] == "Secret":
                auth = yaml_resource["metadata"]["labels"]["auth"]

                if auth == "v3password" and not is_v3pass:
                    continue

                if auth == "v3applicationcredential" and not is_v3app_cred:
                    continue

            yaml_resources.append(yaml_resource)

        return yaml.safe_dump_all(yaml_resources)

    def get_template_content(self) -> str:
        """Get template content from local path or remote location."""
        source = self.source

        if Path(source).exists():
            with open(source) as fd:
                template_cleaned = RE_COMMENTS.sub("", fd.read())

        else:
            response = requests.get(source)
            validate_response(response)
            template_cleaned = RE_COMMENTS.sub("", response.content.decode())

        return self.remove_unused_secret(template_cleaned)

    def get_variables(self) -> dict:
        """Get variables from template content."""
        variables = {}
        content = self.content
        for entry in RE_VARIABLES.finditer(content):
            variables.update({entry.group("variable"): entry.group("default")})
        return variables

    def get(self, key: str) -> Optional[Any]:
        """Get variable value based on its key."""
        return self.variables.get(key)

    def validate(self):
        """Validate presence of variables."""
        keys = self.variables_keys
        is_v3pass = self.is_v3pass
        is_v3app_cred = self.is_v3app_cred
        validate_keys(keys, is_v3pass, is_v3app_cred, template=True)


class Environment(object):
    """Environment variables and validation."""

    def __init__(self, env_file: Optional[str]) -> None:
        self.env_file = env_file

        self.variables = self.get_variables()
        self.variables_keys = set(self.variables.keys())

        self.is_v3pass, self.is_v3app_cred = self.get_v3()

    def get_variables(self):
        """Get variables from env. file if defined, from environment otherwise."""
        env_file = self.env_file

        if env_file and Path(env_file).exists():
            env_variables = {}
            with open(env_file) as fd:
                content = fd.read()
                content_cleaned = RE_COMMENTS.sub("", content)  # Remove comments
                for entry in RE_DOTENV.finditer(content_cleaned):
                    env_variables.update({entry.group("key"): entry.group("value")})

            return env_variables

        return os.environ

    def get_v3(self) -> Tuple[bool, bool]:
        """Get v3 method based on provided variables."""
        variables = self.variables
        is_v3pass: bool = False
        is_v3app_cred: bool = False

        if variables.get("OS_AUTH_TYPE") == "v3applicationcredential":
            is_v3app_cred = True

        if variables.get("OS_AUTH_TYPE") == "v3password" or not (
            variables.get("OS_USERNAME") is None
        ):
            is_v3pass = True

        return is_v3pass, is_v3app_cred

    def get(self, key) -> Optional[Any]:
        """Get variable value based on its key."""
        return self.variables.get(key)

    def validate(self):
        """Validate presence of variables."""
        keys = self.variables_keys
        is_v3pass = self.is_v3pass
        is_v3app_cred = self.is_v3app_cred
        validate_keys(keys, is_v3pass, is_v3app_cred, template=False)


class TemplateType(click.ParamType):
    """Validate template.

    Valid template should be in yaml/yml format.
    Template could be defined as URL (remote location)
    or as path to the local storage.
    If the path to the local storage is defined, then
    the existence of file is also validated.
    """

    name = "template"

    def convert(self, value, param, ctx):
        """Convert the value to the correct type."""
        if not value.endswith((".yaml", ".yml")):
            self.fail(
                f"Template file {value!r} must have a yaml/yml extension.", param, ctx
            )

        if not value.startswith(("http", "https")):
            if not Path(value).exists():
                self.fail(
                    f"Path to the template file {value!r} does not exist.", param, ctx
                )

        return value


def default_template() -> str:
    """Return default template URL.

    Returns:
        str: Default template URL.

    """
    return (
        "https://gitlab.com/yaook/incubator/k8s-capi-provider/"
        "-/raw/devel/templates/cluster-template.yaml"
    )


def description_msg() -> str:
    """Return description message.

    Returns:
        str: Description message

    """
    return f"""
Generate cluster CRs from template.
The template file is filled with environment variables from the local environment
OR from variables defined in the environment file.

\b
Template file:
    - Could be defined as URL or a valid path
    - Default template is taken from {default_template()}

\b
Environment variables:
    - Could be defined in a file OR in the local environment
    - The sources above are NOT combined
    - See the list of variables below:

\b
    Mandatory variables:
    - {FIELDS_MANDATORY}

\b
    Optional variables:
    - {FIELDS_OPTIONAL}

\b
    OpenStack access mandatory variables:
    Please note that only one OpenStack authentication method should be used.

\b
        OpenStack application credential authentication variables:
        - {FIELDS_MANDATORY_V3APPCRED}

\b
        OpenStack password authentication variables:
        - {FIELDS_MANDATORY_V3PASS}
"""


@click.command(
    help=description_msg(),
    context_settings=dict(help_option_names=["-h", "--help"]),
    short_help="Generate cluster custom resources (CRs).",
)
@click.option("--output", "-o", type=click.Path(), help="The output file path")
@click.option(
    "--template",
    "-t",
    default=default_template(),
    show_default=True,
    type=TemplateType(),
    help="URL or path to the template file",
)
@click.option(
    "--env",
    "-e",
    "env_file",
    type=click.Path(exists=True),
    help="Path to the environment variables file",
)
def cluster(output, template, env_file):
    """Generate cluster custom resources (CRs)."""
    env = Environment(env_file)
    env.validate()

    temp = Template(template, is_v3pass=env.is_v3pass, is_v3app_cred=env.is_v3app_cred)
    temp.validate()

    content = temp.content

    for key in temp.variables_keys:
        default = temp.get(key)
        search = (
            f"${{{key}}}" if not default else f"${{{key}|{default}}}"
        )  # ${key} or ${key|default}
        replace = env.get(key)

        if not replace and not default and key not in FIELDS_OPTIONAL:
            click.echo(
                f"Failed to fill template: Variable {key} is not defined"
                " in .env and doesn't have default value!"
            )
            sys.exit(1)

        if not replace:
            msg = f"# Warning: Variable {key} is not defined in .env."
            if default:
                msg = msg + f" The default value '{default}' will be applied!"
            else:
                msg = msg + " The default value will be generated by capi-provider!"
            click.echo(msg)
            replace = default or ""

        content = content.replace(search, replace)

    if output:
        with open(output, "w") as fd:
            fd.write(content)
    else:
        click.echo(content)


if __name__ == "__main__":
    cluster()

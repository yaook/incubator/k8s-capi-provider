#!/usr/bin/env python3

"""This script generates CR template from config toml template file.

Yaook/k8s config toml template file acts as a single source of truth for
k8s-capi-provider cluster template.
It contains (almost :/) all possible yaook/k8s configuration options.

.. code-block:: bash
    generate_template.py --toml-config <config toml template file> [-o <output file>]
"""
import click
import toml
import yaml

CR_APIVERSION = "yaook.mk8s.io/v1"

CLUSTER_KIND = "YaookCluster"
CONTROL_PLANE_KIND = "YaookControlPlane"
MACHINE_DEPLOYMENT_KIND = "YaookMachineDeployment"
MACHINE_TEMPLATE_KIND = "YaookMachineTemplate"
CONFIG_TEMPLATE_KIND = "YaookConfigTemplate"
SERVICE_KIND = "YaookService"


@click.command(
    context_settings=dict(help_option_names=["-h", "--help"]),
    short_help="Generate cluster CRs template from config toml template.",
)
@click.option("--output", "-o", type=click.Path(), help="The output file path")
@click.option(
    "--toml-config",
    "-t",
    required=True,
    type=click.Path(exists=True),
    help="Path to the config toml template file",
)
def template(toml_config, output):
    """Generate cluster CRs template from config toml template."""
    with open(toml_config) as config_file:
        config = toml.load(config_file)

    cluster_name_placeholder = "${CLUSTER_NAME}"
    kubernetes_version_placeholder = (
        f"${{KUBERNETES_VERSION|{config['kubernetes']['version']}}}"
    )

    cluster_cr = {
        "apiVersion": CR_APIVERSION,
        "kind": CLUSTER_KIND,
        "metadata": {"name": cluster_name_placeholder},
        "spec": {
            "terraform": config.get("terraform"),
            "wireguard": config.get("wireguard"),
            "ipsec": config.get("ipsec"),
            "cah-users": config.get("cah-users"),
            "passwordstore": config.get("passwordstore"),
            "miscellaneous": config.get("miscellaneous"),
            "load-balancing": config.get("load-balancing"),
            # special fields that aren't part of config.toml
            "controlPlaneRef": {
                "kind": CONTROL_PLANE_KIND,
                "apiVersion": CR_APIVERSION,
                "name": f"{cluster_name_placeholder}-control-plane",
            },
            "controlPlaneAvailabilityZones": ["${OPENSTACK_CONTROL_PLANE_AZ|AZ1}"],
            "openStackRC": {"secretRef": f"{cluster_name_placeholder}-openrc"},
            "lcm": {
                "TF_VAR_keypair": "${OPENSTACK_SSH_KEY_NAME}",
            },
        },
    }
    cluster_cr["spec"]["wireguard"]["peers"] = [
        {
            "ident": "${WG_USER_IDENT}",
            "pub_key": "${WG_USER_PUB_KEY}",
        }
    ]
    cluster_cr["spec"]["terraform"][
        "public_network"
    ] = "${OPENSTACK_EXTERNAL_NETWORK_NAME|shared-public-IPv4}"

    control_plane_cr = {
        "apiVersion": CR_APIVERSION,
        "kind": CONTROL_PLANE_KIND,
        "metadata": {"name": f"{cluster_name_placeholder}-control-plane"},
        "spec": {
            "replicas": "${CONTROL_PLANE_MACHINE_COUNT|3}",
            "version": kubernetes_version_placeholder,
            "machineTemplate": {
                "infrastructureRef": {
                    "kind": MACHINE_TEMPLATE_KIND,
                    "apiVersion": CR_APIVERSION,
                    "name": f"{cluster_name_placeholder}-control-plane",
                }
            },
            "bootstrap": {
                "configRef": {
                    "kind": CONFIG_TEMPLATE_KIND,
                    "apiVersion": CR_APIVERSION,
                    "name": cluster_name_placeholder,
                }
            },
        },
    }

    machine_template_control_plane_cr = {
        "apiVersion": CR_APIVERSION,
        "kind": MACHINE_TEMPLATE_KIND,
        "metadata": {"name": f"{cluster_name_placeholder}-control-plane"},
        "spec": {
            "flavor": "${OPENSTACK_CONTROL_PLANE_MACHINE_FLAVOR|M}",
            "image": "${OPENSTACK_CONTROL_PLANE_IMAGE_NAME|Ubuntu 20.04 LTS x64}",
        },
    }

    machine_deployment_cr = {
        "apiVersion": CR_APIVERSION,
        "kind": MACHINE_DEPLOYMENT_KIND,
        "metadata": {"name": f"{cluster_name_placeholder}-md-0"},
        "spec": {
            "clusterName": cluster_name_placeholder,
            "replicas": "${WORKER_MACHINE_COUNT|4}",
            "template": {
                "spec": {
                    "clusterName": cluster_name_placeholder,
                    "bootstrap": {
                        "configRef": {
                            "kind": CONFIG_TEMPLATE_KIND,
                            "apiVersion": CR_APIVERSION,
                            "name": cluster_name_placeholder,
                        }
                    },
                    "infrastructureRef": {
                        "kind": MACHINE_TEMPLATE_KIND,
                        "apiVersion": CR_APIVERSION,
                        "name": f"{cluster_name_placeholder}-md-0",
                    },
                    "failureDomain": "${OPENSTACK_NODE_AZ|AZ2}",
                }
            },
        },
    }

    machine_template_machine_deployment_cr = {
        "apiVersion": CR_APIVERSION,
        "kind": MACHINE_TEMPLATE_KIND,
        "metadata": {"name": f"{cluster_name_placeholder}-md-0"},
        "spec": {
            "flavor": "${OPENSTACK_NODE_MACHINE_FLAVOR|M}",
            "image": "${OPENSTACK_NODE_IMAGE_NAME|Ubuntu 20.04 LTS x64}",
        },
    }

    config_template_cr = {
        "apiVersion": CR_APIVERSION,
        "kind": CONFIG_TEMPLATE_KIND,
        "metadata": {"name": cluster_name_placeholder},
        "spec": {
            "kubernetes": config.get("kubernetes"),
            "ch-k8s-lbaas": config.get("ch-k8s-lbaas"),
            "node-scheduling": config.get("node-scheduling"),
            "testing": config.get("testing"),
        },
    }
    config_template_cr["spec"]["ch-k8s-lbaas"][
        "shared_secret"
    ] = "${LBAAS_SHARED_SECRET}"
    config_template_cr["spec"]["kubernetes"]["version"] = kubernetes_version_placeholder

    service_cr = {
        "apiVersion": CR_APIVERSION,
        "kind": SERVICE_KIND,
        "metadata": {"name": f"{cluster_name_placeholder}-service"},
        "spec": {
            "clusterName": cluster_name_placeholder,
            "k8s-service-layer": config.get("k8s-service-layer"),
        },
    }
    # FIXME: Remove default value for use_thanos variable once yaook/k8s#467
    #  is resolved. See: yaook/incubator/k8s-capi-provider#54 for details.
    #  Thanos should have default value `False` for two reasons:
    #  1. If OpenStack application credentials are used.
    #     Currently, Thanos will not work with OpenStack application credentials [0].
    #     If you use OpenStack application credentials
    #     then you have to set `use_thanos = false`.
    #     [0] https://gitlab.com/yaook/k8s/-/issues/436#note_873556688
    #  2. yaook/k8s service layer failed when `use_thanos` does not have a
    #     default value. See yaook/k8s#467 for details.
    service_cr["spec"]["k8s-service-layer"]["prometheus"]["use_thanos"] = False

    # Note: Only one OpenStack authentication method should be used.
    #   The authentication method is defined by `OS_AUTH_TYPE` variable, but
    #   be aware that the `OS_PROJECT_NAME` or `OS_PROJECT_ID` should not be
    #   specified when `v3applicationcredential` is used.
    #   CLI tool yaookctl will take care which one will be filed and rendered.

    secret_v3pass = {
        "apiVersion": "v1",
        "kind": "Secret",
        "metadata": {
            "name": f"{cluster_name_placeholder}-openrc",
            "labels": {
                "auth": "v3password",
            },
        },
        "type": "Opaque",
        "stringData": {
            "OS_AUTH_URL": "${OS_AUTH_URL}",
            "OS_PROJECT_NAME": "${OS_PROJECT_NAME}",
            "OS_USER_DOMAIN_NAME": "${OS_USER_DOMAIN_NAME}",
            "OS_PROJECT_DOMAIN_ID": "${OS_PROJECT_DOMAIN_ID}",
            "OS_USERNAME": "${OS_USERNAME}",
            "OS_PASSWORD": "${OS_PASSWORD}",
            "OS_REGION_NAME": "${OS_REGION_NAME}",
            "OS_INTERFACE": "${OS_INTERFACE}",
            "OS_IDENTITY_API_VERSION": "${OS_IDENTITY_API_VERSION}",
        },
    }

    secret_v3app_cred = {
        "apiVersion": "v1",
        "kind": "Secret",
        "metadata": {
            "name": f"{cluster_name_placeholder}-openrc",
            "labels": {
                "auth": "v3applicationcredential",
            },
        },
        "type": "Opaque",
        "stringData": {
            "OS_AUTH_TYPE": "${OS_AUTH_TYPE}",
            "OS_AUTH_URL": "${OS_AUTH_URL}",
            "OS_APPLICATION_CREDENTIAL_ID": "${OS_APPLICATION_CREDENTIAL_ID}",
            "OS_APPLICATION_CREDENTIAL_SECRET": "${OS_APPLICATION_CREDENTIAL_SECRET}",
        },
    }

    crs = (
        cluster_cr,
        control_plane_cr,
        machine_template_control_plane_cr,
        machine_deployment_cr,
        machine_template_machine_deployment_cr,
        config_template_cr,
        service_cr,
        secret_v3pass,
        secret_v3app_cred,
    )

    if output:
        with open(output, "w") as cr_file:
            yaml.dump_all(crs, cr_file)
    else:
        click.echo(yaml.dump_all(crs))


if __name__ == "__main__":
    template()

"""Module contains CLI group commands."""
import click

from .commands.generate.cluster import cluster
from .commands.generate.crd import crd
from .commands.generate.model import model
from .commands.generate.template import template
from .commands.log.cluster import cluster as log_cluster
from .commands.log.ctrl import ctrl as log_ctrl
from .commands.state.get import get
from .commands.state.inject import inject


@click.group(context_settings=dict(help_option_names=["-h", "--help"]))
def yaookctl():
    """CLI tool to manage yaook/incubator/k8s-capi-provider workload clusters."""


@yaookctl.group(context_settings=dict(help_option_names=["-h", "--help"]))
def generate():
    """Generate cluster related files."""


@yaookctl.group(context_settings=dict(help_option_names=["-h", "--help"]))
def logs():
    """Print cluster related logs."""


# Get/inject
yaookctl.add_command(get)
yaookctl.add_command(inject)

# Logs
logs.add_command(log_cluster)
logs.add_command(log_ctrl)

# Generate
generate.add_command(cluster)
generate.add_command(crd)
generate.add_command(template)
generate.add_command(model)


if __name__ == "__main__":
    yaookctl()

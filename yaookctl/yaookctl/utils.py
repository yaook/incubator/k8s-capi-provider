"""yaookctl global utils module."""
import contextlib
import functools

import click
from kubernetes import config
from kubernetes.client import CoreV1Api
from kubernetes.config import ConfigException


@contextlib.contextmanager
def kubernetes_client(client=CoreV1Api):
    """Get kubernetes client.

    Loads the kubeconfig file and returns the k8s client.

    Args:
        client (kubernetes.client, optional): K8s client to be returned.
            Defaults to CoreV1Api.

    Returns:
        kubernetes.client: Kubernetes client

    Raises:
        click.ClickException: Unable to load kubeconfig file.

    TODO: Replace static client config with the dynamic client
      see: https://github.com/kubernetes-client/python/tree/master/examples/dynamic-client  # noqa: E501
    """
    try:
        config.load_incluster_config()
    except ConfigException:
        try:
            config.load_kube_config()
        except ConfigException as e:
            raise click.ClickException(f"Unable to load kubeconfig, error {str(e)}")

    yield client()


def with_client_v1(func):
    """Load kubeconfig and inject v1 k8s client to the wrapped function."""

    @click.pass_context
    @functools.wraps(func)
    def wrapped(ctx: click.Context, *args, **kwargs):
        with kubernetes_client() as client:
            return func(ctx, client, *args, **kwargs)

    return wrapped

# K8s CAPI Provider

Welcome to the k8s-capi-provider repository! :rocket:

k8s-capi-provider is [yaook][yaook] incubator project. It is intended to be a
Lifecycle-Management (LCM) tool which provides, operates and customize, highly available,
and flexible kubernetes cluster. It adopts [CAPI][capi] approach and encapsulates
LCM logic from another yaook project [yaook-k8s][yaook-k8s]. It is based on [kopf][kopf] framework.

## Documentation

Documentation is currently available at https://yaook.gitlab.io/incubator/k8s-capi-provider/.

The documentation is created with [mdBook](https://github.com/rust-lang/mdBook) and located under [docs/](docs/).
Built instructions for the documentation can be found in the [respective README](docs/README.md).

### Getting Started

Visit handy [tutorial](docs/src/tutorial.md) and try to manage workload cluster by k8s-capi-provider LCM.

### Local Development

Visit a [guide](docs/src/local_dev.md) on how to set up local development environment.

### yaookctl

`yaookctl` tool is currently used to generate CR files using CR templates and environment variables.
See [yaookctl](yaookctl/README.md) for details.

## Contributing

If you wish to contribute, please read the [Contributing](docs/src/contributing.md) guide.

<!-- References -->

[yaook]: https://gitlab.com/yaook
[yaook-k8s]: https://gitlab.com/yaook/k8s
[capi]: https://cluster-api.sigs.k8s.io
[kopf]: https://kopf.readthedocs.io

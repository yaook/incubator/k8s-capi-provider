import sys
from asyncio import StreamReader
from pathlib import Path

import pytest
from loguru import logger

from src.settings import ProviderSettings, get_settings
from src.utils import logging

SETTINGS: ProviderSettings = get_settings()


@pytest.mark.parametrize(
    "settings_ctrl, settings_path",
    [
        (SETTINGS.cluster_ctrl, "cluster_ctrl.path"),
        (SETTINGS.service_ctrl, "service_ctrl.path"),
    ],
)
@pytest.mark.asyncio
async def test_register(tmp_path, override_settings, settings_ctrl, settings_path):
    logger.remove()  # Remove loggers

    @logging.register(settings=settings_ctrl)
    async def register_logger(*args, **kwargs):
        pass

    cluster_name = "test-name"
    cluster_namespace = "test-namespace"
    cluster_body = {
        "metadata": {
            "name": cluster_name,
            "namespace": cluster_namespace,
        }
    }

    with override_settings({settings_path: tmp_path}):
        # Register loggers multiple times
        _ = [await register_logger(body=cluster_body) for _ in range(32)]

        counter = 0
        sinks = []
        for handler in logging.get_handlers(cluster_name, cluster_namespace):
            if hasattr(handler._sink, "_file"):
                sinks.append(handler._sink._file.name)
            if hasattr(handler._sink, "_stream"):
                sinks.append(handler._sink._stream)
            counter = counter + 1

        logs = settings_ctrl.path / cluster_namespace / cluster_name

        assert counter == 4  # info.log + warning.log + error.log + error 2 stderr
        assert str(logs / settings_ctrl.logging.path_info) in sinks
        assert str(logs / settings_ctrl.logging.path_warning) in sinks
        assert str(logs / settings_ctrl.logging.path_error) in sinks
        assert sys.stderr in sinks


@pytest.mark.parametrize(
    "settings_ctrl, settings_path",
    [
        (SETTINGS.cluster_ctrl, "cluster_ctrl.path"),
        (SETTINGS.service_ctrl, "service_ctrl.path"),
    ],
)
@pytest.mark.asyncio
async def test_register_two_clusters(
    tmp_path, override_settings, settings_ctrl, settings_path
):
    logger.remove()  # Remove loggers

    @logging.register(settings=settings_ctrl)
    async def register_logger(*args, **kwargs):
        pass

    cluster_name_one = "test-name-one"
    cluster_namespace_one = "test-namespace-one"
    cluster_body_one = {
        "metadata": {
            "name": cluster_name_one,
            "namespace": cluster_namespace_one,
        }
    }
    cluster_name_two = "test-name-two"
    cluster_namespace_two = "test-namespace-two"
    cluster_body_two = {
        "metadata": {
            "name": cluster_name_two,
            "namespace": cluster_namespace_two,
        }
    }

    with override_settings({settings_path: tmp_path}):
        # Register loggers multiple times
        _ = [await register_logger(body=cluster_body_one) for _ in range(32)]
        _ = [await register_logger(body=cluster_body_two) for _ in range(32)]

        for namespace, name in (
            (cluster_namespace_one, cluster_name_one),
            (cluster_namespace_two, cluster_name_two),
        ):
            counter = 0
            sinks = []
            for handler in logging.get_handlers(name, namespace):
                if hasattr(handler._sink, "_file"):
                    sinks.append(handler._sink._file.name)
                if hasattr(handler._sink, "_stream"):
                    sinks.append(handler._sink._stream)
                counter = counter + 1

            logs = settings_ctrl.path / namespace / name

            assert counter == 4  # info.log + warning.log + error.log + error 2 stderr
            assert str(logs / settings_ctrl.logging.path_info) in sinks
            assert str(logs / settings_ctrl.logging.path_warning) in sinks
            assert str(logs / settings_ctrl.logging.path_error) in sinks
            assert sys.stderr in sinks


@pytest.mark.parametrize(
    "settings_ctrl, settings_path",
    [
        (SETTINGS.cluster_ctrl, "cluster_ctrl.path"),
        (SETTINGS.service_ctrl, "service_ctrl.path"),
    ],
)
@pytest.mark.asyncio
async def test_unregister(tmp_path, override_settings, settings_ctrl, settings_path):
    logger.remove()  # Remove loggers

    @logging.register(settings=settings_ctrl)
    async def register_logger(*args, **kwargs):
        pass

    @logging.unregister(settings=settings_ctrl)
    async def unregister_logger(*args, **kwargs):
        pass

    cluster_name = "test-name"
    cluster_namespace = "test-namespace"
    cluster_body = {
        "metadata": {
            "name": cluster_name,
            "namespace": cluster_namespace,
        }
    }

    with override_settings({settings_path: tmp_path}):
        # Register loggers multiple times
        _ = [await register_logger(body=cluster_body) for _ in range(32)]

        assert len(list(logging.get_handlers(cluster_name, cluster_namespace))) == 4

        await unregister_logger(body=cluster_body)

        assert len(list(logging.get_handlers(cluster_name, cluster_namespace))) == 0


@pytest.mark.parametrize(
    "settings_ctrl, settings_path, settings_log",
    [
        (SETTINGS.cluster_ctrl, "cluster_ctrl.path", "cluster_ctrl.logging"),
        (SETTINGS.service_ctrl, "service_ctrl.path", "service_ctrl.logging"),
    ],
)
@pytest.mark.asyncio
async def test_rotation_retention(
    tmp_path, override_settings, settings_ctrl, settings_path, settings_log
):
    logger.remove()  # Remove loggers

    @logging.register(settings=settings_ctrl)
    async def register_logger(*args, **kwargs):
        pass

    cluster_name = "test-name"
    cluster_namespace = "test-namespace"
    cluster_body = {
        "metadata": {
            "name": cluster_name,
            "namespace": cluster_namespace,
        }
    }
    number_of_logs = 32
    number_of_backups = 10

    with override_settings(
        {
            settings_path: tmp_path,
            # Loguru does not split log messages across log files.
            # The log size is checked after the
            # flushing of a single log message to the log file.
            settings_log + ".rotation": 1,
            # At least one backup should be used for proper rotation behavior.
            settings_log + ".backup_count": number_of_backups,
        }
    ):
        await register_logger(body=cluster_body)

        test_logger = logger.bind(namespace=cluster_namespace, cluster=cluster_name)

        for message_number in range(1, number_of_logs + 1):
            test_logger.info(f"hello logger - message {message_number}.")

        await test_logger.complete()
        logs = settings_ctrl.path / cluster_namespace / cluster_name

        no_of_logs = Path(logs / settings_ctrl.logging.path_info.parent).iterdir()

        # Empty error.log + warning.log + info.log + 10 x info log backup
        assert len(list(no_of_logs)) == number_of_backups + 3
        # Check info.log
        assert (
            f"hello logger - message {number_of_logs}."
            in Path(logs / settings_ctrl.logging.path_info).read_text()
        )
        # Check info.log.1 backup
        assert (
            f"hello logger - message {number_of_logs - 1}."
            in Path(logs / (str(settings_ctrl.logging.path_info) + ".1")).read_text()
        )
        # Check info.log.10 backup
        assert (
            f"hello logger - message {number_of_logs - 10}."
            in Path(logs / (str(settings_ctrl.logging.path_info) + ".10")).read_text()
        )


@pytest.mark.parametrize(
    "settings_ctrl, settings_path, settings_log",
    [
        (SETTINGS.cluster_ctrl, "cluster_ctrl.path", "cluster_ctrl.logging"),
        (SETTINGS.service_ctrl, "service_ctrl.path", "service_ctrl.logging"),
    ],
)
@pytest.mark.asyncio
async def test_filter(
    tmp_path, override_settings, capsys, settings_ctrl, settings_path, settings_log
):
    logger.remove()  # Remove loggers

    @logging.register(settings=settings_ctrl)
    async def register_logger(*args, **kwargs):
        pass

    cluster_name = "test-name"
    cluster_namespace = "test-namespace"
    cluster_body = {
        "metadata": {
            "name": cluster_name,
            "namespace": cluster_namespace,
        }
    }

    with override_settings(
        {
            settings_path: tmp_path,
            # Loguru does not split log messages across log files.
            # The log size is checked after the
            # flushing of a single log message to the log file.
            settings_log + ".rotation": 1000,
            # At least one backup should be used for proper rotation behavior.
            settings_log + ".backup_count": 1,
        }
    ):
        await register_logger(body=cluster_body)

        test_logger = logger.bind(namespace=cluster_namespace, cluster=cluster_name)

        test_logger.info("[warning] hello warning - should be logged to info.")
        test_logger.warning("[warning] hello warning - should be logged to warning.")
        test_logger.error("hello error - should be logged to error.")

        await test_logger.complete()
        logs = settings_ctrl.path / cluster_namespace / cluster_name

        no_of_logs = Path(logs / settings_ctrl.logging.path_error.parent).iterdir()

        assert len(list(no_of_logs)) == 3  # error.log + warning.log + info.log
        # Check info.log
        assert (
            "[warning] hello warning - should be logged to info."
            in Path(logs / settings_ctrl.logging.path_info).read_text()
        )
        # Check warning.log
        assert (
            "[warning] hello warning - should be logged to warning."
            in Path(logs / settings_ctrl.logging.path_warning).read_text()
        )
        # Check error.log
        assert (
            "hello error - should be logged to error."
            in Path(logs / settings_ctrl.logging.path_error).read_text()
        )
        captured = capsys.readouterr()
        assert not captured.out
        assert not captured.err.rstrip() == "hello error - should be logged to error."


@pytest.mark.asyncio
async def test_write_stream_stdout(capsys):
    logger.remove()  # Remove loggers

    def _filter(record):
        return (
            record["level"].name == "INFO"
            and namespace == record["extra"].get("namespace")
            and name == record["extra"].get("cluster")
        )

    expected = "hello write stream stdout"
    logger.add(
        sys.stdout,
        format="{message}",
        filter=_filter,
        enqueue=True,
    )

    name = "test-name"
    namespace = "test-namespace"
    stream = StreamReader()
    stream.feed_data(expected.encode())
    stream.feed_eof()

    await logging.write_stream(stream, name, namespace)

    captured = capsys.readouterr()
    assert captured.out.rstrip() == expected


@pytest.mark.asyncio
async def test_write_stream_stderr(capsys):
    logger.remove()  # Remove loggers

    def _filter(record):
        return (
            record["level"].name == "ERROR"
            and namespace == record["extra"].get("namespace")
            and name == record["extra"].get("cluster")
        )

    expected = "hello write stream stderr"
    logger.add(
        sys.stderr,
        format="{message}",
        filter=_filter,
        enqueue=True,
    )

    name = "test-name"
    namespace = "test-namespace"
    stream = StreamReader()
    stream.feed_data(expected.encode())
    stream.feed_eof()

    await logging.write_stream(stream, name, namespace, is_stderr=True)

    captured = capsys.readouterr()
    assert captured.err.rstrip() == expected
    assert not captured.out


@pytest.mark.asyncio
async def test_write_stream_buffer():
    logger.remove()  # Remove loggers

    expected = "hello write stream buffer"
    logger.add(
        sys.stderr,
        format="{message}",
        enqueue=True,
    )

    name = "test-name"
    namespace = "test-namespace"
    stream = StreamReader()
    stream.feed_data(expected.encode())
    stream.feed_eof()

    buffer = await logging.write_stream(stream, name, namespace, is_buffered=True)

    assert buffer.decode() == expected


@pytest.mark.asyncio
async def test_write_stream_buffer_skip_warning():
    logger.remove()  # Remove loggers

    expected = "hello write stream buffer"
    msg = f"""
    [warning] hello warning stream buffer
    {expected}
    [warning] hello again warning stream buffer
    """
    logger.add(
        sys.stderr,
        format="{message}",
        enqueue=True,
    )

    name = "test-name"
    namespace = "test-namespace"
    stream = StreamReader()
    stream.feed_data(msg.encode())
    stream.feed_eof()

    buffer = await logging.write_stream(
        stream, name, namespace, is_buffered=True, is_stderr=True
    )

    assert buffer.decode().strip() == expected

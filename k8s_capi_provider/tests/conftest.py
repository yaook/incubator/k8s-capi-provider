import contextlib
import os
import shutil
from functools import reduce
from importlib import reload
from pathlib import Path
from subprocess import CalledProcessError
from typing import NamedTuple

import kopf
import pytest
import yaml
from kopf._core.intents import registries
from kopf.testing import KopfRunner
from kubernetes import config as k8s_client_config
from mock import AsyncMock, MagicMock
from pytest_kind import KindCluster

from src.settings import CRD, ProviderSettings, get_settings
from src.utils.kubernetes import Kubernetes
from src.utils.openstack import Flavor

SETTINGS: ProviderSettings = get_settings()
CRDS: CRD = SETTINGS.crd
K8S_GIT_DIR = SETTINGS.yaook_git.directory


class KubernetesPytest(NamedTuple):
    name: str = "pytest"
    kubeconfig_path: Path = Path(".pytest-kind") / name / "kubeconfig"


class SecretResource(NamedTuple):
    kind: str
    name: str
    namespace: str = "default"
    api_version: str = "v1"

    def remove(self):
        """Remove resource from cluster."""
        _, config_file = KubernetesPytest()
        k8s = Kubernetes(config_file=config_file)
        k8s.remove_secret(
            name=self.name,
            namespace=self.namespace,
            ignore_errors=True,
        )


class CustomResource(NamedTuple):
    kind: str
    plural: str
    name: str
    namespace: str = "default"
    api_version: str = "yaook.mk8s.io/v1"

    def patch(self, body: dict, status=False):
        """Patch custom resource.

        Args:
            body (dict): The patch to be applied to the resource.
            status (bool, optional): If True, patch only the status subresource.
                Defaults to False.

        Note: Instance of pytest-kind could not be used here because the
          kubectl-patch of the sub resources (e.g. status) is supported from k8s v1.24.
          We didn't want to force the version of k8s in unit-tests as
          the k8s-capi-provider could run in lower versions of k8s as well.

        """
        _, config_file = KubernetesPytest()
        return Kubernetes(config_file=config_file).patch_cr(
            name=self.name,
            body=body,
            plural=self.plural,
            namespace=self.namespace,
            status=status,
        )

    def get(self) -> dict:
        """Get custom resource.

        Returns:
            dict: Custom resource.

        """
        _, config_file = KubernetesPytest()
        return Kubernetes(config_file=config_file).get_cr(
            name=self.name,
            plural=self.plural,
            namespace=self.namespace,
        )

    def remove(self):
        """Remove finalizers and delete resource from cluster."""
        _, config_file = KubernetesPytest()
        k8s = Kubernetes(config_file=config_file)
        k8s.patch_cr(
            name=self.name,
            body={"metadata": {"finalizers": []}},
            plural=self.plural,
            namespace=self.namespace,
            ignore_errors=True,
        )
        k8s.delete_cr(
            name=self.name,
            plural=self.plural,
            namespace=self.namespace,
            ignore_errors=True,
        )


class YaookCustomResources(NamedTuple):
    path: Path = Path("./manifests/example-cluster.yaml")
    cluster: CustomResource = CustomResource(
        name="example-cluster",
        kind="YaookCluster",
        plural=CRDS.cluster_plural,
    )
    control_plane: CustomResource = CustomResource(
        name="example-cluster-control-plane",
        kind="YaookControlPlane",
        plural=CRDS.control_plane_plural,
    )
    machine_template_control: CustomResource = CustomResource(
        name="example-cluster-control-plane",
        kind="YaookMachineTemplate",
        plural=CRDS.machine_template_plural,
    )
    machine_deployment: CustomResource = CustomResource(
        name="example-cluster-md-0",
        kind="YaookMachineDeployment",
        plural=CRDS.machine_deployment_plural,
    )
    machine_template_deploy: CustomResource = CustomResource(
        name="example-cluster-md-0",
        kind="YaookMachineTemplate",
        plural=CRDS.machine_template_plural,
    )
    config_template: CustomResource = CustomResource(
        name="example-cluster",
        kind="YaookConfigTemplate",
        plural=CRDS.config_template_plural,
    )
    service: CustomResource = CustomResource(
        name="example-cluster-service",
        kind="YaookService",
        plural=CRDS.service_plural,
    )
    secret: SecretResource = SecretResource(
        name="example-cluster-openrc",
        kind="Secret",
    )

    def get_raw(self, kind: str) -> dict:
        """Get CR in raw (dict) format.

        Args:
            kind (str): Kind of the wanted resource.

        Returns:
            dict: CR in raw format.

        Raises:
            ValueError: If given kind does not exist in CRDs.

        """
        for crd in yaml.safe_load_all(self.path.open()):
            if crd["kind"] == kind:
                return crd

        raise ValueError(f"Kind {kind} not found in project's CRDs.")


class YaookCustomResourceDefinitions(NamedTuple):
    path: Path = Path("./chart/crds/clusters.yaml")
    # TODO: define resources if needed.


def pytest_addoption(parser):
    """Register :mod:`argparse`-style options and ini-style config values for pytest.

    Called once at the beginning of a test run.

    Args:
        parser (pytest.config.Parser): pytest parser

    """
    parser.addoption(
        "--gitlab-ci", action="store_true", default=False, help="gitlab CI run"
    )
    parser.addoption(
        "--run-slow", action="store_true", default=False, help="run slow tests"
    )


def pytest_configure(config):
    """Allows plugins to perform initial configuration.

    Register markers.

    Args:
        config (pytest.config.Config): config object

    """
    # Markers
    config.addinivalue_line("markers", "slow: mark slow test")
    config.addinivalue_line("markers", "handler: mark handler test")


def pytest_collection_modifyitems(config, items):
    """Called after pytest collection has been performed, may filter or
    re-order the items in-place.

    Args:
        config (pytest.config.Config): config object
        items (List[pytest.nodes.Item]): list of test item objects

    """
    if not config.getoption("--run-slow"):
        skip_kind = pytest.mark.skip(reason="need --run-slow option to run")
        for item in items:
            if "slow" in item.keywords:
                item.add_marker(skip_kind)


@pytest.fixture(scope="session")
def cleanup_kind(request):
    """Cleanup files produced by KinD once session finished.

    Skipp if `--keep-cluster` option is used.

    """

    def remove_dirs():
        for _dir in [".kube", ".pytest-kind"]:
            try:
                shutil.rmtree(_dir)
            except FileNotFoundError:
                pass

    if request.config.getoption("keep_cluster"):
        return

    request.addfinalizer(remove_dirs)


def modify_kubeconfig(kubeconfig_path, source="0.0.0.0", target="docker"):
    """Modify k8s kubeconfig host for Gitlab CI run."""

    with open(kubeconfig_path, "r") as fd:
        kubeconfig = fd.read()
        kubeconfig = kubeconfig.replace(source, target)

    with open(kubeconfig_path, "w") as fd:
        fd.write(kubeconfig)


@pytest.fixture(scope="session")
def kind(request, cleanup_kind):
    """Provide a kubernetes KinD cluster as test fixture.

    GitLab CI:
        Docker in Docker (DinD) service executes
        KinD cluster in GitLab CI environment.
        In order to be able to reach the cluster
        through DinD service, we need to modify KinD
        host in kubeconfig from `0.0.0.0` to the
        DinD service name (`docker`) as well as
        add the DinD service name (`docker`) to the
        Api server certSANs (this is done within
        custom KinD config)

    """
    name, kubeconfig_path = KubernetesPytest()
    gitlab = request.config.getoption("gitlab_ci")
    keep = request.config.getoption("keep_cluster")

    os.environ["KUBECONFIG"] = str(kubeconfig_path)
    config_file = Path("./ci") / "gitlab" / "kind-config.yaml" if gitlab else None

    cluster = KindCluster(name)
    cluster.create(config_file=config_file)

    if gitlab:
        modify_kubeconfig(kubeconfig_path)

    yield cluster

    if not keep:
        cluster.delete()


@pytest.fixture
def cleanup(kind, request, yaook_crds, yaook_crs):
    """Cleanup files and k8s objects produced by handler once test finished.

    TODO: Improve cleanup and handle deletion of unexpected resources

    """

    def remove_dirs():
        for _dir in [K8S_GIT_DIR]:
            try:
                shutil.rmtree(_dir)
            except FileNotFoundError:
                pass

    def remove_resources():
        for resource in yaook_crs:
            if hasattr(resource, "remove"):
                resource.remove()

        try:
            kind.kubectl("delete", "-f", str(yaook_crds.path))
        except CalledProcessError:
            pass

    request.addfinalizer(remove_dirs)
    request.addfinalizer(remove_resources)


@pytest.fixture
def yaook_crds() -> YaookCustomResourceDefinitions:
    return YaookCustomResourceDefinitions()


@pytest.fixture
def yaook_crs() -> YaookCustomResources:
    return YaookCustomResources()


@pytest.fixture(scope="module")
def reload_registry():
    """Reload kopf global registry.

    Kopf uses global registry variable which is
    populated by the `kopf.on` decorators, and is used
    to register the resources being watched and handled.
    This global registry is created when
    :mod:`kopf._core.intents.registries is loaded and then it
    is used by the `kopf.on` decorators.
    To prevent global state sharing between operators
    (k8s-capi-provider controllers) a global registry
    has to be reloaded when operator is executed.
    Alternatively, we can create and use dedicated
    :class:`kopf.OperatorRegistry` per operator, which
    is recommended in kopf docs, see:
    https://kopf.readthedocs.io/en/stable/embedding/#multiple-operators
    But, then we have to pass attribute `registry=...` to every
    handler explicitly and embed kopf operator into code
    which is not necessary as we use dedicated environments
    (containers) per operator.

    FIXME: Find a better solution here.

    """
    reload(registries)


@pytest.fixture
def cluster_ctrl():
    """Create k8s-capi-provider cluster controller"""
    # Override default timeouts
    settings = kopf.OperatorSettings()
    settings.watching.server_timeout = 10
    settings.networking.request_timeout = 10
    _, kubeconfig_path = KubernetesPytest()
    # Load kubeconfig for k8s client used within operator
    k8s_client_config.load_kube_config(config_file=str(kubeconfig_path))

    @kopf.on.login()
    def login_fn(**kwargs):
        return kopf.login_with_kubeconfig(**kwargs)

    return KopfRunner(
        [
            "run",
            "--standalone",
            "-A",
            "-m",
            "src.controllers.cluster.run",
        ],
        timeout=60,
        settings=settings,
    )


@pytest.fixture()
def service_ctrl():
    """Create k8s-capi-provider service controller"""
    # Override default timeouts
    settings = kopf.OperatorSettings()
    settings.watching.server_timeout = 10
    settings.networking.request_timeout = 10
    _, kubeconfig_path = KubernetesPytest()
    k8s_client_config.load_kube_config(config_file=str(kubeconfig_path))

    @kopf.on.login()
    def login_fn(**kwargs):
        return kopf.login_with_kubeconfig(**kwargs)

    return KopfRunner(
        [
            "run",
            "--standalone",
            "-A",
            "-m",
            "src.controllers.service.run",
        ],
        timeout=60,
        settings=settings,
    )


def nested_setattr(obj, attr, val):
    pre, _, post = attr.rpartition(".")
    return setattr(nested_getattr(obj, pre) if pre else obj, post, val)


def nested_getattr(obj, attr, *args):
    def _getattr(obj, attr):
        return getattr(obj, attr, *args)

    return reduce(_getattr, [obj] + attr.split("."))


@contextlib.contextmanager
def override(overrides):
    original = {}
    settings = get_settings()
    try:
        for key, value in overrides.items():
            original[key] = nested_getattr(settings, key)
            nested_setattr(settings, key, value)

        yield

    finally:
        for key, value in original.items():
            nested_setattr(settings, key, value)


@pytest.fixture
def override_settings():
    yield override


@pytest.fixture()
def mock_cluster_exec(mocker):
    return mocker.patch(
        "src.controllers.cluster.reconcile.execute",
        side_effect=AsyncMock,
        autospec=True,
    )


@pytest.fixture()
def mock_openstack_calls(mocker, limits, flavors):
    return mocker.patch.multiple(
        "src.utils.openstack.OpenStack",
        get_limits=MagicMock(return_value=limits),
        get_flavors=MagicMock(return_value=flavors),
    )


@pytest.fixture()
def mock_service_exec(mocker):
    return mocker.patch(
        "src.controllers.service.reconcile.execute",
        side_effect=AsyncMock,
        autospec=True,
    )


@pytest.fixture
def kubernetes():
    """Kubernetes client/utils instance fixture.

    Use KinD instance kubeconfig only when
    `--run-slow` argument is used and KinD
    instance exists.

    Returns:
        Kubernetes: Kubernetes instance.

    """
    _, config_file = KubernetesPytest()
    if config_file.exists():
        return Kubernetes(config_file=config_file)

    return Kubernetes()


@pytest.fixture
def limits():
    return {
        "maxSecurityGroups": 10,
        "maxServerGroups": 10,
        "maxTotalCores": 10,
        "maxTotalFloatingIps": 5,
        "maxTotalInstances": 10,
        "maxTotalRAMSize": 51200,
        "totalCoresUsed": 0,
        "totalFloatingIpsUsed": 0,
        "totalInstancesUsed": 0,
        "totalRAMUsed": 0,
        "totalSecurityGroupsUsed": 1,
        "totalServerGroupsUsed": 0,
    }


@pytest.fixture
def flavors():
    return {
        "L": Flavor(name="L", ram=8192, vcpus=4),
        "L.mem+": Flavor(name="L.mem+", ram=15360, vcpus=4),
        "M": Flavor(name="M", ram=4096, vcpus=2),
        "M.mem+": Flavor(name="M.mem+", ram=8192, vcpus=2),
        "S": Flavor(name="S", ram=2048, vcpus=1),
        "S.mem+": Flavor(name="S.mem+", ram=4096, vcpus=1),
        "XL": Flavor(name="XL", ram=15360, vcpus=8),
        "XL.mem+": Flavor(name="XL.mem+", ram=30720, vcpus=8),
        "XS": Flavor(name="XS", ram=1024, vcpus=1),
    }

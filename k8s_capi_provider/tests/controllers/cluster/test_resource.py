import kopf
import pytest

from src.data.cluster import YaookClusterResource
from src.exceptions import ProviderPermanentError, ProviderPermanentErrorReason


def test_resource(yaook_crs, kubernetes):
    body = yaook_crs.get_raw(kind=yaook_crs.cluster.kind)
    cluster_resource = YaookClusterResource(body=body, kubernetes=kubernetes)
    assert body["metadata"]["name"] == cluster_resource.name


def test_resource_from_kopf(yaook_crs, kubernetes):
    body = yaook_crs.get_raw(kind=yaook_crs.cluster.kind)
    kopf_body = kopf.Body(body)
    cluster_resource = YaookClusterResource.from_kopf(
        body=kopf_body, kubernetes=kubernetes
    )
    assert body["metadata"]["name"] == cluster_resource.name


@pytest.mark.slow
def test_resource_from_k8s(kind, yaook_crs, yaook_crds, kubernetes, cleanup):
    kind.kubectl("apply", "-f", yaook_crds.path)
    kind.kubectl("apply", "-f", yaook_crs.path)

    cluster_resource = YaookClusterResource.from_k8s(
        name=yaook_crs.cluster.name,
        namespace=yaook_crs.cluster.namespace,
        kubernetes=kubernetes,
    )

    assert yaook_crs.cluster.name == cluster_resource.name
    assert yaook_crs.cluster.namespace == cluster_resource.namespace


def test_resource_validation(yaook_crs, kubernetes):
    body = yaook_crs.get_raw(kind=yaook_crs.cluster.kind)
    body.pop("spec")  # remove spec

    with pytest.raises(ProviderPermanentError) as error:
        YaookClusterResource(body=body, kubernetes=kubernetes)

    assert ProviderPermanentErrorReason.RESOURCE_VALIDATION_ERROR.name in str(error)


def test_get_config(yaook_crs, kubernetes):
    body = yaook_crs.get_raw(kind=yaook_crs.cluster.kind)
    cluster_resource = YaookClusterResource(body=body, kubernetes=kubernetes)
    expected = {
        "terraform": {
            "azs": ["AZ1", "AZ2", "AZ3"],
            "dualstack_support": False,
            "gateway_flavor": "XS",
            "gateway_image_name": "Debian 11 (bullseye)",
            "public_network": "shared-public-IPv4",
            "subnet_cidr": "172.30.154.0/24",
        },
        "wireguard": {
            "ip_cidr": "172.30.153.64/26",
            "ip_gw": "172.30.153.65/26",
            "peers": [{"ident": "test@example.com", "pub_key": "test"}],
            "port": 7777,
        },
        "ipsec": {},
        "cah-users": {"rollout": False},
        "passwordstore": {},
        "miscellaneous": {"wireguard_on_workers": False},
        "load-balancing": {},
    }
    assert expected == cluster_resource.get_config()

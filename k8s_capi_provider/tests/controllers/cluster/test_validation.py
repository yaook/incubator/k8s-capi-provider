import pytest

from src.controllers.cluster.reconcile import validate_and_update_terraform_config
from src.data.cluster import YaookClusterResource
from src.utils.openstack import OpenStack


@pytest.mark.slow
def test_validate_and_update_terraform_config(
    mock_openstack_calls, kind, yaook_crs, yaook_crds, kubernetes, cleanup
):
    kind.kubectl("apply", "-f", yaook_crds.path)
    kind.kubectl("apply", "-f", yaook_crs.path)

    cluster = YaookClusterResource.from_k8s(
        name=yaook_crs.cluster.name,
        namespace=yaook_crs.cluster.namespace,
        kubernetes=kubernetes,
    )
    openstack = OpenStack(cluster.get_openstack_rc())
    config = cluster.get_config()
    deps = cluster.get_dependencies()

    validate_and_update_terraform_config(
        config["terraform"],
        openstack.flavors,
        openstack.get_available_quotas(),
        cluster,
        deps.control_plane,
        deps.machine_deployments,
    )

    assert config["terraform"]["masters"] == 3
    assert config["terraform"]["workers"] == 0

import os
from pathlib import Path
from time import sleep

import pytest
import yaml
from kopf._kits.runner import KopfRunner

from src.data.base import ResourcePhase
from src.exceptions import ProviderTemporaryErrorReason
from src.settings import get_settings

# FIXME: Replace an arbitrary sleep constant with by a
#  proper :func:`wait_for_handler` function that waits
#  for a handler health check.
WAIT_FOR_HANDLER = 8  # [s]
NUMBER_OF_CMDS = 4
pytestmark = pytest.mark.usefixtures("reload_registry")


@pytest.mark.slow
@pytest.mark.handler
def test_on_startup(cluster_ctrl, kind, tmp_path, override_settings):
    """Test `on.startup` handler."""
    settings = get_settings()

    with override_settings({"yaook_git.directory": tmp_path}):
        with cluster_ctrl as ctrl:
            pass

    assert ctrl.exception is None
    assert ctrl.exit_code == 0
    assert "ERROR" not in ctrl.stdout

    assert tmp_path.absolute().exists()
    assert Path(os.getenv("MANAGED_K8S_GIT")).name == tmp_path.name

    assert (
        cluster_ctrl.settings.persistence.progress_storage.prefix
        == settings.cluster_ctrl.kopf_prefix
    )
    assert (
        cluster_ctrl.settings.persistence.diffbase_storage.prefix
        == settings.cluster_ctrl.kopf_prefix
    )


@pytest.mark.slow
@pytest.mark.handler
def test_waiting_for_dependency(
    cluster_ctrl, kind, yaook_crds, yaook_crs, tmp_path, mock_cluster_exec, cleanup
):
    """Test `on.create` handler."""
    # Remove cluster dependency (OS secret) from resources.
    crs_updated = tmp_path / "example-cluster-updated.yaml"
    crs_updated_list = []

    with open(yaook_crs.path) as crs:
        for manifest in yaml.safe_load_all(crs):
            if manifest["kind"] == yaook_crs.secret.kind:
                continue

            crs_updated_list.append(manifest)

    with open(crs_updated, "w") as fd:
        yaml.dump_all(crs_updated_list, fd)

    with cluster_ctrl as ctrl:
        kind.kubectl("apply", "-f", yaook_crds.path)
        kind.kubectl("apply", "-f", crs_updated)
        sleep(WAIT_FOR_HANDLER)
        cluster = yaook_crs.cluster.get()

    assert ctrl.exception is None
    assert ctrl.exit_code == 0
    assert ProviderTemporaryErrorReason.WAITING_FOR_RESOURCE.name in ctrl.stdout
    assert f"{yaook_crs.secret.name} is not ready yet." in ctrl.stdout

    assert mock_cluster_exec.call_count == 0
    assert cluster["status"]["phase"] == ResourcePhase.PENDING
    assert not cluster["status"]["ready"]


def error_in_stdout(stdout: str) -> bool:
    """Return True if in stdout is an error other than temporary.

    Args:
        stdout (str): The standard output as unicode string.

    Returns:
        bool: True if in stdout is an error other than temporary, otherwise False.
    """
    for line in stdout.splitlines():
        if "ERROR" in line and "failed temporarily" not in line:
            return True

    return False


@pytest.mark.slow
@pytest.mark.handler
def test_on_create(
    cluster_ctrl,
    kind,
    yaook_crds,
    yaook_crs,
    mock_cluster_exec,
    mock_openstack_calls,
    cleanup,
):
    """Test `on.create` handler."""
    with cluster_ctrl as ctrl:
        kind.kubectl("apply", "-f", yaook_crds.path)
        kind.kubectl("apply", "-f", yaook_crs.path)
        sleep(WAIT_FOR_HANDLER)
        cluster = yaook_crs.cluster.get()

    assert ctrl.exception is None
    assert ctrl.exit_code == 0
    assert not error_in_stdout(ctrl.stdout)

    assert mock_cluster_exec.call_count == NUMBER_OF_CMDS
    assert "Handler 'on_action' succeeded" in ctrl.stdout
    assert "Creation is processed: 1 succeeded; 0 failed." in ctrl.stdout
    assert cluster["status"]["phase"] == ResourcePhase.RUNNING
    assert cluster["status"]["ready"]


@pytest.mark.slow
@pytest.mark.handler
def test_on_update(
    cluster_ctrl,
    kind,
    yaook_crds,
    yaook_crs,
    mock_cluster_exec,
    mock_openstack_calls,
    cleanup,
):
    """Test `on.update` handler.

    1. create yaook custom resources
    2. update yaook custom resources
    """
    with cluster_ctrl as ctrl:
        kind.kubectl("apply", "-f", yaook_crds.path)
        kind.kubectl("apply", "-f", yaook_crs.path)
        sleep(WAIT_FOR_HANDLER)
        # Update YaookMachineDeployment custom resource
        yaook_crs.machine_deployment.patch({"spec": {"replicas": 10}})
        sleep(WAIT_FOR_HANDLER)
        cluster = yaook_crs.cluster.get()

    assert ctrl.exception is None
    assert ctrl.exit_code == 0
    assert not error_in_stdout(ctrl.stdout)

    assert mock_cluster_exec.call_count == 2 * NUMBER_OF_CMDS
    assert "Handler 'on_action' succeeded" in ctrl.stdout
    assert "Creation is processed: 1 succeeded; 0 failed." in ctrl.stdout
    assert "Updating is processed: 1 succeeded; 0 failed." in ctrl.stdout
    assert cluster["status"]["phase"] == ResourcePhase.RUNNING
    assert cluster["status"]["ready"]


@pytest.mark.slow
@pytest.mark.handler
def test_on_resume(
    cluster_ctrl,
    kind,
    yaook_crds,
    yaook_crs,
    mock_cluster_exec,
    mock_openstack_calls,
    cleanup,
):
    """Test `on.resume` handler.

    1. create yaook custom resources
    2. stop operator instance
    3. start operator instance and check if the state is resumed
    """
    with cluster_ctrl as ctrl:
        kind.kubectl("apply", "-f", yaook_crds.path)
        kind.kubectl("apply", "-f", yaook_crs.path)
        sleep(WAIT_FOR_HANDLER)

    args, *_ = cluster_ctrl.args
    settings = cluster_ctrl.settings

    with KopfRunner(
        args=args,
        settings=settings,
    ) as ctrl_restarted:
        sleep(WAIT_FOR_HANDLER + 5)
        cluster = yaook_crs.cluster.get()

    assert ctrl.exception is None
    assert ctrl.exit_code == 0
    assert not error_in_stdout(ctrl.stdout)
    assert "Handler 'on_action' succeeded" in ctrl.stdout
    assert "Creation is processed: 1 succeeded; 0 failed." in ctrl.stdout

    assert ctrl_restarted.exception is None
    assert ctrl_restarted.exit_code == 0
    assert not error_in_stdout(ctrl.stdout)
    assert "Handler 'on_action' succeeded" in ctrl_restarted.stdout
    assert "Resuming is processed: 1 succeeded; 0 failed." in ctrl_restarted.stdout

    assert mock_cluster_exec.call_count == 2 * NUMBER_OF_CMDS
    assert cluster["status"]["phase"] == ResourcePhase.RUNNING
    assert cluster["status"]["ready"]

import os
from pathlib import Path
from time import sleep

import pytest
from kopf._kits.runner import KopfRunner

from src.data.base import ResourcePhase
from src.exceptions import ProviderTemporaryErrorReason
from src.settings import get_settings

# FIXME: Replace an arbitrary sleep constant with by a
#  proper :func:`wait_for_handler` function that waits
#  for a handler health check.
WAIT_FOR_HANDLER = 8  # [s]
NUMBER_OF_CMDS = 3
pytestmark = pytest.mark.usefixtures("reload_registry")


@pytest.mark.slow
@pytest.mark.handler
def test_on_startup(service_ctrl, kind, tmp_path, override_settings):
    """Test `on.startup` handler."""
    settings = get_settings()

    with override_settings({"yaook_git.directory": tmp_path}):
        with service_ctrl as ctrl:
            pass

    assert ctrl.exception is None
    assert ctrl.exit_code == 0
    assert "ERROR" not in ctrl.stdout

    assert tmp_path.absolute().exists()
    assert Path(os.getenv("MANAGED_K8S_GIT")).name == tmp_path.name

    assert (
        service_ctrl.settings.persistence.progress_storage.prefix
        == settings.service_ctrl.kopf_prefix
    )
    assert (
        service_ctrl.settings.persistence.diffbase_storage.prefix
        == settings.service_ctrl.kopf_prefix
    )


@pytest.mark.slow
@pytest.mark.handler
def test_waiting_for_cluster(
    service_ctrl, kind, yaook_crds, yaook_crs, mock_service_exec, cleanup
):
    """Test `on.create` handler."""
    with service_ctrl as ctrl:
        kind.kubectl("apply", "-f", yaook_crds.path)
        kind.kubectl("apply", "-f", yaook_crs.path)
        sleep(WAIT_FOR_HANDLER)
        service = yaook_crs.service.get()

    assert ctrl.exception is None
    assert ctrl.exit_code == 0
    assert ProviderTemporaryErrorReason.WAITING_FOR_RESOURCE.name in ctrl.stdout
    assert f"{yaook_crs.cluster.name} is not ready yet." in ctrl.stdout

    assert mock_service_exec.call_count == 0
    assert service["status"]["phase"] == ResourcePhase.PENDING
    assert not service["status"]["ready"]


@pytest.mark.slow
@pytest.mark.handler
def test_on_create(
    service_ctrl, kind, yaook_crds, yaook_crs, mock_service_exec, cleanup
):
    """Test `on.create` handler."""
    with service_ctrl as ctrl:
        kind.kubectl("apply", "-f", yaook_crds.path)
        kind.kubectl("apply", "-f", yaook_crs.path)
        yaook_crs.cluster.patch(
            {"status": {"phase": ResourcePhase.RUNNING.value}}, status=True
        )
        sleep(WAIT_FOR_HANDLER)
        service = yaook_crs.service.get()

    assert ctrl.exception is None
    assert ctrl.exit_code == 0

    assert mock_service_exec.call_count == NUMBER_OF_CMDS
    assert "Handler 'on_action' succeeded" in ctrl.stdout
    assert "Creation is processed: 1 succeeded; 0 failed." in ctrl.stdout
    assert service["status"]["phase"] == ResourcePhase.RUNNING
    assert service["status"]["ready"]


@pytest.mark.slow
@pytest.mark.handler
def test_on_update(
    service_ctrl,
    kind,
    yaook_crds,
    yaook_crs,
    mock_service_exec,
    cleanup,
):
    """Test `on.update` handler.

    1. create yaook custom resources
    2. update yaook custom resources
    """
    with service_ctrl as ctrl:
        kind.kubectl("apply", "-f", yaook_crds.path)
        kind.kubectl("apply", "-f", yaook_crs.path)
        yaook_crs.cluster.patch(
            {"status": {"phase": ResourcePhase.RUNNING.value}}, status=True
        )
        sleep(WAIT_FOR_HANDLER)
        # Update YaookService custom resource
        yaook_crs.service.patch(
            {"spec": {"k8s-service-layer": {"ingress": {"enabled": True}}}}
        )
        sleep(WAIT_FOR_HANDLER)
        service = yaook_crs.service.get()

    assert ctrl.exception is None
    assert ctrl.exit_code == 0

    assert mock_service_exec.call_count == 2 * NUMBER_OF_CMDS
    assert "Handler 'on_action' succeeded" in ctrl.stdout
    assert "Creation is processed: 1 succeeded; 0 failed." in ctrl.stdout
    assert "Updating is processed: 1 succeeded; 0 failed." in ctrl.stdout
    assert service["status"]["phase"] == ResourcePhase.RUNNING
    assert service["status"]["ready"]


@pytest.mark.slow
@pytest.mark.handler
def test_on_resume(
    service_ctrl, kind, yaook_crds, yaook_crs, mock_service_exec, cleanup
):
    """Test `on.resume` handler.

    1. create yaook custom resources
    2. stop operator instance
    3. start operator instance and check if the state is resumed
    """
    with service_ctrl as ctrl:
        kind.kubectl("apply", "-f", yaook_crds.path)
        kind.kubectl("apply", "-f", yaook_crs.path)
        yaook_crs.cluster.patch(
            {"status": {"phase": ResourcePhase.RUNNING.value}}, status=True
        )
        sleep(WAIT_FOR_HANDLER)

    args, *_ = service_ctrl.args
    settings = service_ctrl.settings

    with KopfRunner(
        args=args,
        settings=settings,
    ) as ctrl_restarted:
        sleep(WAIT_FOR_HANDLER + 5)
        service = yaook_crs.service.get()

    assert ctrl.exception is None
    assert ctrl.exit_code == 0
    assert "Handler 'on_action' succeeded" in ctrl.stdout
    assert "Creation is processed: 1 succeeded; 0 failed." in ctrl.stdout

    assert ctrl_restarted.exception is None
    assert ctrl_restarted.exit_code == 0
    assert "ERROR" not in ctrl_restarted.stdout
    assert "Handler 'on_action' succeeded" in ctrl_restarted.stdout
    assert "Resuming is processed: 1 succeeded; 0 failed." in ctrl_restarted.stdout

    assert mock_service_exec.call_count == 2 * NUMBER_OF_CMDS
    assert service["status"]["phase"] == ResourcePhase.RUNNING
    assert service["status"]["ready"]

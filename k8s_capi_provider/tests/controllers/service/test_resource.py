import kopf
import pytest

from src.data.service import YaookServiceResource
from src.exceptions import ProviderPermanentError, ProviderPermanentErrorReason


def test_resource(yaook_crs, kubernetes):
    body = yaook_crs.get_raw(kind=yaook_crs.service.kind)
    service_resource = YaookServiceResource(body=body, kubernetes=kubernetes)
    assert body["metadata"]["name"] == service_resource.name


def test_resource_from_kopf(yaook_crs, kubernetes):
    body = yaook_crs.get_raw(kind=yaook_crs.service.kind)
    kopf_body = kopf.Body(body)
    service_resource = YaookServiceResource.from_kopf(
        body=kopf_body, kubernetes=kubernetes
    )
    assert body["metadata"]["name"] == service_resource.name


@pytest.mark.slow
def test_resource_from_k8s(kind, yaook_crs, yaook_crds, kubernetes, cleanup):
    kind.kubectl("apply", "-f", yaook_crds.path)
    kind.kubectl("apply", "-f", yaook_crs.path)

    service_resource = YaookServiceResource.from_k8s(
        name=yaook_crs.service.name,
        namespace=yaook_crs.service.namespace,
        kubernetes=kubernetes,
    )

    assert yaook_crs.service.name == service_resource.name
    assert yaook_crs.service.namespace == service_resource.namespace


def test_resource_validation(yaook_crs, kubernetes):
    body = yaook_crs.get_raw(kind=yaook_crs.service.kind)
    body.pop("spec")  # remove spec

    with pytest.raises(ProviderPermanentError) as error:
        YaookServiceResource(body=body, kubernetes=kubernetes)

    assert ProviderPermanentErrorReason.RESOURCE_VALIDATION_ERROR.name in str(error)


def test_get_config(yaook_crs, kubernetes):
    body = yaook_crs.get_raw(kind=yaook_crs.service.kind)
    service_resource = YaookServiceResource(body=body, kubernetes=kubernetes)
    expected = {
        "k8s-service-layer": {
            "cert-manager": {"enabled": False},
            "etcd-backup": {},
            "ingress": {"enabled": False},
            "prometheus": {"use_thanos": False},
            "rook": {"cluster_name": "rook-ceph", "namespace": "rook-ceph"},
            "vault": {},
        }
    }
    assert expected == service_resource.get_config()

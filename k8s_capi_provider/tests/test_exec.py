"""This module test reconcile function and its dependencies"""
from pathlib import Path

import pytest
from loguru import logger
from mock import AsyncMock

from src.exceptions import (
    ProviderPermanentError,
    ProviderPermanentErrorReason,
    ProviderTemporaryError,
    ProviderTemporaryErrorReason,
)
from src.settings import ProviderSettings, get_settings
from src.utils import common, logging
from src.utils.common import exec_cmd

SETTINGS: ProviderSettings = get_settings()


class ProcessMock(object):
    def __init__(
        self, stdout: bytes = None, stderr: bytes = None, return_code: int = 0
    ):
        self.stdout = stdout
        self.stderr = stderr
        self.return_code = return_code

    async def communicate(self, input=None):
        return await AsyncMock(return_value=(self.stdout, self.stderr))()

    @property
    def returncode(self):
        return self.return_code


@pytest.mark.asyncio
async def test_exec_cmd():
    cmd = ["echo", "hello"]

    stdout, stderr = await exec_cmd(cmd)

    assert not stdout
    assert not stderr


@pytest.mark.asyncio
async def test_exec_cmd_stdout():
    expected = "hello stdout"
    cmd = ["echo", "-n", expected]

    stdout, stderr = await exec_cmd(cmd, output=True)

    assert stdout.decode() == expected
    assert not stderr


@pytest.mark.asyncio
async def test_exec_cmd_input():
    expected = "hello stdin"
    cmd = ["cat"]

    stdout, stderr = await exec_cmd(cmd, cmd_input=expected.encode(), output=True)

    assert stdout.decode() == expected
    assert not stderr


@pytest.mark.asyncio
async def test_exec_cmd_stderr():
    expected = "hello stderr"
    cmd = ["python", "-c", f"import sys;sys.stderr.write('{expected}')"]

    stdout, stderr = await exec_cmd(cmd)

    assert stderr.decode() == expected
    assert not stdout


@pytest.mark.asyncio
async def test_exec_cmd_env():
    expected = "hello env"
    cmd = ["printenv", "test"]
    env = {"test": expected}

    stdout, stderr = await exec_cmd(cmd, env=env, output=True)

    assert stdout.decode().rstrip() == expected
    assert not stderr


@pytest.mark.asyncio
async def test_exec_cmd_cwd(tmp_path):
    cmd = ["pwd"]
    cwd = tmp_path / "hello_cwd"
    cwd.mkdir()

    stdout, stderr = await exec_cmd(cmd, cwd=cwd, output=True)

    assert stdout.decode().rstrip() == str(cwd)
    assert not stderr


@pytest.mark.asyncio
async def test_exec_cmd_return_code():
    expected = "hello return code 1"
    cmd = [
        "python",
        "-c",
        f"import sys;sys.stderr.write('{expected}');sys.exit(1)",
    ]

    with pytest.raises(ProviderPermanentError) as error:
        await exec_cmd(cmd)

    assert ProviderPermanentErrorReason.YAOOK_K8S_ERROR.name in str(error)
    assert expected in str(error)


@pytest.mark.asyncio
async def test_exec_cmd_unknown():
    """Test unknown command

    :func:`asyncio.create_subprocess_exec` throws error when trying
    to execute a non-existent file.
    """
    cmd = ["unknown"]

    with pytest.raises(ProviderPermanentError) as error:
        await exec_cmd(cmd)

    assert ProviderPermanentErrorReason.YAOOK_K8S_ERROR.name in str(error)
    assert "No such file or directory" in str(error)


@pytest.mark.asyncio
async def test_exec_cmd_timeout():
    cmd = ["sleep", "3"]

    with pytest.raises(ProviderTemporaryError) as error:
        await exec_cmd(cmd, timeout=1)

    assert ProviderTemporaryErrorReason.TIMEOUT_ERROR.name in str(error)
    assert f"{' '.join(cmd)} timeout" in str(error)


@pytest.mark.asyncio
async def test_exec_cmd_permanent_error(mocker):
    cmd = ["echo", "hello permanent error"]

    for reason in ProviderPermanentErrorReason:
        mock = mocker.patch.object(
            common,
            "get_subprocess",
            return_value=ProcessMock(stderr=reason.value.encode(), return_code=1),
        )

        with pytest.raises(ProviderPermanentError) as error:
            await exec_cmd(cmd)

        assert reason.name in str(error)
        assert mock.call_count == 1


@pytest.mark.asyncio
async def test_exec_cmd_temporary_error(mocker):
    cmd = ["echo", "hello temporary error"]

    for reason in ProviderTemporaryErrorReason:
        mock = mocker.patch.object(
            common,
            "get_subprocess",
            return_value=ProcessMock(stderr=reason.value.encode(), return_code=1),
        )

        with pytest.raises(ProviderTemporaryError) as error:
            await exec_cmd(cmd)

        assert reason.name in str(error)
        assert mock.call_count == 1


@pytest.mark.parametrize(
    "settings_ctrl, settings_path",
    [
        (SETTINGS.cluster_ctrl, "cluster_ctrl.path"),
        (SETTINGS.service_ctrl, "service_ctrl.path"),
    ],
)
@pytest.mark.asyncio
async def test_exec_cmd_log2file_stdout(
    tmp_path, override_settings, capsys, settings_ctrl, settings_path
):
    logger.remove()  # Remove loggers

    @logging.register(settings=settings_ctrl)
    async def register_logger(*args, **kwargs):
        pass

    expected = "hello file stdout"
    cluster_name = "test-name"
    cluster_namespace = "test-namespace"
    cluster_body = {
        "metadata": {
            "name": cluster_name,
            "namespace": cluster_namespace,
        }
    }

    with override_settings({settings_path: tmp_path}):
        await register_logger(body=cluster_body)
        await exec_cmd(
            cmd=["echo", "-n", expected],
            cluster_name=cluster_name,
            cluster_namespace=cluster_namespace,
            output=True,
            log2file=True,
        )
        logs = settings_ctrl.path / cluster_namespace / cluster_name

        assert expected in Path(logs / settings_ctrl.logging.path_info).read_text()
        assert not Path(logs / settings_ctrl.logging.path_error).read_text()
        # Ensure that the log is not printed to the stdout or stderr
        captured = capsys.readouterr()
        assert not captured.out
        assert not captured.err


@pytest.mark.parametrize(
    "settings_ctrl, settings_path",
    [
        (SETTINGS.cluster_ctrl, "cluster_ctrl.path"),
        (SETTINGS.service_ctrl, "service_ctrl.path"),
    ],
)
@pytest.mark.asyncio
async def test_exec_cmd_log2file_stderr(
    tmp_path, override_settings, capsys, settings_ctrl, settings_path
):
    logger.remove()  # Remove loggers

    @logging.register(settings=settings_ctrl)
    async def register_logger(*args, **kwargs):
        pass

    expected = "hello file stderr"
    cluster_name = "test-name"
    cluster_namespace = "test-namespace"
    cluster_body = {
        "metadata": {
            "name": cluster_name,
            "namespace": cluster_namespace,
        }
    }

    with override_settings({settings_path: tmp_path}):
        await register_logger(body=cluster_body)
        await exec_cmd(
            cmd=["python", "-c", f"import sys;sys.stderr.write('{expected}')"],
            cluster_name=cluster_name,
            cluster_namespace=cluster_namespace,
            output=True,
            log2file=True,
        )
        logs = settings_ctrl.path / cluster_namespace / cluster_name

        assert not Path(logs / settings_ctrl.logging.path_info).read_text()
        assert expected in Path(logs / settings_ctrl.logging.path_error).read_text()
        # Error should be printed to the stderr as well
        captured = capsys.readouterr()
        assert not captured.out
        assert expected in captured.err


@pytest.mark.parametrize(
    "settings_ctrl, settings_path",
    [
        (SETTINGS.cluster_ctrl, "cluster_ctrl.path"),
        (SETTINGS.service_ctrl, "service_ctrl.path"),
    ],
)
@pytest.mark.asyncio
async def test_exec_cmd_log2file_warning(
    tmp_path, override_settings, capsys, settings_ctrl, settings_path
):
    logger.remove()  # Remove loggers

    @logging.register(settings=settings_ctrl)
    async def register_logger(*args, **kwargs):
        pass

    cluster_name = "test-name"
    cluster_namespace = "test-namespace"
    cluster_body = {
        "metadata": {
            "name": cluster_name,
            "namespace": cluster_namespace,
        }
    }

    with override_settings({settings_path: tmp_path}):
        await register_logger(body=cluster_body)
        for warning_message in logging.YAOOK_WARNINGS:
            warning_message = warning_message.replace('"', '\\"')

            await exec_cmd(
                cmd=[
                    "python",
                    "-c",
                    f'import sys;sys.stderr.write("{warning_message}")',
                ],
                cluster_name=cluster_name,
                cluster_namespace=cluster_namespace,
                output=True,
                log2file=True,
            )
        logs = settings_ctrl.path / cluster_namespace / cluster_name

        assert not Path(logs / settings_ctrl.logging.path_info).read_text()
        assert not Path(logs / settings_ctrl.logging.path_error).read_text()
        for warning in logging.YAOOK_WARNINGS:
            assert (
                warning in Path(logs / settings_ctrl.logging.path_warning).read_text()
            )
        # Warnings should be printed to the warning log file only
        captured = capsys.readouterr()
        assert not captured.out
        assert not captured.err

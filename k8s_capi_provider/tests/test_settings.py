import pytest

from src.settings import get_settings


def test_settings_cache():
    expected_currsize = 1
    # Load settings multiple times
    _ = [get_settings() for _ in range(32)]
    assert get_settings.cache_info().currsize == expected_currsize


def test_override_settings(override_settings):
    settings = get_settings()
    assert settings.cluster_ctrl.logging.log2file

    with override_settings({"cluster_ctrl.logging.log2file": False}):
        assert not settings.cluster_ctrl.logging.log2file

    assert settings.cluster_ctrl.logging.log2file


def test_override_settings_with_error(override_settings):
    settings = get_settings()
    assert settings.cluster_ctrl.logging.log2file

    # Make sure first valid params are reverted to their original values
    with pytest.raises(
        AttributeError, match="'ProviderSettings' object has no attribute 'non_exists'"
    ):
        with override_settings(
            {
                "cluster_ctrl.logging.log2file": False,
                "non_exists": False,
            }
        ):
            pass

    assert settings.cluster_ctrl.logging.log2file

# DO NOT EDIT this file by hand!
# It is generated from corresponding Open API schema.
# Visit `yaookctl/commands/generate/model.py` for further details.

# generated by datamodel-codegen:
#   filename:  <stdin>

from __future__ import annotations

from typing import Optional

from pydantic import BaseModel


class Spec(BaseModel):
    flavor: Optional[str] = 'M'
    image: Optional[str] = 'Ubuntu 20.04 LTS x64'
    providerID: Optional[str] = None


class Status(BaseModel):
    ready: Optional[bool] = None


class Metadata(BaseModel):
    name: Optional[str] = None
    namespace: Optional[str] = None


class YaookMachineTemplate(BaseModel):
    spec: Spec
    status: Optional[Status] = None
    metadata: Optional[Metadata] = None

# DO NOT EDIT this file by hand!
# It is generated from corresponding Open API schema.
# Visit `yaookctl/commands/generate/model.py` for further details.

# generated by datamodel-codegen:
#   filename:  <stdin>

from __future__ import annotations

from typing import Any, Dict, Optional

from pydantic import BaseModel


class ConfigRef(BaseModel):
    apiVersion: Optional[str] = None
    kind: Optional[str] = None
    name: Optional[str] = None


class Bootstrap(BaseModel):
    configRef: Optional[ConfigRef] = None


class InfrastructureRef(BaseModel):
    apiVersion: Optional[str] = None
    kind: Optional[str] = None
    name: Optional[str] = None


class MachineTemplate(BaseModel):
    infrastructureRef: InfrastructureRef


class Spec(BaseModel):
    bootstrap: Bootstrap
    kubeadmConfigSpec: Optional[Dict[str, Any]] = {}
    machineTemplate: MachineTemplate
    replicas: Optional[int] = None
    version: str


class Status(BaseModel):
    flavor: Optional[str] = None
    replicas: Optional[int] = None


class Metadata(BaseModel):
    name: Optional[str] = None
    namespace: Optional[str] = None


class YaookControlPlane(BaseModel):
    spec: Spec
    status: Optional[Status] = None
    metadata: Optional[Metadata] = None

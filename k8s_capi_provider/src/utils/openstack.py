"""Module contains OpenStack client, methods and dependencies."""
from __future__ import annotations

from typing import Dict, NamedTuple

import loguru
from keystoneauth1 import session
from keystoneauth1.identity import v3
from novaclient import client as novaclient

from ..exceptions import ProviderPermanentError, ProviderPermanentErrorReason


class Flavor(NamedTuple):
    """OpenStack flavor."""

    name: str
    ram: int
    vcpus: int


class Quota(NamedTuple):
    """OpenStack quota."""

    floating_ips: int = 0
    instances: int = 0
    ram: int = 0
    vcpus: int = 0
    server_groups: int = 0
    security_groups: int = 0

    def __add__(self, other) -> Quota:
        if not isinstance(other, Quota):
            return NotImplemented

        return Quota(
            floating_ips=self.floating_ips + other.floating_ips,
            instances=self.instances + other.instances,
            ram=self.ram + other.ram,
            vcpus=self.vcpus + other.vcpus,
            server_groups=self.server_groups + other.server_groups,
            security_groups=self.security_groups + other.security_groups,
        )

    def __sub__(self, other: Quota) -> Quota:
        return Quota(
            floating_ips=self.floating_ips - other.floating_ips,
            instances=self.instances - other.instances,
            ram=self.ram - other.ram,
            vcpus=self.vcpus - other.vcpus,
            server_groups=self.server_groups - other.server_groups,
            security_groups=self.security_groups - other.security_groups,
        )

    def __gt__(self, other) -> bool:
        if not isinstance(other, Quota):
            return NotImplemented

        return (
            self.floating_ips > other.floating_ips
            or self.instances > other.instances
            or self.ram > other.ram
            or self.vcpus > other.vcpus
            or self.server_groups > other.server_groups
            or self.security_groups > other.security_groups
        )


class OpenStack(object):
    """OpenStack client and methods."""

    def __init__(self, openrc: dict, logger=None):
        self.openrc = openrc
        self.logger = logger if logger is not None else loguru.logger
        self.nova = self.get_nova(openrc)
        self.flavors = self.get_flavors()

    @staticmethod
    def get_password_auth(openrc: dict) -> v3.Password:
        """Get v3.Password variables from OpenStack RC file.

        Args:
            openrc (dict): OpenStack RC file variables.

        Returns:
            dict: v3.Password variables

        """
        return v3.Password(
            auth_url=openrc["OS_AUTH_URL"],
            username=openrc["OS_USERNAME"],
            password=openrc["OS_PASSWORD"],
            project_name=openrc["OS_PROJECT_NAME"],
            user_domain_name=openrc["OS_USER_DOMAIN_NAME"],
            project_domain_id=openrc["OS_PROJECT_DOMAIN_ID"],
        )

    @staticmethod
    def get_app_cred_auth(openrc: dict) -> v3.ApplicationCredential:
        """Get v3.ApplicationCredential variables from OpenStack RC file.

        Args:
            openrc (dict): OpenStack RC file variables.

        Returns:
            dict: v3.ApplicationCredential variables

        """
        return v3.ApplicationCredential(
            auth_url=openrc["OS_AUTH_URL"],
            application_credential_id=openrc["OS_APPLICATION_CREDENTIAL_ID"],
            application_credential_secret=openrc["OS_APPLICATION_CREDENTIAL_SECRET"],
        )

    def get_nova(self, openrc: dict) -> novaclient.Client:
        """Get nova client.

        Args:
            openrc (dict): OpenStack RC file variables.

        Returns:
            novaclient.Client

        """
        if openrc.get("OS_AUTH_TYPE") == "v3applicationcredential":
            try:
                auth = self.get_app_cred_auth(openrc)
            except KeyError as err:
                raise ProviderPermanentError(
                    reason=ProviderPermanentErrorReason.OPENSTACK_RC_ERROR.name,
                    message="Failed to create application credential"
                    " authentication method."
                    f" Missing open rc variables: {str(err)}",
                )

        else:
            try:
                auth = self.get_password_auth(openrc)
            except KeyError as err:
                raise ProviderPermanentError(
                    reason=ProviderPermanentErrorReason.OPENSTACK_RC_ERROR.name,
                    message="Failed to create password"
                    " authentication method."
                    f" Missing open rc variables: {str(err)}",
                )

        return novaclient.Client("2", session=session.Session(auth=auth))

    def get_keypair(self, name: str):
        """Get keypair from OpenStack.

        Args:
            name (str): Name of the keypair to be returned.

        Returns:
            keypair.

        """
        return self.nova.keypairs.get(name)

    def create_keypair(self, name: str, public_key: str = None):
        """Create keypair in OpenStack.

        Args:
            name (str): Name of the keypair to be created.
            public_key (str, optional): Keypair public key.

        Returns:
            created keypair.

        """
        return self.nova.keypairs.create(name, public_key=public_key)

    def remove_keypair(self, name: str):
        """Remove keypair from OpenStack.

        Args:
            name (str): Name of the keypair to be removed.

        Returns:
            deleted keypair.

        """
        return self.nova.keypairs.delete(name)

    def get_limits(self) -> dict:
        """Get OpenStack resource usage limits.

        Returns:
            dict: limits.

        """
        return {limit.name: limit.value for limit in self.nova.limits.get().absolute}

    def get_available_quotas(self) -> Quota:
        """Get OpenStack quotas based on resource usage limits.

        Returns:
            Quota: available quotas.

        """
        limits = self.get_limits()
        return Quota(
            floating_ips=limits["maxTotalFloatingIps"] - limits["totalFloatingIpsUsed"],
            server_groups=limits["maxServerGroups"] - limits["totalServerGroupsUsed"],
            instances=limits["maxTotalInstances"] - limits["totalInstancesUsed"],
            ram=limits["maxTotalRAMSize"] - limits["totalRAMUsed"],
            vcpus=limits["maxTotalCores"] - limits["totalCoresUsed"],
            security_groups=limits["maxSecurityGroups"]
            - limits["totalSecurityGroupsUsed"],
        )

    def get_flavors(self) -> Dict[str, Flavor]:
        """Get OpenStack flavors.

        Returns:
            dict(str, Flavor): Flavors.

        """
        return {
            flavor.name: Flavor(name=flavor.name, ram=flavor.ram, vcpus=flavor.vcpus)
            for flavor in self.nova.flavors.list()
        }

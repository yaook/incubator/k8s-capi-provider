"""This module defines reconcile function and its dependencies."""
import os
from functools import wraps
from typing import Optional, Union

import git
import kopf
from git import InvalidGitRepositoryError

from ..data.base import ResourceAction
from ..models.yaookcluster import Lcm
from ..settings import ClusterCtrl, ProviderSettings, ServiceCtrl, get_settings
from ..stages.ansible import StageAnsible
from ..stages.config import StageConfig
from ..stages.gpg import StageGPG
from ..stages.ssh import StageSSH
from ..stages.wireguard import StageWireguard
from ..utils.common import exec_cmd
from ..utils.kubernetes import Kubernetes
from ..utils.openstack import OpenStack
from ..utils.state import ProviderState

SETTINGS: ProviderSettings = get_settings()


def set_controller(
    settings_kopf: kopf.OperatorSettings,
    settings_ctrl: Union[ServiceCtrl, ClusterCtrl],
    logger: kopf.Logger,
) -> None:
    """Set controller on startup.

    Args:
        settings_kopf (kopf.OperatorSetting): Kopf specific controller settings.
        settings_ctrl (Union[ServiceCtrl, ClusterCtrl]): Controller settings.
        logger(kopf.Logger): Kopf logger.

    """
    logger.info(f"startup of {settings_ctrl.name} controller started")
    # Set kopf settings
    settings_kopf.persistence.progress_storage = kopf.AnnotationsProgressStorage(
        prefix=settings_ctrl.kopf_prefix
    )
    settings_kopf.persistence.diffbase_storage = kopf.AnnotationsDiffBaseStorage(
        prefix=settings_ctrl.kopf_prefix
    )
    settings_kopf.background.cancellation_polling = settings_ctrl.cancellation_polling
    settings_kopf.watching.client_timeout = settings_ctrl.client_timeout
    settings_kopf.watching.server_timeout = settings_ctrl.server_timeout

    # Set git environment
    yaook_git_dir = SETTINGS.yaook_git.directory
    yaook_git_url = SETTINGS.yaook_git.url
    yaook_git_version = SETTINGS.yaook_git.version

    # Ensure that the current root directory is a valid git repository.
    try:
        git.Repo()
    except InvalidGitRepositoryError:
        repo_root = git.Repo.init()
        repo_root.index.commit("init")

    # Clone and checkout Yaook/k8s repository if it is not already there.
    if not os.path.exists(yaook_git_dir) or (
        os.path.isdir(yaook_git_dir) and not os.listdir(yaook_git_dir)
    ):
        repo_yaook = git.Repo.clone_from(yaook_git_url, yaook_git_dir, no_checkout=True)
        repo_yaook.git.checkout(yaook_git_version)
        logger.info(
            f"Yaook/k8s project version {yaook_git_version}"
            f" successfully cloned from {yaook_git_url}"
        )

    os.environ["MANAGED_K8S_GIT"] = os.getcwd() + "/" + str(yaook_git_dir)
    logger.info(f"startup of {settings_ctrl.name} controller finished")


def load_update_base_state(func):
    """Load and update base state.

    Decorator loads base states from k8s secrets before
    wrapped function execution and updates k8s secrets after
    the wrapped function is executed.
    As a base state is considered state which is produced
    by terraform and ansible scripts.
    Those states should be loaded before each yaook/k8s stage
    and updated after each yaook/k8s stage.

    Args:
        func (callable): Wrapped function.

    Returns:
        callable: Decorator that loads/updates cluster base state.

    """

    @wraps(func)
    async def handle_state(*args, **kwargs):
        cluster_name = kwargs["cluster_name"]
        cluster_namespace = kwargs["cluster_namespace"]
        kubernetes = kwargs["kubernetes"]
        settings_ctrl = kwargs["settings_ctrl"]
        adopt_state = kwargs.get("adopt_state", False)

        cluster_path = settings_ctrl.path / cluster_namespace / cluster_name
        os.makedirs(cluster_path, exist_ok=True)

        for state in (
            SETTINGS.terraform.state,
            SETTINGS.terraform.state_vars,
            SETTINGS.ansible.state,
        ):
            provider_state = ProviderState(
                k8s=kubernetes,
                name="-".join((cluster_name, state.name)),
                namespace=cluster_namespace,
                path=cluster_path / state.path,
                adopt_state=adopt_state,
            )
            provider_state.load()

        try:
            await func(*args, **kwargs)
        finally:
            # Cluster states should be updated even if the yaook/k8s stage
            # failed for any reason. This ensures that the persistent
            # storage (k8s secret) is in sync with the real cluster state.
            # If there is a saved exception it is re-raised at the end of
            # the `finally` clause.
            for state in (
                SETTINGS.terraform.state,
                SETTINGS.terraform.state_vars,
                SETTINGS.ansible.state,
            ):
                provider_state = ProviderState(
                    k8s=kubernetes,
                    name="-".join((cluster_name, state.name)),
                    namespace=cluster_namespace,
                    path=cluster_path / state.path,
                    adopt_state=adopt_state,
                )
                provider_state.update()

    return handle_state


@load_update_base_state
async def execute(
    cmd: list,
    cluster_name: str,
    cluster_namespace: str,
    action: ResourceAction,
    kubernetes: Kubernetes,
    settings_ctrl: Union[ClusterCtrl, ServiceCtrl],
    openstack: Optional[OpenStack] = None,
    lcm: Optional[dict] = None,
    config: Optional[dict] = None,
    adopt_state: Optional[bool] = False,
    managed_provider_identity: Optional[bool] = True,
):
    """Execute provider stages and handle states.

    Args:
        cmd (list): List of commands to be executed.
        cluster_name (str): Cluster name.
        cluster_namespace (str): Cluster namespace name.
        action (ResourceAction): Yaook resource action phase.
        kubernetes (Kubernetes): Kubernetes utils instance.
        settings_ctrl (ClusterCtrl, ServiceCtrl): Controller specific settings.
        openstack (OpenStack, optional): OpenStack utils instance.
        lcm (dict, optional): Cluster lcm.
        config (dict, optional): Resource config.
        adopt_state (bool, optional): True if the state resources should be
            adopted by reconciled resource. Defaults to False.
        managed_provider_identity (bool, optional):  True if the ssh/gpg/wg
            identities will be created by k8s-capi-provider. The provider
            identity should not be created by k8s-capi-provider when
            the cluster was injected (by `yaookctl inject`) from yaook/k8s LCM.
            Then, the origin yaook/k8s ssh/gpg/wg identities (private keys) are
            used as a provider one and should not be created again.
            Defaults to True.
    """
    if config is None:
        config = {}
    extra_env = {}
    cluster_path = settings_ctrl.path / cluster_namespace / cluster_name

    if action == ResourceAction.DELETE:
        # TF_CLI_ARGS_destroy terraform arguments ensures gracefully lock
        # release when the deletion is requested during some action
        # e.g. creation of the cluster.
        extra_env.update(
            {
                "TF_CLI_ARGS_destroy": "-lock-timeout=30s",
            }
        )

    if action != ResourceAction.DELETE:
        # ~~~~~~~ Stage SSH ~~~~~~~
        # Stage is executed only within the cluster controller reconciliation.
        if openstack and lcm:
            with StageSSH(
                cluster_name=cluster_name,
                cluster_namespace=cluster_namespace,
                openstack=openstack,
                kubernetes=kubernetes,
                settings=SETTINGS.ssh,
                cluster_path=cluster_path,
                user_key=lcm.get("TF_VAR_keypair")
                if lcm.get("TF_VAR_keypair") != Lcm.__fields__["TF_VAR_keypair"].default
                else None,
                adopt_state=adopt_state,
            ) as ssh:
                extra_env.update(
                    await ssh.get_environment(
                        managed_provider_identity=managed_provider_identity
                    )
                )

                if not managed_provider_identity:
                    await ssh.check_injected_identity()
        # ~~~~~~~ Stage SSH ~~~~~~~

        # ~~~~~~~ Stage GPG ~~~~~~~
        with StageGPG(
            cluster_name=cluster_name,
            cluster_namespace=cluster_namespace,
            kubernetes=kubernetes,
            settings=SETTINGS.gpg,
            cluster_path=cluster_path,
            adopt_state=adopt_state,
        ) as gpg:
            extra_env.update(gpg.get_environment())

            if managed_provider_identity:
                gpg_provider_key = gpg.get_provider_key()
                gpg.register_additional_gpg(
                    config.setdefault("passwordstore", {}).setdefault(
                        "additional_users", []
                    )
                )
                config["passwordstore"]["additional_users"].append(
                    {
                        "ident": gpg_provider_key.name,
                        "gpg_id": gpg_provider_key.fingerprint,
                    }
                )
            else:
                gpg.check_injected_identity()
        # ~~~~~~~ Stage GPG ~~~~~~~

        # ~~~~~~~ Stage WireGuard ~~~~~~~
        with StageWireguard(
            cluster_name=cluster_name,
            cluster_namespace=cluster_namespace,
            kubernetes=kubernetes,
            settings=SETTINGS.wireguard,
            cluster_path=cluster_path,
            wg_user=lcm.get("wg_user") if lcm else None,
            adopt_state=adopt_state,
        ) as wireguard:
            extra_env.update(
                await wireguard.get_environment(
                    managed_provider_identity=managed_provider_identity,
                )
            )
            if managed_provider_identity:
                config.setdefault("wireguard", {}).setdefault("peers", []).append(
                    {"ident": extra_env["wg_user"], "pub_key": extra_env["wg_public"]}
                )
            else:
                wireguard.check_injected_identity()
        # ~~~~~~~ Stage WireGuard ~~~~~~~

        # ~~~~~~~ Stage Ansible ~~~~~~~
        with StageAnsible(
            settings=SETTINGS.ansible,
            cluster_path=cluster_path,
            provider_ssh_key=SETTINGS.ssh.provider.state.path,
        ) as ansible:
            extra_env.update(ansible.get_environment())
        # ~~~~~~~ Stage Ansible ~~~~~~~

        # ~~~~~~~ Stage Config ~~~~~~~
        with StageConfig(
            cluster_name=cluster_name,
            cluster_namespace=cluster_namespace,
            kubernetes=kubernetes,
            settings=SETTINGS.config_toml,
            settings_git=SETTINGS.yaook_git,
            cluster_path=cluster_path,
            adopt_state=adopt_state,
        ) as config_stage:
            config_stage.merge(config)
        # ~~~~~~~ Stage Config ~~~~~~~

    # ~~~~~~~ Stage Environment ~~~~~~~
    # Merge environments for cmd
    env = {}
    if lcm:
        # Convert lcm values to strings
        env.update(
            {k: str(v).lower() if type(v) is bool else str(v) for k, v in lcm.items()}
        )
    if openstack:
        env.update(openstack.openrc)

    # Extend and possibly overrides the above by `extra_env` variables
    env.update(
        {
            **os.environ,
            **extra_env,
        }
    )
    # ~~~~~~~ Stage Environment ~~~~~~~

    await exec_cmd(
        cmd=cmd,
        env=env,
        cwd=cluster_path,
        cluster_name=cluster_name,
        cluster_namespace=cluster_namespace,
        output=True,
        log2file=settings_ctrl.logging.log2file,
        timeout=3600 * 2,
    )

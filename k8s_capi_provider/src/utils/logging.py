"""Module contains logging definition and its dependencies."""

import sys
from asyncio import StreamReader
from functools import wraps
from pathlib import Path
from typing import Generator, Optional, Tuple

from loguru import logger
from loguru._handler import Handler

from ..exceptions import ProviderPermanentError, ProviderPermanentErrorReason
from ..settings import ProviderSettings, get_settings

logger.remove()  # Remove default loguru logger
SETTINGS: ProviderSettings = get_settings()

# FIXME: Warning messages from underlying yaook/k8s scripts are printed to the
#  STDERR. Therefore, we should redirect these warnings to the warning logs and
#  avoid log them as error in our k8s-capi-provider. We defined the list of
#  known warnings. Currently, it is not possible to better distinguish
#  warning from errors produced by yaook/k8s, as the ansible warnings
#  are often multiline.
#  !Fix all underlying warnings in yaaok/k8s and remove this very ugly workaround!
YAOOK_WARNINGS = [
    "[#]",
    "warning",
    "Cloning into",
    "done.",
    "ANSIBLE_DEBUG=1 to see detailed information",
    "[WARNING]: Using run_once with the free strategy is not currently supported.",
    "This task will still be executed for every host in the inventory list.",
    "[WARNING]: Module remote_tmp /root/.ansible/tmp did not exist and was created",
    "with a mode of 0700, this may cause issues when running as another user. To",
    "avoid this, create the remote_tmp dir with the correct permissions manually",
    "[WARNING]: Skipped '/etc/cni/net.d/' path due to this access issue:",
    "'/etc/cni/net.d/' is not a directory",
    "[WARNING]: Unable to find",
    "/managed-k8s/k8s-service-",
    "/managed-k8s/k8s-managed-",
    "sources/monitoring/",
    "in expected paths",
    "(use -vvvvv to see paths)",
    "paths)",
    "[WARNING]: An unhandled exception occurred while running the lookup plugin",
    "'file'. Error was a <class 'ansible.errors.AnsibleError'>, original message:",
    "could not locate file in lookup:",
    "/jsonnet-sources/monitoring/manifests.files.json",
    "/jsonnet-sources/monitoring/service-layer.files.json",
    "uninitialized_frontend_nodes",
    "node_with_k8s_not_installed",
]


def get_cluster(body: Optional[dict] = None) -> Tuple[str, str]:
    """Get cluster name and namespace from resource body.

    Cluster name is defined in YaookService.spec.clusterName field
    or as a YaookCuster resource name (YaookCuster.metadata.name).
    We assumed that namespace for both resources should be same, hence
    the cluster resource namespcae == service resource namespace.

    Args:
        body (dict, optional): Resource body.

    Returns:
        tuple(str, str): Cluster name, cluster namespace.

    Raises:
        ProviderPermanentError: If cluster name or namespace is not defined.

    """
    if not body:
        raise ProviderPermanentError(
            reason=ProviderPermanentErrorReason.K8S_CAPI_PROVIDER_ERROR.name,
            message="Unable to register cluster loggers."
            " The resource body is not defined.",
        )

    try:
        cluster_namespace = body["metadata"]["namespace"]
    except KeyError:
        raise ProviderPermanentError(
            reason=ProviderPermanentErrorReason.K8S_CAPI_PROVIDER_ERROR.name,
            message="Unable to register cluster loggers."
            " The cluster namespace is not defined.",
        )

    try:
        cluster_name = body["spec"]["clusterName"]
    except KeyError:
        try:
            cluster_name = body["metadata"]["name"]
        except KeyError:
            raise ProviderPermanentError(
                reason=ProviderPermanentErrorReason.K8S_CAPI_PROVIDER_ERROR.name,
                message="Unable to register cluster loggers."
                " The cluster name is not defined.",
            )

    return cluster_name, cluster_namespace


def get_handlers(name: str, namespace: str) -> Generator[Handler, None, None]:
    """Yield loguru handler that match given cluster name and namespace.

    Args:
        name (str): Cluster name.
        namespace (str): Cluster namespace.

    Yields:
        Handler: Loguru handler that match given name and namespace.
    """
    for handler in logger._core.handlers.values():  # type: ignore[attr-defined]
        if (
            hasattr(handler._filter, "name")
            and hasattr(handler._filter, "namespace")
            and handler._filter.name == name
            and handler._filter.namespace == namespace
        ):
            yield handler


class YaookFilter(object):
    """Filter log messages based on cluster name and namespace."""

    def __init__(self, name: str, namespace: str, level: str = "INFO"):
        self._name = name
        self._namespace = namespace
        self._level = level

    @property
    def name(self) -> str:
        """Return Cluster name.

        Returns:
            str: Cluster name.

        """
        return self._name

    @property
    def namespace(self) -> str:
        """Return Cluster namespace.

        Returns:
            str: Cluster namespace.

        """
        return self._namespace

    def __call__(self, record: dict) -> bool:
        """Filter log messages based on cluster name and namespace.

        Args:
            record (dict): Message to be filtered.

        Returns:
            bool: True if cluster name and namespace are defined in
              message extra args., False otherwise.

        """
        return (
            record["level"].name == self._level
            and self._namespace == record["extra"].get("namespace")
            and self._name == record["extra"].get("cluster")
        )


def retention(path: Path, backup_count: int):
    """Create a callable that rotate log files.

    Args:
        path (Path): Path of rotated log file.
        backup_count (int): Number of backups.

    Returns:
        callable: Callable that rotate log files.
    """

    def rotate(logs: list):
        new_log = sorted(logs).pop(0)

        for i in range(backup_count, 0, -1):
            path_new = path.with_suffix(path.suffix + f".{i}")

            path_old = path.with_suffix(path.suffix + f".{i - 1}")
            if path_old.is_file():
                path_old.replace(path_new)

        Path(new_log).replace(path.with_suffix(path.suffix + ".1"))

    return rotate


def register(settings=SETTINGS.cluster_ctrl):
    """Create a callable that register cluster file loggers.

    Returns:
        callable: Decorator that register cluster file loggers.

    """

    def decorator(func):
        @wraps(func)
        async def set_logging(*args, **kwargs):
            if not settings.logging.log2file:
                return

            cluster_name, cluster_namespace = get_cluster(kwargs.get("body"))

            log_backup_count = settings.logging.backup_count
            log_rotation = settings.logging.rotation
            cluster_path = settings.path / cluster_namespace / cluster_name
            logfile_info = cluster_path / settings.logging.path_info
            logfile_warning = cluster_path / settings.logging.path_warning
            logfile_error = cluster_path / settings.logging.path_error

            handlers = list(get_handlers(cluster_name, cluster_namespace))

            if (
                not logfile_info.exists()
                or not logfile_warning.exists()
                or not logfile_error.exists()
                or not len(handlers) == 4
            ):
                # Ensure that there are not any cluster handler
                for handler in handlers:
                    logger.remove(handler._id)

                # Add cluster handlers
                # INFO logs to the info.log file
                logger.add(
                    logfile_info,
                    format="[{time:YYYY-MM-DD HH:mm:ss,SSS}] {message}",
                    filter=YaookFilter(cluster_name, cluster_namespace, level="INFO"),
                    retention=retention(logfile_info, backup_count=log_backup_count),
                    rotation=log_rotation,
                    enqueue=True,
                )
                # WARNING logs to the warning.log file
                logger.add(
                    logfile_warning,
                    format="[{time:YYYY-MM-DD HH:mm:ss,SSS}] {message}",
                    filter=YaookFilter(
                        cluster_name, cluster_namespace, level="WARNING"
                    ),
                    retention=retention(logfile_warning, backup_count=log_backup_count),
                    rotation=log_rotation,
                    enqueue=True,
                )
                # ERROR logs to the error.log file
                logger.add(
                    logfile_error,
                    format="[{time:YYYY-MM-DD HH:mm:ss,SSS}] {message}",
                    filter=YaookFilter(cluster_name, cluster_namespace, level="ERROR"),
                    retention=retention(logfile_error, backup_count=log_backup_count),
                    rotation=log_rotation,
                    enqueue=True,
                )
                # ERROR logs to the stderr
                logger.add(
                    sys.stderr,
                    # Log format inspired by KOPF
                    format="[{time:YYYY-MM-DD HH:mm:ss,SSS}] [{level}]"
                    " [{extra[namespace]}/{extra[cluster]}] {message}",
                    filter=YaookFilter(cluster_name, cluster_namespace, level="ERROR"),
                    enqueue=True,
                )

            return await func(*args, **kwargs)

        return set_logging

    return decorator


def unregister(settings=SETTINGS.cluster_ctrl):
    """Create a callable that unregister cluster file loggers.

    Returns:
        callable: Decorator that unregister cluster file loggers.

    """

    def decorator(func):
        @wraps(func)
        async def unset_logging(*args, **kwargs):

            retval = await func(*args, **kwargs)

            if settings.logging.log2file:
                cluster_name, cluster_namespace = get_cluster(kwargs.get("body"))

                for handler in get_handlers(cluster_name, cluster_namespace):
                    logger.remove(handler._id)

            return retval

        return unset_logging

    return decorator


async def write_stream(
    stream: Optional[StreamReader] = None,
    cluster_name: Optional[str] = None,
    cluster_namespace: Optional[str] = None,
    is_stderr: bool = False,
    is_buffered: bool = False,
) -> Optional[bytearray]:
    """Read given stream and write it to the log file.

    Args:
        stream (StreamReader, optional): Stream to be read and log to file.
        cluster_name (str, optional): Cluster name.
        cluster_namespace (str, optional): Cluster namespace.
        is_stderr (bool, optional): If given stream should be logged as error.
            Defaults to False.
        is_buffered (bool, optional): If buffering to array is desired.
            Defaults to False.

    Returns:
        bytearray: Bytearray with requested stream if desired.

    """
    try:
        assert stream
        assert cluster_name
        assert cluster_namespace
    except AssertionError:
        # see https://github.com/python/mypy/issues/7511
        return None

    is_warning = False
    buffer = bytearray()
    cluster_logger = logger.bind(namespace=cluster_namespace, cluster=cluster_name)

    while line := await stream.readline():
        line_decoded = line.rstrip().decode()

        if is_stderr:
            if is_warning := any(
                warning.lower() in line_decoded.lower() for warning in YAOOK_WARNINGS
            ):
                cluster_logger.warning(f"{line_decoded}")
            else:
                cluster_logger.error(f"{line_decoded}")
        else:
            cluster_logger.info(f"{line_decoded}")
        if is_buffered and not is_warning:  # Skip buffering of warnings
            buffer.extend(line)

    await cluster_logger.complete()

    return buffer

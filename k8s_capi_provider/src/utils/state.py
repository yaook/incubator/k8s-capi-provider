"""Module contains state functions and dependencies."""

import gzip
import io
import os
import shutil
import tarfile
from pathlib import Path

from kubernetes.client import V1ObjectMeta, V1Secret

from .common import encode_to_64
from .kubernetes import Kubernetes


def uncompress_state(state_path: Path, state_key: str, state_value: bytes):
    """Uncompress state from gzip compressed bytes.

    Args:
        state_path (Path): Destination directory
        state_key (str): Filename to be gzipped
        state_value (bytes): Gzip compressed bytes
    """
    if state_key.endswith(".tar.gz"):
        with tarfile.open(fileobj=io.BytesIO(state_value)) as tar:
            tar.extractall(path=os.path.dirname(state_path))

    elif state_key.endswith(".gz"):
        os.makedirs(os.path.dirname(state_path), exist_ok=True)
        with open(state_path, "wb") as file:
            file.write(gzip.decompress(state_value))


def compress_state(state_path: Path) -> str:
    """Compress an existing state.

    Creates .tar.gz archive from an existing directory or
    .gz archive from file.

    Args:
        state_path (Path): Path to the state that should be compressed.

    Returns:
        str: Compressed state key name

    """
    compress_path = state_path

    if state_path.is_file():
        compress_path = state_path.with_name(state_path.name + ".gz")

        with open(state_path, "rb") as file:
            with gzip.open(compress_path, "wb") as gzipfile:
                shutil.copyfileobj(file, gzipfile)

    elif state_path.is_dir():
        compress_path = state_path.with_name(state_path.name + ".tar.gz")

        with tarfile.open(compress_path, "w:gz") as tar:
            tar.add(str(state_path), arcname=state_path.name)

    return compress_path.name


class ProviderState(object):
    """Manage Provider state in Kubernetes.

    Provider uses Kubernetes secret as a persistent storage for states
    produced by cluster LCM. State could be loaded from k8s secret to the
    POD's local storage as well as workload cluster state located in POD's
    local storage could be saved as a k8s secret.

    """

    def __init__(
        self, name: str, namespace: str, path: Path, k8s: Kubernetes, adopt_state=False
    ):
        self.name = name
        self.namespace = namespace
        self.path = path
        self.k8s = k8s
        self.adopt_state = adopt_state

    def load(self):
        """Load state from secret."""
        k8s = self.k8s
        name = self.name
        path = self.path
        namespace = self.namespace

        state_key, state_value = None, None
        if state_secret := k8s.get_secret(name, namespace, ignore_errors=True):
            state_key, state_value = next(k8s.get_secret_data(state_secret))

        if state_key and state_key.endswith((".tar.gz", ".gz")):
            uncompress_state(path, state_key, state_value)

        return self

    def update(self):
        """Update state secret."""
        k8s = self.k8s
        name = self.name
        path = self.path
        namespace = self.namespace
        adopt_state = self.adopt_state

        compress_state_key = compress_state(path)
        try:
            with open(os.path.dirname(path) + "/" + compress_state_key, "rb") as file:
                encoded_string = encode_to_64(file.read())
        except FileNotFoundError:
            # Yaook stage does not create state e.g. stage `actions/init.sh`
            return

        secret = V1Secret(
            type="Opaque",
            metadata=V1ObjectMeta(name=name),
            data={compress_state_key: encoded_string},
        )
        k8s.apply_secret(secret, namespace, adopt=adopt_state)

        return self

    def remove(self):
        """Remove states: secret & local."""
        k8s = self.k8s
        name = self.name
        namespace = self.namespace
        path = self.path

        k8s.remove_secret(name, namespace, ignore_errors=True)
        shutil.rmtree(path, ignore_errors=True)

"""Module contains networking definition and its dependencies."""

import random
from ipaddress import IPv4Network
from typing import Optional

import netifaces

from ..exceptions import ProviderPermanentError, ProviderPermanentErrorReason
from ..utils.common import exec_cmd

SUBNET_MASK = 30
PRIVATE_NETWORKS = [
    IPv4Network("10.0.0.0/8"),
    IPv4Network("172.16.0.0/12"),
    IPv4Network("192.168.0.0/16"),
]


def get_all_local_networks():
    """Get all local networks except for localhost.

    Returns:
        List of networks
    """
    local_networks = []
    for iface in netifaces.interfaces():
        if iface != "lo":
            for address in netifaces.ifaddresses(iface).get(netifaces.AF_INET, []):
                local_networks.append(
                    IPv4Network((address["addr"], address["netmask"]), strict=False)
                )
    return local_networks


def exclude_cidr_from_networks(exclude_cidr, networks):
    """Exclude one cidr from networks iterable.

    Args:
        exclude_cidr: Network to exclude from 'networks'
        networks: Networks iterable

    Returns:
        List of networks with excluded 'exclude_cidr'
    """
    subnets = []
    for network in networks:
        try:
            subnets += list(network.address_exclude(exclude_cidr))
        except ValueError:
            if not exclude_cidr.supernet_of(network):
                subnets.append(network)
    return subnets


def random_subnet(network, subnet_mask):
    """Generate random subnetwork with 'subnet_mask' from given 'network'.

    Args:
        network: Network from random subnetwork will be generated
        subnet_mask: Netmask of desired subnetwork

    Returns:
        Random IPv4 subnetwork with 'subnet_mask' generated from 'network'
    """
    rand_bits = network.max_prefixlen - network.prefixlen
    rand_ip_address = network.network_address + random.randint(0, 2**rand_bits - 1)
    return IPv4Network((rand_ip_address, subnet_mask), strict=False)


async def create_network_namespace(
    name: str, not_usable_subnets: Optional[list] = None
):
    """Create network namespace and related iptables rule.

    Args:
        name (str): Name of network namespace and iptables rule to create
        not_usable_subnets (list, optional): Subnets which shouldn't be used

    Raises:
        ProviderPermanentError
    """
    if not_usable_subnets is None:
        not_usable_subnets = []
    usable_subnets = PRIVATE_NETWORKS.copy()
    for not_usable_subnet in not_usable_subnets:
        usable_subnets = exclude_cidr_from_networks(not_usable_subnet, usable_subnets)
    subnet = random_subnet(
        random.choice(
            [
                usable_subnet
                for usable_subnet in usable_subnets
                if usable_subnet.prefixlen <= SUBNET_MASK
            ]
        ),
        SUBNET_MASK,
    )
    ip = subnet.network_address
    cmds = (
        f"ip netns add {name}",
        f"ip link add {ip} type veth peer name veth-ns netns {name}",
        f"ip addr add {ip + 1}/{SUBNET_MASK} dev {ip}",
        f"ip link set {ip} up",
        f"ip -n {name} addr add {ip + 2}/{SUBNET_MASK} dev veth-ns",
        f"ip -n {name} link set veth-ns up",
        f"ip -n {name} link set lo up",
        f"ip -n {name} route add default via {ip + 1}",
        f"iptables -t nat -A POSTROUTING -s {subnet} -j MASQUERADE "
        f"-m comment --comment {name}",
    )
    for cmd in cmds:
        try:
            await exec_cmd(cmd.split())
        except ProviderPermanentError as e:
            e.reason = ProviderPermanentErrorReason.NETWORK_NAMESPACE_ERROR.name
            raise


async def delete_network_namespace(name: str):
    """Delete network namespace and related iptables rule.

    Args:
        name (str): Name of network namespace and iptables rule to delete

    Raises:
        ProviderPermanentError
    """
    try:
        rules, _ = await exec_cmd(
            ["iptables", "-t", "nat", "-L", "POSTROUTING", "--line-number"],
            output=True,
        )
        cmds = (f"ip netns delete {name}",) + tuple(
            "iptables -t nat -D POSTROUTING " + rule.split()[0]
            for rule in [
                match
                for match in rules.decode().splitlines()
                if f"/* {name} */" in match
            ]
        )
        for cmd in cmds:
            await exec_cmd(cmd.split())
    except ProviderPermanentError as e:
        e.reason = ProviderPermanentErrorReason.NETWORK_NAMESPACE_ERROR.name
        raise

"""Module contains some common logic used in provider."""

import asyncio
import base64
import json
import os
from functools import wraps
from typing import Any, List, Optional, Tuple, Union

import kopf
from async_timeout import timeout as atimeout
from kubernetes.client.rest import ApiException

from ..data.base import ResourcePhase
from ..exceptions import (
    ProviderPermanentError,
    ProviderPermanentErrorReason,
    ProviderTemporaryError,
    ProviderTemporaryErrorReason,
    get_yaook_reason,
    parse_yaook_error,
)
from ..utils.logging import write_stream


def encode_to_64(bytes_in: bytes) -> str:
    """Compute the base 64 encoding of a bytes.

    Args:
        bytes_in (bytes): The bytes to encode.

    Returns:
        str: The result of the encoding.

    """
    return base64.b64encode(bytes_in).decode()


async def get_subprocess(
    cmd: list,
    cwd: Union[str, bytes, "os.PathLike[str]", "os.PathLike[bytes]", None] = None,
    env: Any = None,
    cmd_input: Optional[bytes] = None,
    output: bool = False,
) -> asyncio.subprocess.Process:
    """Create subprocess.

    Args:
        cmd (list): List of commands to be executed.
        cwd (str, pathlib.Path, optional): Current working directory.
        env (Any, optional): Extra env variables to be set for cmd.
        cmd_input (bytes, optional): Stdin for command.
        output (bool, optional): If output of command is desired. Defaults to False.

    Returns:
        asyncio.subprocess.Process: Created subprocess.

    Raises:
        ProviderPermanentError: If given command is not valid.

    """
    try:
        return await asyncio.create_subprocess_exec(
            *cmd,
            cwd=cwd,
            env=env,
            stderr=asyncio.subprocess.PIPE,
            stdout=asyncio.subprocess.PIPE if output else None,
            stdin=asyncio.subprocess.PIPE if cmd_input else None,
        )
    except OSError as e:
        message = f"{cmd} {e.strerror}"
        reason = get_yaook_reason(message)
        raise ProviderPermanentError(reason=reason.name, message=message)


async def get_communicate(
    process: asyncio.subprocess.Process, cmd_input: Optional[bytes] = None
) -> Tuple[bytes, bytes]:
    """Communicate given process.

    Interact with process as follows:
        1. Send data to stdin (if input is not None)
        2. Read data from stdout and stderr, until EOF is reached
        3. Execute process and wait for termination

    Args:
        process (asyncio.subprocess.Process): Subprocess to be communicated.
        cmd_input(bytes, optional): Stdin for command.

    Returns:
        tuple(bytes, bytes): stdout, stderr
    """
    return await process.communicate(input=cmd_input)


async def get_log2file(
    process: asyncio.subprocess.Process,
    cluster_name: Optional[str] = None,
    cluster_namespace: Optional[str] = None,
) -> List[Any]:
    """Get stream tasks.

    Gather and execute given process with tasks which write
    stdout and stderr to the log files.
    Tasks do the following:
        1. Read data from stdout and stderr and stream them
            to the log files, until EOF is reached
        2. Execute process and wait for termination

    Args:
        process (asyncio.subprocess.Process): Subprocess to be gathered.
        cluster_name (str, optional): Cluster name.
        cluster_namespace (str, optional): Cluster namespace.

    Returns:
        tuple(bytes, bytes, int): stdout, stderr, process return code
    """
    tasks = [
        asyncio.create_task(
            write_stream(process.stdout, cluster_name, cluster_namespace)
        ),
        asyncio.create_task(
            write_stream(
                process.stderr,
                cluster_name,
                cluster_namespace,
                is_stderr=True,
                is_buffered=True,
            )
        ),
        process.wait(),
    ]
    return await asyncio.gather(*tasks)


async def exec_cmd(
    cmd: list,
    env: Any = None,
    cwd: Union[str, bytes, "os.PathLike[str]", "os.PathLike[bytes]", None] = None,
    cmd_input: Optional[bytes] = None,
    cluster_name: Optional[str] = None,
    cluster_namespace: Optional[str] = None,
    timeout: Union[float, int, None] = None,
    output: bool = False,
    log2file: bool = False,
) -> Tuple[bytes, bytes]:
    """Execute command.

    Stdout, and stderr outputs are buffered in memory, so we skipped
    buffering of command's stdout by default, because of a large data size expectation.

    Command's stdout could be printed to the operator's stdout (default behaviour) or
    to the log files if `log2file` option is enabled.

    Args:
        cmd (list): List of commands to be executed.
        env (optional): Extra env variables to be set for cmd.
        cwd (str, pathlib.Path, optional): Current working directory.
        cmd_input(bytes, optional): Stdin for command.
        cluster_name (str, optional): Cluster name.
        cluster_namespace (str, optional): Cluster namespace.
        timeout (float, int, optional): Timeout period for the command to complete.
        output (bool, optional): If output of command is desired. Defaults to False.
        log2file (bool, optional): If log to file is desired. Defaults to False.

    Returns:
        tuple(bytes, bytes): Command stdout if output True else None, Command stderr.

    Raises:
        asyncio.exceptions.CancelledError: If task has been cancelled.
        ProviderTemporaryError: If timeout period expired.
        ProviderPermanentError: If command returns non-zero code or
            dependent variables are not set.

    """
    process = await get_subprocess(cmd, cwd, env, cmd_input, output)
    if log2file:
        try:
            assert cluster_name
            assert cluster_namespace
        except AssertionError:
            raise ProviderPermanentError(
                reason=ProviderPermanentErrorReason.K8S_CAPI_PROVIDER_ERROR.name,
                message="Unable to stream cluster logs to logfiles."
                " The cluster name or cluster namespace is not defined.",
            )

    try:
        async with atimeout(timeout):
            if log2file:
                stdout, stderr, *_ = await get_log2file(
                    process,
                    cluster_name=cluster_name,
                    cluster_namespace=cluster_namespace,
                )
            else:
                stdout, stderr = await get_communicate(process, cmd_input=cmd_input)

    except asyncio.exceptions.CancelledError:
        # Task has been cancelled, terminate the process.
        try:
            process.terminate()
        except OSError:
            # Ignore 'no such process' error
            pass

        raise

    except asyncio.exceptions.TimeoutError:
        # Task timeout, kill the process.
        try:
            process.kill()
        except OSError:
            # Ignore 'no such process' error
            pass

        raise ProviderTemporaryError(
            reason=ProviderTemporaryErrorReason.TIMEOUT_ERROR.name,
            message=f"{' '.join(cmd)} timeout",
        )

    if process.returncode != 0:
        message = parse_yaook_error(stderr)
        reason = get_yaook_reason(message)

        if isinstance(reason, ProviderTemporaryErrorReason):
            raise ProviderTemporaryError(reason=reason.name, message=message)

        raise ProviderPermanentError(reason=reason.name, message=message)

    return stdout, stderr


def error_status(resource_status=None):
    """Set resource status in case of handler exceptions.

    Decorator catching every exception.

    Exceptions that are not instances of :class:`ProviderTemporaryError` are
    written to the resource status fields and then re-raised.

    Exceptions that are instances of :class:`ProviderTemporaryError` are
    re-raised without status change. These exceptions postpone the
    current handler for the next iteration, which happens after a defined delay.

    """

    def decorator(func):
        @wraps(func)
        async def set_status(*args, **kwargs):
            try:
                return await func(*args, **kwargs)
            except Exception as err:
                if isinstance(err, ProviderTemporaryError):
                    raise

                reason = (
                    err.reason
                    if isinstance(err, ProviderPermanentError)
                    else err.__class__.__name__
                )
                message = (
                    err.message if isinstance(err, ProviderPermanentError) else str(err)
                )

                if resource_status:
                    kwargs["patch"]["status"] = resource_status(
                        phase=ResourcePhase.FAILED,
                        failureReason=reason,
                        failureMessage=message,
                    ).dict(exclude_none=True)

                raise

        return set_status

    return decorator


def raise_not_handled_resource(
    plural: str, name: str, delay: Optional[float] = 2
) -> None:
    """Raise temporary error that resource has not yet been handled.

    Args:
        plural (str): Resource plural.
        name (str): Resource name.
        delay (float, optional): Delay for kopf temporary error.

    Raises:
        ProviderTemporaryError

    """
    raise ProviderTemporaryError(
        reason=ProviderTemporaryErrorReason.WAITING_FOR_RESOURCE.name,
        message=f"The {plural}: {name} has not yet been handled.",
        delay=delay,
    )


def wait_for_dependencies(delay=5, resource_status=None):
    """Wait for k8s resource dependencies decorator factory.

    Decorator catches :class:`ApiException`. If the reason of
    the exception is Not Found (404) then the non-blocking
    :class:`TemporaryError` is raised.
    It will postpone the handler for the next iteration,
    which happens after defined delay.

    It waits for every k8s resource infinitely.

    """

    def decorator(func):
        @wraps(func)
        async def wait_for(*args, **kwargs):
            try:
                return await func(*args, **kwargs)
            except ApiException as e:
                if e.status == 404:
                    body = json.loads(e.body)
                    name, kind = body["details"]["name"], body["details"]["kind"]

                    if resource_status:
                        kwargs["patch"]["status"] = resource_status(
                            phase=ResourcePhase.PENDING,
                            ready=False,
                        ).dict(exclude_none=True)

                    raise ProviderTemporaryError(
                        reason=ProviderTemporaryErrorReason.WAITING_FOR_RESOURCE.name,
                        message=f"The {kind}: {name} is not ready yet.",
                        delay=delay,
                    )
                raise

        return wait_for

    return decorator


async def cancel_tasks(prefix: str, logger: kopf.Logger = None) -> None:
    """Cancel tasks based on given prefix.

    Args:
        prefix (str): Prefix of the tasks which will be cancelled.
        logger (kopf.Logger, optional): Kopf logger.

    """
    tasks = [
        task
        for task in asyncio.all_tasks()
        if task.get_name() and task.get_name().startswith(prefix)
    ]
    for task in tasks:
        task.cancel()
        try:
            await task
        except asyncio.exceptions.CancelledError:
            if logger:
                logger.warning(f"{task.get_name()} task has been cancelled.")

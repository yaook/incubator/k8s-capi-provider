"""Module contains Kubernetes client, methods and dependencies."""

import base64
from typing import Generator

import kopf
import loguru
from kubernetes import client, config
from kubernetes.client import CoreV1Api, CustomObjectsApi, V1Secret
from kubernetes.client.rest import ApiException

from ..settings import CRD, ProviderSettings, get_settings

SETTINGS: ProviderSettings = get_settings()
CRDS: CRD = SETTINGS.crd


class Kubernetes(object):
    """Kubernetes client and methods."""

    def __init__(self, logger=None, config_file=None):
        self.logger = logger if logger is not None else loguru.logger

        if config_file is not None:
            config.load_kube_config(config_file=str(config_file))

        self.v1_api = self.get_v1_api()
        self.custom_api = self.get_custom_api()

    @staticmethod
    def get_v1_api() -> CoreV1Api:
        """Get V1 api object."""
        return client.CoreV1Api()

    @staticmethod
    def get_custom_api() -> CustomObjectsApi:
        """Get custom api object."""
        return client.CustomObjectsApi()

    @staticmethod
    def get_secret_data(secret: V1Secret) -> Generator[tuple, None, None]:
        """Get data from secret.

        Args:
            secret (V1Secret):

        Yields:
            tuple(str, str): Key and corresponding decoded value from secret.

        """
        for key, value in secret.data.items():
            yield key, base64.b64decode(value)

    def get_secret(
        self, name: str, namespace: str, ignore_errors: bool = False
    ) -> V1Secret:
        """Read kubernetes secret resource data.

        Args:
            name (str): Secret name to be read.
            namespace (str): Secret namespace.
            ignore_errors (bool, optional): Ignore any error occur,
                just log debug message. Defaults to False.

        Returns:
            V1Secret: Kubernetes secret.

        """
        try:
            return self.v1_api.read_namespaced_secret(name, namespace)
        except ApiException as e:
            if not ignore_errors:
                raise

            self.logger.debug(f"Secret {name} cannot be read: {e.reason}")

    def remove_secret(
        self, name: str, namespace: str, ignore_errors: bool = False
    ) -> V1Secret:
        """Remove kubernetes secret from cluster.

        Args:
            name (str): Secret name to be read.
            namespace (str): Secret namespace.
            ignore_errors (bool, optional): Ignore any error occur,
                just log debug message. Defaults to False.

        Returns:
            V1Secret: Kubernetes secret.

        """
        try:
            return self.v1_api.delete_namespaced_secret(name, namespace)
        except ApiException as e:
            if not ignore_errors:
                raise

            self.logger.debug(f"Secret {name} cannot be removed: {e.reason}")

    def apply_secret(
        self,
        secret: V1Secret,
        namespace: str,
        adopt: bool = False,
        ignore_errors: bool = False,
    ) -> V1Secret:
        """Apply the secret resource.

        Args:
            secret (V1Secret): Secret to create.
            namespace (str): Secret namespace.
            adopt (bool, optional): If True, the owner references is added.
                Defaults to False.
            ignore_errors (bool, optional): Ignore any error occur,
                just log debug message. Defaults to False.

        Returns:
            V1Secret: Kubernetes secret.

        """
        if adopt:
            kopf.adopt(secret)

        try:
            return self.v1_api.create_namespaced_secret(
                namespace=namespace, body=secret
            )
        except ApiException as e:
            self.logger.debug(
                f"Secret {secret.metadata.name} cannot be created because: {e.reason}. "
                "Trying to patch it ..."
            )
            try:
                return self.v1_api.patch_namespaced_secret(
                    name=secret.metadata.name, namespace=namespace, body=secret
                )
            except ApiException as e:
                if not ignore_errors:
                    raise

                self.logger.debug(
                    f"Secret {secret.metadata.name} cannot be"
                    f" patched because: {e.reason}"
                )

    def list_cr(
        self,
        plural: str,
        namespace: str,
        group: str = CRDS.group,
        version: str = CRDS.version,
        ignore_errors: bool = False,
    ) -> dict:
        """List namespaced custom resources.

        Args:
            plural (str): Custom resource plural
            namespace (str): Custom resource namespace
            group (str, optional): Custom resource group.
                Defaults to CRDS.group
            version (str, optional): Custom resource group version.
                Defaults to CRDS.version
            ignore_errors (bool, optional): Ignore any error occur,
                just log debug message. Defaults to False

        Returns:
            dict: Object that contains Kubernetes CR items.

        """
        try:
            return self.custom_api.list_namespaced_custom_object(
                group=group,
                version=version,
                namespace=namespace,
                plural=plural,
            )
        except ApiException as e:
            if not ignore_errors:
                raise

            self.logger.debug(
                f"Custom resources {plural} cannot be listed because: {e.reason}."
            )

    def get_cr(
        self,
        name: str,
        plural: str,
        namespace: str,
        group: str = CRDS.group,
        version: str = CRDS.version,
        ignore_errors: bool = False,
    ) -> dict:
        """Read namespaced custom resources.

        Args:
            name (str): Custom resource name.
            plural (str): Custom resource plural.
            namespace (str): Custom resource namespace.
            group (str, optional): Custom resource group.
                Defaults to CRDS.group.
            version (str, optional): Custom resource group version.
                Defaults to CRDS.version.
            ignore_errors (bool, optional): Ignore any error occur,
                just log debug message. Defaults to False.

        Returns:
            dict: Kubernetes CR.

        """
        try:
            return self.custom_api.get_namespaced_custom_object(
                group=group,
                version=version,
                namespace=namespace,
                plural=plural,
                name=name,
            )
        except ApiException as e:
            if not ignore_errors:
                raise

            self.logger.debug(
                f"Custom resource {name} cannot be read because: {e.reason}."
            )

    def patch_cr(
        self,
        name: str,
        body: dict,
        plural: str,
        namespace: str,
        group: str = CRDS.group,
        version: str = CRDS.version,
        status: bool = False,
        ignore_errors: bool = False,
        owner: bool = False,
        owner_body: dict = None,
        owner_controller: bool = True,
    ) -> dict:
        """Patch namespaced custom resources.

        Args:
            name (str): Custom resource name.
            body (V1CustomResourceDefinition): Custom resource body.
            plural (str): Custom resource plural.
            namespace (str): Custom resource namespace.
            group (str, optional): Custom resource group.
                Defaults to CRDS.group.
            version (str, optional): Custom resource group version.
                Defaults to CRDS.version.
            status (bool, optional): If True, patch only the status subresource.
                Defaults to False.
            ignore_errors (bool, optional): Ignore any error occur,
                just log debug message. Defaults to False.
            owner (bool, optional): If True, "only" the owner references is
                added (not adopted). Defaults to False.
            owner_body (dict, optional): Custom owner should be explicitly
                defined, otherwise the default owner is added by kopf.
                Kopf by default uses handled resource as an owner.
            owner_controller (bool, optional): If True, the owner is also the
                controller of the object. False otherwise.

        Returns:
            dict: Kubernetes CR.

        """
        if owner:
            kopf.append_owner_reference(
                body, owner=owner_body, controller=owner_controller
            )

        patch_func = (
            self.custom_api.patch_namespaced_custom_object_status
            if status
            else self.custom_api.patch_namespaced_custom_object
        )

        try:
            return patch_func(
                group=group,
                version=version,
                namespace=namespace,
                plural=plural,
                name=name,
                body=body,
            )
        except ApiException as e:
            if not ignore_errors:
                raise

            self.logger.debug(
                f"Custom resource {name} cannot be patched because: {e.reason}."
            )

    def delete_cr(
        self,
        name: str,
        plural: str,
        namespace: str,
        group: str = CRDS.group,
        version: str = CRDS.version,
        ignore_errors: bool = False,
    ) -> dict:
        """Delete namespaced custom resources.

        Args:
            name (str): Custom resource name.
            plural (str): Custom resource plural.
            namespace (str): Custom resource namespace.
            group (str, optional): Custom resource group.
                Defaults to CRDS.group.
            version (str, optional): Custom resource group version.
                Defaults to CRDS.version.
            ignore_errors (bool, optional): Ignore any error occur,
                just log debug message. Defaults to False.

        Returns:
            dict: Kubernetes CR.

        """
        try:
            return self.custom_api.delete_namespaced_custom_object(
                group=group,
                version=version,
                namespace=namespace,
                plural=plural,
                name=name,
            )
        except ApiException as e:
            if not ignore_errors:
                raise

            self.logger.debug(
                f"Custom resource {name} cannot be deleted because: {e.reason}."
            )

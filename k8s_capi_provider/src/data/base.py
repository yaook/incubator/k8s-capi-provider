"""Module contains base resource data."""

import json
from enum import Enum
from typing import Union

import kopf
from pydantic import ValidationError

from ..exceptions import ProviderPermanentError, ProviderPermanentErrorReason
from ..models.yaookcluster import YaookCluster
from ..models.yaookconfigtemplate import YaookConfigTemplate
from ..models.yaookcontrolplane import YaookControlPlane
from ..models.yaookmachinedeployment import YaookMachineDeployment
from ..models.yaookmachinetemplate import YaookMachineTemplate
from ..models.yaookservice import YaookService
from ..settings import CRD, ProviderSettings, get_settings
from ..utils.kubernetes import Kubernetes

SETTINGS: ProviderSettings = get_settings()
CRDS: CRD = SETTINGS.crd


class ResourceAction(str, Enum):
    """Constants for resource actions.

    It extends :class:`kopf.Reason` base constants.
    """

    CREATE = "create"
    UPDATE = "update"
    DELETE = "delete"
    RESUME = "resume"
    RECONCILE = "reconcile"

    def __str__(self) -> str:
        """Action's value."""
        return str(self.value)


class ResourcePhase(str, Enum):
    """Constants for resource phases.

    Resource state is clearly described by one
    of the following phases. Phases and their transitions
    are managed by the Yaook CAPI controllers.

    """

    PENDING = "pending"  # Init resource phase. Default value in CRDs
    CREATING = "creating"  # Creating in progress
    RUNNING = "running"  # Cluster is ready
    TERMINATING = "terminating"  # Termination in progress
    UPDATING = "updating"  # Update in progress
    RESUMING = "resuming"  # Resume in progress
    RECONCILING = "reconciling"  # Reconcile loop in progress
    FAILED = "failed"  # Failed

    def __str__(self) -> str:
        """Phase's value."""
        return str(self.value)


class YaookResourceBase(object):
    """Yaook resource base class."""

    RESOURCE_MODEL: Union[
        YaookService,
        YaookCluster,
        YaookConfigTemplate,
        YaookMachineTemplate,
        YaookMachineDeployment,
        YaookControlPlane,
    ]
    RESOURCE_PLURAL: str

    def __init__(self, body: dict, kubernetes: Kubernetes):
        self.body = body
        self.kubernetes = kubernetes
        self.obj = self.get_obj(self.body)
        self.deps = None

    @classmethod
    def from_kopf(cls, body: kopf.Body, kubernetes: Kubernetes):
        """Create resource instance based on its body.

        Method loads resource instance from its body provided
        by kopf.
        Fields:
          - `metadata.resourceVersion`
          - `metadata.creationTimestamp`
        may cause conflict issue when resource is patched
        by the whole body, hence these fields are removed.

        Args:
            body (kopf.Body): Resource body.
            kubernetes (Kubernetes): Kubernetes utils instance.

        Returns:
            YaookResource instance.

        """
        body.get("metadata", {}).pop("resourceVersion", None)
        body.get("metadata", {}).pop("creationTimestamp", None)

        return cls({**body}, kubernetes)

    @classmethod
    def from_k8s(
        cls,
        name: str,
        namespace: str,
        kubernetes: Kubernetes,
    ):
        """Create resource instance based on its name and namespace.

        Method loads resource instance from k8s cluster based on its
        name and namespace.
        Fields:
          - `metadata.resourceVersion`
          - `metadata.creationTimestamp`
        may cause conflict issue when resource is patched
        by the whole body, hence these fields are removed.

        Args:
            name (str): Resource name.
            namespace (str): Resource namespace.
            kubernetes (Kubernetes): Kubernetes utils instance.

        Returns:
            YaookResource instance.

        """
        resp = kubernetes.get_cr(
            name=name,
            namespace=namespace,
            plural=cls.RESOURCE_PLURAL,
        )

        resp.get("metadata", {}).pop("resourceVersion", None)
        resp.get("metadata", {}).pop("creationTimestamp", None)

        return cls(resp, kubernetes)

    @property
    def name(self) -> str:
        """Get resource name.

        Returns:
            str: Resource name.

        """
        return self.obj.metadata.name

    @property
    def namespace(self) -> str:
        """Get resource namespace.

        Returns:
            str: Resource namespace.

        """
        return self.obj.metadata.namespace

    def load_dependencies(self):
        """Load resource dependencies."""
        raise NotImplementedError

    def get_dependencies(
        self,
    ):
        """Get resource dependencies.

        Returns:
            NamedTuple: Deps instance.

        """
        if not self.deps:
            self.deps = self.load_dependencies()

        return self.deps

    def get_config(self):
        """Get resource config variables."""
        raise NotImplementedError

    def get_obj(
        self, body: dict
    ) -> Union[
        YaookService,
        YaookCluster,
        YaookConfigTemplate,
        YaookMachineTemplate,
        YaookMachineDeployment,
        YaookControlPlane,
    ]:
        """Get resource object.

        Args:
            body (dict): Resource body.

        Returns:
            Union[
                YaookService, YaookCluster, YaookConfigTemplate, YaookMachineTemplate,
                YaookMachineDeployment, YaookControlPlane
            ]: Resource object.

        Raises:
            ProviderPermanentError: If model validation failed.

        """
        try:
            return self.RESOURCE_MODEL(**body)
        except ValidationError as err:
            raise ProviderPermanentError(
                reason=ProviderPermanentErrorReason.RESOURCE_VALIDATION_ERROR.name,
                message=json.dumps(err.json()),
            )

    def get_resource(
        self, name: str, namespace: str, ignore_errors: bool = False
    ) -> dict:
        """Get custom resource.

        Args:
            name (str): Resource name.
            namespace (str): Resource namespace.
            ignore_errors (bool, optional): Ignore any error occur,
                just log debug message. Defaults to False.

        Returns:
            dict: Resource.

        """
        return self.kubernetes.get_cr(
            name=name,
            namespace=namespace,
            plural=self.RESOURCE_PLURAL,
            ignore_errors=ignore_errors,
        )

    def is_reconciling(self) -> bool:
        """Evaluate if the resource is in the reconciling state.

        Returns:
            bool: True if the resource is in reconciling state, False otherwise.

        """
        if not self.obj.status:
            return False

        if self.obj.status.phase == ResourcePhase.RECONCILING:
            return True

        return False

    def is_running(self) -> bool:
        """Evaluate if the resource is in the running state.

        Returns:
            bool: True if the resource is in running state, False otherwise.

        """
        if not self.obj.status:
            return False

        if self.obj.status.phase == ResourcePhase.RUNNING:
            return True

        return False

    def is_deleting(self) -> bool:
        """Evaluate if the resource is in the deleting state.

        This is evaluated based on the presence of `deletionTimestamp`
        field in the metadata section of the raw resource body.

        Returns:
            bool: True if the resource is in deleting state, False otherwise.

        """
        if self.body.get("metadata", {}).get("deletionTimestamp"):
            return True

        return False

    def set_owner(
        self,
        owner_body: dict,
        owner_controller: bool = True,
        ignore_errors: bool = False,
    ) -> dict:
        """Set resource owner reference.

        Args:
            owner_body (dict): Resource body of the owner resource.
            owner_controller (bool, optional): If True, the owner is also the
                controller of the object. Defaults to False.
            ignore_errors (bool, optional): Ignore any error occur,
                just log debug message. Defaults to False.

        Returns:
            dict: Updated resource.

        """
        return self.kubernetes.patch_cr(
            name=self.name,
            namespace=self.namespace,
            plural=self.RESOURCE_PLURAL,
            body=self.body,
            owner=True,
            owner_body=owner_body,
            owner_controller=owner_controller,
            ignore_errors=ignore_errors,
        )

    def set_status(self, status: dict, ignore_errors: bool = False) -> dict:
        """Set resource status.

        Args:
            status (dict): Status to be set in the resource.
            ignore_errors (bool, optional): Ignore any error occur,
                just log debug message. Defaults to False.

        Returns:
            dict: Updated resource.

        """
        return self.kubernetes.patch_cr(
            name=self.name,
            namespace=self.namespace,
            plural=self.RESOURCE_PLURAL,
            body={"status": status},
            status=True,
            ignore_errors=ignore_errors,
        )

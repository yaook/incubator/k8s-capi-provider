"""Module contains YaookCluster resource data."""
from __future__ import annotations

from enum import Enum
from pathlib import Path
from typing import Dict, List, NamedTuple, Optional

from ..data.base import ResourceAction, ResourcePhase, YaookResourceBase
from ..models.yaookcluster import Status as YaookClusterStatus
from ..models.yaookcluster import YaookCluster
from ..models.yaookconfigtemplate import YaookConfigTemplate
from ..models.yaookcontrolplane import YaookControlPlane
from ..models.yaookmachinedeployment import YaookMachineDeployment
from ..models.yaookmachinetemplate import YaookMachineTemplate
from ..settings import CRD, ProviderSettings, get_settings
from ..utils.kubernetes import Kubernetes
from ..utils.openstack import Flavor, Quota

SETTINGS: ProviderSettings = get_settings()
CRDS: CRD = SETTINGS.crd


class YaookClusterStage(str, Enum):
    """YaookCluster resource stages and transitions.

    Underlying yaook/k8s project provides LCM of
    workload clusters in a layered manner.
    Each layer is defined by `name` and `value`
    which points the provider to the shell
    script which should be executed to deploy/
    install particular layer.

    """

    init = "init.sh"
    terraform = "apply-terraform.sh"
    stage2 = "apply-stage2.sh"
    stage3 = "apply-stage3.sh"
    upgrade = "upgrade.sh"
    delete = "destroy.sh"

    def __str__(self) -> str:
        """Stage's value."""
        return str(self.value)

    def path(
        self, path: Path, namespace: Optional[str] = None, version: Optional[str] = None
    ) -> str:
        """Return yaook/k8s stage path.

        Args:
            path (Path): Cluster path.
            namespace (str, optional): Cluster namespace.
            version(str, optional): Kubernetes version.

        Returns:
            str: yaook/k8s stage path

        """
        stage_path = "/".join([str(path), "actions", self.value])

        if version and namespace:
            return f"{namespace}{stage_path} {version}"

        if namespace:
            return f"{namespace}{stage_path}"

        if version:
            return f"{stage_path} {version}"

        return stage_path

    def status_before(self, action: ResourceAction) -> Optional[YaookClusterStatus]:
        """Return status that reflect cluster state before the given action and stage.

        Actions:
         CREATE:
          Set initial states to False. The cluster is not ready.
         UPDATE (UPGRADE):
          Set initial states to False. Currently, we do not know
          what is going to be modified. Therefore we do no know which layers
          are ready or not and we assume that the whole cluster is not ready
          at all, during the update process.
          FIXME: Provide better visibility on what is going to be modified
           and set the statuses accordingly.
         RESUME:
          Keep the *Ready statuses as they are and set only the phase.
         RECONCILE:
          Keep the *Ready statuses as they are and set only the phase.
         DELETE:
          Set initial states to False. The cluster is going to be deleted.

        Args:
            action (ResourceAction): Resource action which will be executed

        Returns:
            YaookClusterStatus: Status fields that reflect current
                action and stage

        """
        if self.name == YaookClusterStage.init.name:
            """Stage init."""
            if action == ResourceAction.CREATE:
                return YaookClusterStatus(
                    phase=ResourcePhase.CREATING,
                    ready=False,
                    infrastructureReady=False,
                    bootstrapReady=False,
                )

            if action == ResourceAction.UPDATE:
                return YaookClusterStatus(
                    phase=ResourcePhase.UPDATING,
                    ready=False,
                    infrastructureReady=False,
                    bootstrapReady=False,
                )

            if action == ResourceAction.RESUME:
                return YaookClusterStatus(
                    phase=ResourcePhase.RESUMING,
                )

            if action == ResourceAction.RECONCILE:
                return YaookClusterStatus(
                    phase=ResourcePhase.RECONCILING,
                )

            if action == ResourceAction.DELETE:
                return YaookClusterStatus(
                    phase=ResourcePhase.TERMINATING,
                    ready=False,
                    infrastructureReady=False,
                    bootstrapReady=False,
                )

        if self.name == YaookClusterStage.terraform.name:
            """Stage terraform."""

        if self.name == YaookClusterStage.stage2.name:
            """Stage 2."""

        if self.name == YaookClusterStage.stage3.name:
            """Stage 3."""

        if self.name == YaookClusterStage.upgrade.name:
            """Stage upgrade."""
            return YaookClusterStatus(
                phase=ResourcePhase.UPDATING,
                ready=False,
                infrastructureReady=False,
                bootstrapReady=False,
            )

        if self.name == YaookClusterStage.delete.name:
            """Stage delete."""

    def status_after(self, action: ResourceAction) -> Optional[YaookClusterStatus]:
        """Return status that reflect cluster state after the given action and stage.

        Actions:
         CREATE:
         UPDATE:
         RESUME:
         RECONCILE:
          Set *Ready statuses accordingly to the stage. After the stage2 the
          infrastructure layer is ready. After the stage3 the bootstrap
          layer is ready.
          Then the entire luster is considered as ready.
          After the final stage3 erase also any previous failure reason and message.
          In case of upgrade the entire luster is considered as ready as well.

          Reconciling/Resuming should be an idempotent operation, therefore just force
          the infrastructure/bootstrap ready status according to stage to True
          and keep the status of whole cluster as it is.
          It ensures that reconciliation from FAILED state works as expected.
         DELETE:
          Do nothing. The cluster is going to be deleted.

        Args:
            action (ResourceAction): Resource action which was executed

        Returns:
            YaookClusterStatus: Status fields that reflect current
                action and stage

        """
        if self.name == YaookClusterStage.init.name:
            """Stage init."""

        if self.name == YaookClusterStage.terraform.name:
            """Stage terraform."""

        if self.name == YaookClusterStage.stage2.name:
            """Stage 2."""
            if action == ResourceAction.CREATE:
                return YaookClusterStatus(
                    phase=ResourcePhase.CREATING,
                    ready=False,
                    infrastructureReady=True,
                )
            if action == ResourceAction.UPDATE:
                return YaookClusterStatus(
                    phase=ResourcePhase.UPDATING,
                    ready=False,
                    infrastructureReady=True,
                )
            if action == ResourceAction.RESUME:
                return YaookClusterStatus(
                    phase=ResourcePhase.RESUMING,
                    infrastructureReady=True,
                )
            if action == ResourceAction.RECONCILE:
                return YaookClusterStatus(
                    phase=ResourcePhase.RECONCILING,
                    infrastructureReady=True,
                )

        if self.name == YaookClusterStage.stage3.name:
            """Stage 3."""
            if action in (
                ResourceAction.CREATE,
                ResourceAction.UPDATE,
                ResourceAction.RESUME,
                ResourceAction.RECONCILE,
            ):
                return YaookClusterStatus(
                    phase=ResourcePhase.RUNNING,
                    ready=True,
                    bootstrapReady=True,
                    failureReason="",
                    failureMessage="",
                )

        if self.name == YaookClusterStage.upgrade.name:
            """Stage upgrade."""
            if action in (
                ResourceAction.CREATE,
                ResourceAction.UPDATE,
                ResourceAction.RESUME,
                ResourceAction.RECONCILE,
            ):
                return YaookClusterStatus(
                    phase=ResourcePhase.RUNNING,
                    ready=True,
                    infrastructureReady=True,
                    bootstrapReady=True,
                    failureReason="",
                    failureMessage="",
                )

        if self.name == YaookClusterStage.delete.name:
            """Stage delete."""


class Gateway(NamedTuple):
    """Yaook gateway."""

    azs: List[str]
    flavor: Flavor

    @property
    def size(self) -> int:
        """Return the number of gateways."""
        return len(self.azs)

    @property
    def floating_ips(self) -> int:
        """Return the number of floating ips requested by gateways."""
        return self.size + 1 if self.size else 0

    @property
    def vcpus(self) -> int:
        """Return the number of VCPUs of gateways."""
        return self.flavor.vcpus * self.size

    @property
    def ram(self) -> int:
        """Return the amount of RAM of gateways."""
        return self.flavor.ram * self.size

    def get_quota(self) -> Quota:
        """Get Gateway quota.

        Returns:
            Quota: current or desired quota.

        """
        return Quota(
            floating_ips=self.floating_ips,
            instances=self.size,
            ram=self.ram,
            vcpus=self.vcpus,
        )


class YaookClusterDeps(NamedTuple):
    """Named tuple for identifying YaookCluster resource dependencies."""

    control_plane: YaookControlPlaneResource
    machine_deployments: List[YaookMachineDeploymentResource]


class YaookClusterResource(YaookResourceBase):
    """YaookCluster resource."""

    RESOURCE_MODEL = YaookCluster
    RESOURCE_PLURAL = CRDS.cluster_plural

    @property
    def control_plane_name(self) -> str:
        """Get control plane name from ControlPlane resource spec.

        Returns:
            str: Control plane name.

        """
        return self.obj.spec.controlPlaneRef.name

    def load_dependencies(self) -> YaookClusterDeps:
        """Load resource dependencies.

        Returns:
            YaookClusterDeps instance.

        """
        md_list = self.kubernetes.list_cr(
            namespace=self.namespace,
            plural=CRDS.machine_deployment_plural,
        )
        machine_deployments = [
            YaookMachineDeploymentResource(md, self.kubernetes)
            for md in md_list["items"]
            if md["spec"]["clusterName"] == self.name
        ]
        control_plane = YaookControlPlaneResource.from_k8s(
            name=self.control_plane_name,
            namespace=self.namespace,
            kubernetes=self.kubernetes,
        )
        control_plane.set_azs(self.obj.spec.controlPlaneAvailabilityZones or [])

        return YaookClusterDeps(
            control_plane=control_plane,
            machine_deployments=machine_deployments,
        )

    @property
    def managed_provider_identity(self) -> bool:
        """Get managed_provider_identity flag.

        True if the ssh/gpg/wg identities will be created by k8s-capi-provider.
        The provider identity should not be created by k8s-capi-provider when
        the cluster was injected (by `yaookctl inject`) from yaook/k8s LCM.
        Then, the origin yaook/k8s ssh/gpg/wg identities (private keys) are
        used as a provider one and should not be created again.

        Returns:
            bool: managed_provider_identity flag

        """
        return self.obj.spec.lcm.MANAGED_PROVIDER_IDENTITY

    def get_openstack_rc(self) -> dict:
        """Get OpenStack RC variables from k8s secret."""
        secret = self.kubernetes.get_secret(
            self.obj.spec.openStackRC.secretRef, self.namespace
        )
        return dict(
            (key, value.decode())
            for key, value in self.kubernetes.get_secret_data(secret)
        )

    def get_lcm(self) -> dict:
        """Get resource LCM config variables.

        Returns:
            dict: LCM config composed of resource spec.

        """
        return self.obj.spec.lcm.dict(exclude_none=True, by_alias=True)

    def get_config(self) -> dict:
        """Get resource config variables.

        Returns:
            dict: Config composed of resource spec.

        """
        return {
            "terraform": self.obj.spec.terraform.dict(exclude_none=True, by_alias=True),
            "wireguard": self.obj.spec.wireguard.dict(exclude_none=True, by_alias=True),
            "ipsec": self.obj.spec.ipsec.dict(exclude_none=True, by_alias=True),
            "cah-users": self.obj.spec.cah_users.dict(exclude_none=True, by_alias=True),
            "passwordstore": self.obj.spec.passwordstore.dict(
                exclude_none=True, by_alias=True
            ),
            "miscellaneous": self.obj.spec.miscellaneous.dict(
                exclude_none=True, by_alias=True
            ),
            "load-balancing": self.obj.spec.load_balancing.dict(
                exclude_none=True, by_alias=True
            ),
        }

    def get_gateway_flavor(self, current: bool = False) -> str:
        """Get Gateway flavor name.

        Returns:
            str: current or desired flavor name.

        """
        if current:
            return self.body.get("status", {}).get("gateway", {}).get("flavor", "XS")

        return self.obj.spec.terraform.gateway_flavor

    def get_gateway_azs(self, current: bool = False) -> List[str]:
        """Get Gateway azs.

        Returns:
            List[str]: current or desired azs.

        """
        if current:
            return self.body.get("status", {}).get("gateway", {}).get("azs", [])

        return self.obj.spec.terraform.azs

    def get_gateway_quota(
        self, flavors: Dict[str, Flavor], current: bool = False
    ) -> Quota:
        """Get Gateway quota.

        Returns:
            Quota: current or desired quota.

        """
        azs = self.get_gateway_azs(current=current)
        flavor = self.get_gateway_flavor(current=current)
        return Gateway(azs, flavors[flavor]).get_quota()

    def get_gateway_config(self, current: bool = False) -> dict:
        """Get gateway config.

        Returns:
            dict: current or desired config.

        """
        azs = self.get_gateway_azs(current=current)
        flavor = self.get_gateway_flavor(current=current)

        return {
            "azs": azs,
            "gateway_flavor": flavor,
            "gateway_image_name": self.obj.spec.terraform.gateway_image_name,
        }

    def get_server_groups(self, current: bool = False) -> int:
        """Get Cluster server groups.

        Default number of server group is 1 because underlying yaook/k8s
        scripts create one server group per cluster.

        Returns:
            int: number of server groups.

        """
        if current:
            return (
                self.body.get("status", {}).get("openstack", {}).get("serverGroups", 0)
            )

        return 1

    def update_workers_terraform_config(
        self,
        terraform_config: dict,
        md: YaookMachineDeploymentResource,
        replicas: Optional[int] = None,
    ) -> None:
        """Update terraform workers config with new MachineDeployment."""
        if replicas is None:
            replicas = md.get_replicas()

        workers = terraform_config.get("workers", 0)
        worker_azs = terraform_config.get("worker_azs", [])
        worker_flavors = terraform_config.get("worker_flavors", [])
        worker_images = terraform_config.get("worker_images", [])
        worker_names = terraform_config.get("worker_names", [])

        workers += replicas
        if md_az := md.get_availability_zone():
            worker_azs += [md_az] * replicas
        else:
            mod = len(worker_azs) % len(self.get_gateway_azs())
            worker_azs += (self.get_gateway_azs() * replicas)[mod : replicas + mod]
        worker_flavors += [md.get_flavor()] * replicas
        worker_images += [md.get_image()] * replicas
        worker_names += ["-".join((md.name, str(i))) for i in range(replicas)]

        terraform_config.update(
            {
                "workers": workers,
                "worker_azs": worker_azs,
                "worker_flavors": worker_flavors,
                "worker_images": worker_images,
                "worker_names": worker_names,
            }
        )

    @staticmethod
    def reset_workers_terraform_config(
        terraform_config: dict,
    ) -> None:
        """Reset terraform workers config."""
        terraform_config.update(
            {
                "workers": 0,
                "worker_azs": [],
                "worker_flavors": [],
                "worker_images": [],
                "worker_names": [],
            }
        )

    def set_workers_status(
        self,
        terraform_config: dict,
        machine_deployments: Optional[List[YaookMachineDeploymentResource]] = None,
    ):
        """Set workers status."""
        if machine_deployments is None:
            machine_deployments = self.get_dependencies().machine_deployments

        md_idx = 0
        for md in machine_deployments:
            count = 0
            for worker_name in terraform_config["worker_names"][md_idx:]:
                if worker_name.rsplit("-", 1)[0] == md.name:
                    count += 1
                else:
                    break
            flavor = terraform_config["worker_flavors"][md_idx] if count else None
            md_idx += count
            md.set_status(
                status={
                    "replicas": count,
                    "flavor": flavor,
                }
            )

    def set_overall_openstack_cluster_status(
        self, terraform_config: dict, flavors: Dict[str, Flavor]
    ):
        """Set overall openstack status and status for gateways."""
        gateways = len(terraform_config["azs"])
        instances = terraform_config["masters"] + terraform_config["workers"] + gateways
        flavors_all = (
            terraform_config["master_flavors"]
            + terraform_config["worker_flavors"]
            + [terraform_config["gateway_flavor"]] * gateways
        )
        vcpus = sum([flavors[flavor].vcpus for flavor in flavors_all])
        ram = sum([flavors[flavor].ram for flavor in flavors_all])

        self.set_status(
            status={
                "gateway": {
                    "azs": terraform_config["azs"],
                    "flavor": terraform_config["gateway_flavor"],
                },
                "openstack": {
                    "instances": instances,
                    "vcpus": vcpus,
                    "ram": ram,
                    "floatingIps": gateways + 1,
                    "serverGroups": 1,
                },
            }
        )


class YaookControlPlaneDeps(NamedTuple):
    """Named tuple for identifying YaookControlPlane resource dependencies."""

    config_template: YaookConfigTemplateResource
    machine_template: YaookMachineTemplateResource


class YaookControlPlaneResource(YaookResourceBase):
    """YaookControlPlane resource."""

    RESOURCE_MODEL = YaookControlPlane
    RESOURCE_PLURAL = CRDS.control_plane_plural

    def __init__(self, body: dict, kubernetes: Kubernetes):
        super().__init__(body, kubernetes)
        self.azs: List[str] = []

    @property
    def replicas(self) -> int:
        """Get number of replicas.

        Returns:
            int: Number of replicas.

        """
        return self.obj.spec.replicas

    @property
    def cluster_namespace(self) -> str:
        """Get cluster namespace.

        We assume that the YaookControlPlane resource
        has to be deployed in the same namespace as
        YaookCluster resource.

        Returns:
            str: Cluster namespace.

        """
        return self.namespace

    @property
    def machine_template_name(self) -> str:
        """Get machine template name from ControlPlane resource spec.

        Returns:
            str: Machine template name.

        """
        return self.obj.spec.machineTemplate.infrastructureRef.name

    @property
    def config_template_name(self) -> str:
        """Get config template name from ControlPlane resource spec.

        Returns:
            str: Config template name.

        """
        return self.obj.spec.bootstrap.configRef.name

    def load_dependencies(self) -> YaookControlPlaneDeps:
        """Load resource dependencies.

        Returns:
            YaookControlPlaneDeps instance.

        """
        return YaookControlPlaneDeps(
            config_template=YaookConfigTemplateResource.from_k8s(
                name=self.config_template_name,
                namespace=self.cluster_namespace,
                kubernetes=self.kubernetes,
            ),
            machine_template=YaookMachineTemplateResource.from_k8s(
                name=self.machine_template_name,
                namespace=self.cluster_namespace,
                kubernetes=self.kubernetes,
            ),
        )

    def set_azs(self, azs: List[str]) -> None:
        """Set control plane availability zones."""
        self.azs = azs

    def get_config(self) -> dict:
        """Get resource config variables.

        Returns:
            dict: Config composed of resource spec.

        """
        return {"terraform": self.get_terraform_config()}

    def get_terraform_config(self, current: bool = False) -> dict:
        """Get terraform config variables.

        Returns:
            dict: Config composed of terraform variables.

        """
        masters = self.get_replicas(current=current)
        master_azs = self.azs
        master_flavors = [self.get_flavor(current=current)] * masters
        master_images = [self.get_image()] * masters
        master_names = ["-".join((self.name, str(i))) for i in range(masters)]

        return {
            "masters": masters,
            "master_azs": master_azs,
            "master_flavors": master_flavors,
            "master_images": master_images,
            "master_names": master_names,
        }

    def get_quota(self, flavors: Dict[str, Flavor], current: bool = False) -> Quota:
        """Get ControlPlane quota.

        Returns:
            Quota: current or desired quota.

        """
        masters = self.get_replicas(current=current)
        flavor = self.get_flavor(current=current)
        ram = flavors[flavor].ram * masters
        vcpus = flavors[flavor].vcpus * masters

        return Quota(instances=masters, ram=ram, vcpus=vcpus)

    def get_replicas(self, current: bool = False) -> int:
        """Get ControlPlane replicas.

        Returns:
            int: current or desired replicas.

        """
        if current:
            return self.body.get("status", {}).get("replicas", 0)

        return self.replicas

    def get_flavor(self, current: bool = False) -> str:
        """Get ControlPlane flavor name.

        Returns:
            str: current or desired flavor name.

        """
        if current:
            return self.body.get("status", {}).get("flavor", "M")

        machine_template = self.get_dependencies().machine_template
        return machine_template.obj.spec.flavor

    def get_image(self) -> str:
        """Get ControlPlane image name.

        Returns:
            str: image name.

        """
        machine_template = self.get_dependencies().machine_template
        return machine_template.obj.spec.image

    def set_masters_status(
        self,
        terraform_config: dict,
    ):
        """Set masters status."""
        self.set_status(
            status={
                "replicas": terraform_config["masters"],
                "flavor": terraform_config["master_flavors"][0],
            }
        )


class YaookMachineDeploymentDeps(NamedTuple):
    """Named tuple for identifying YaookMachineDeployment resource dependencies."""

    machine_template: YaookMachineTemplateResource


class YaookMachineDeploymentResource(YaookResourceBase):
    """YaookMachineDeployment resource."""

    RESOURCE_MODEL = YaookMachineDeployment
    RESOURCE_PLURAL = CRDS.machine_deployment_plural

    @property
    def replicas(self) -> int:
        """Get number of replicas.

        Returns:
            int: Number of replicas.

        """
        return self.obj.spec.replicas

    @property
    def cluster_name(self) -> str:
        """Get cluster name from MachineDeployment resource spec.

        Returns:
            str: Cluster name.

        """
        return self.obj.spec.clusterName

    @property
    def cluster_namespace(self) -> str:
        """Get cluster namespace.

        We assume that the YaookMachineDeployment resource
        has to be deployed in the same namespace as
        YaookCluster resource.

        Returns:
            str: Cluster namespace.

        """
        return self.namespace

    @property
    def machine_template_name(self) -> str:
        """Get machine template name from MachineDeployment resource spec.

        Returns:
            str: Machine template name.

        """
        return self.obj.spec.template.spec.infrastructureRef.name

    def load_dependencies(self) -> YaookMachineDeploymentDeps:
        """Load resource dependencies.

        Returns:
            YaookMachineDeploymentDeps instance.

        """
        return YaookMachineDeploymentDeps(
            machine_template=YaookMachineTemplateResource.from_k8s(
                name=self.machine_template_name,
                namespace=self.cluster_namespace,
                kubernetes=self.kubernetes,
            ),
        )

    def get_config(self) -> dict:
        """Get resource config variables.

        Returns:
            dict: Config composed of resource spec.

        """
        return {}

    def get_quota(
        self,
        flavors: Dict[str, Flavor],
        current: bool = False,
        replicas: Optional[int] = None,
    ) -> Quota:
        """Get MachineDeployment quota.

        Returns:
            Quota: current or desired quota.

        """
        workers = replicas or self.get_replicas(current=current)
        flavor = self.get_flavor(current=current)
        ram = flavors[flavor].ram * workers
        vcpus = flavors[flavor].vcpus * workers

        return Quota(instances=workers, ram=ram, vcpus=vcpus)

    def get_replicas(self, current: bool = False) -> int:
        """Get MachineDeployment replicas.

        Returns:
            int: current or desired replicas.

        """
        if current:
            return self.body.get("status", {}).get("replicas", 0)

        return self.replicas

    def get_flavor(self, current: bool = False) -> str:
        """Get MachineDeployment flavor name.

        Returns:
            str: current or desired flavor name.

        """
        if current:
            return self.body.get("status", {}).get("flavor", "M")

        machine_template = self.get_dependencies().machine_template
        return machine_template.obj.spec.flavor

    def get_image(self) -> str:
        """Get MachineDeployment image name.

        Returns:
            str: image name.

        """
        machine_template = self.get_dependencies().machine_template
        return machine_template.obj.spec.image

    def get_availability_zone(self) -> str:
        """Get MachineDeployment availability zone.

        Returns:
            str: availability zone.

        """
        return self.obj.spec.template.spec.failureDomain

    def calculate_max_number_of_workers(
        self, flavors: Dict[str, Flavor], available_quotas: Quota
    ) -> int:
        """Calculate maximum number of workers."""
        current_quota = self.get_quota(flavors, current=True)
        desired_flavor = self.get_flavor()
        max_replicas = available_quotas.instances + current_quota.instances
        max_ram_replicas = (available_quotas.ram + current_quota.ram) // flavors[
            desired_flavor
        ].ram
        max_vcpus_replicas = (available_quotas.vcpus + current_quota.vcpus) // flavors[
            desired_flavor
        ].vcpus

        return min(
            self.replicas,
            max_replicas,
            max_ram_replicas,
            max_vcpus_replicas,
        )


class YaookMachineTemplateResource(YaookResourceBase):
    """YaookMachineTemplate resource."""

    RESOURCE_MODEL = YaookMachineTemplate
    RESOURCE_PLURAL = CRDS.machine_template_plural

    @property
    def cluster_namespace(self) -> str:
        """Get cluster namespace.

        We assume that the YaookMachineTemplate resource
        has to be deployed in the same namespace as
        YaookCluster resource.

        Returns:
            str: Cluster namespace.

        """
        return self.namespace

    def load_dependencies(self):
        """Load resource dependencies.

        Currently, the YaookMachineTemplateResource does not have
        any further dependencies.

        """

    def get_config(self) -> dict:
        """Get resource config variables.

        Returns:
            dict: Config composed of resource spec.

        """
        return {}


class YaookConfigTemplateResource(YaookResourceBase):
    """YaookConfigTemplate resource."""

    RESOURCE_MODEL = YaookConfigTemplate
    RESOURCE_PLURAL = CRDS.config_template_plural

    @property
    def cluster_namespace(self) -> str:
        """Get cluster namespace.

        We assume that the YaookConfigTemplate resource
        has to be deployed in the same namespace as
        YaookCluster resource.

        Returns:
            str: Cluster namespace.

        """
        return self.namespace

    def load_dependencies(self):
        """Load resource dependencies.

        Currently, the YaookConfigTemplateResource does not have
        any further dependencies.

        """

    def get_config(self) -> dict:
        """Get resource config variables.

        Returns:
            dict: Config composed of resource spec.

        """
        return {
            "kubernetes": self.obj.spec.kubernetes.dict(
                exclude_none=True, by_alias=True
            ),
            "ch-k8s-lbaas": self.obj.spec.ch_k8s_lbaas.dict(
                exclude_none=True, by_alias=True
            ),
            "node-scheduling": self.obj.spec.node_scheduling.dict(
                exclude_none=True, by_alias=True
            ),
            "testing": self.obj.spec.testing.dict(exclude_none=True, by_alias=True),
        }

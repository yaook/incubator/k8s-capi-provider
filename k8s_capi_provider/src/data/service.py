"""Module contains YaookService resource data."""
from enum import Enum
from pathlib import Path
from typing import NamedTuple, Optional

from ..data.base import ResourceAction, ResourcePhase, YaookResourceBase
from ..data.cluster import YaookClusterResource
from ..models.yaookservice import Status as YaookServiceStatus
from ..models.yaookservice import YaookService
from ..settings import CRD, ProviderSettings, get_settings

SETTINGS: ProviderSettings = get_settings()
CRDS: CRD = SETTINGS.crd


class YaookServiceStage(str, Enum):
    """YaookService resource stages and transitions.

    Underlying yaook/k8s project provides LCM of
    workload clusters in a layered manner.
    Each layer is defined by `name` and `value`
    which points the provider to the shell
    script which should be executed to deploy/
    install particular layer.

    """

    init = "init.sh"
    stage4 = "apply-stage4.sh"
    stage5 = "apply-stage5.sh"

    def __str__(self) -> str:
        """Stage's value."""
        return str(self.value)

    def path(self, path: Path, namespace: Optional[str] = None) -> str:
        """Return yaook/k8s stage path.

        Args:
            path (Path): Cluster path.
            namespace (str, optional): Cluster namespace.

        Returns:
            str: yaook/k8s stage path

        """
        stage_path = "/".join([str(path), "actions", self.value])
        if namespace:
            return namespace + stage_path

        return stage_path

    def status_before(self, action: ResourceAction) -> Optional[YaookServiceStatus]:
        """Return status that reflect service state before the given action and stage.

        Actions:
         CREATE:
          Set ready status to False. The service layer is not ready.
         UPDATE:
          Set ready status to False. The service layer is not ready.
         RESUME:
          Keep the ready status as is are and set only the phase.
         RECONCILE:
          Keep the ready status as is are and set only the phase.
         DELETE:
          et ready status to False. The service is going to be deleted.

        Args:
            action (ResourceAction): Resource action which will be executed

        Returns:
            YaookServiceStatus: Status fields that reflect current
                action and stage

        """
        if self.name == YaookServiceStage.init.name:
            """Stage init."""
            if action == ResourceAction.CREATE:
                return YaookServiceStatus(
                    phase=ResourcePhase.CREATING,
                    ready=False,
                )

            if action == ResourceAction.UPDATE:
                return YaookServiceStatus(
                    phase=ResourcePhase.UPDATING,
                    ready=False,
                )

            if action == ResourceAction.RESUME:
                return YaookServiceStatus(
                    phase=ResourcePhase.RESUMING,
                )

            if action == ResourceAction.RECONCILE:
                return YaookServiceStatus(
                    phase=ResourcePhase.RECONCILING,
                )

            if action == ResourceAction.DELETE:
                return YaookServiceStatus(
                    phase=ResourcePhase.TERMINATING,
                    ready=False,
                )

        if self.name == YaookServiceStage.stage4.name:
            """Stage 4."""

        if self.name == YaookServiceStage.stage5.name:
            """Stage 5."""

    def status_after(self, action: ResourceAction) -> Optional[YaookServiceStatus]:
        """Return status that reflect service state after the given action and stage.

        Actions:
         CREATE:
         UPDATE:
         RESUME:
         RECONCILE:
          After the successful final stage5, service layer is ready.
          After that final stage erase also any previous failure reason and message.
         DELETE:
          Do nothing. The service layer is going to be deleted.

        Args:
            action (ResourceAction): Resource action which was executed

        Returns:
            YaookServiceStatus: Status fields that reflect current
                action and stage

        """
        if self.name == YaookServiceStage.init.name:
            """Stage init."""

        if self.name == YaookServiceStage.stage4.name:
            """Stage 4."""

        if self.name == YaookServiceStage.stage5.name:
            """Stage 5."""
            if action in (
                ResourceAction.CREATE,
                ResourceAction.UPDATE,
                ResourceAction.RESUME,
                ResourceAction.RECONCILE,
            ):
                return YaookServiceStatus(
                    phase=ResourcePhase.RUNNING,
                    ready=True,
                    failureReason="",
                    failureMessage="",
                )


class YaookServiceDeps(NamedTuple):
    """Named tuple for identifying YaookService resource dependencies."""

    cluster: YaookClusterResource


class YaookServiceResource(YaookResourceBase):
    """YaookService resource."""

    RESOURCE_MODEL = YaookService
    RESOURCE_PLURAL = CRDS.service_plural

    @property
    def cluster_name(self) -> str:
        """Get cluster name from service resource spec.

        Returns:
            str: Cluster name.

        """
        return self.obj.spec.clusterName

    @property
    def cluster_namespace(self) -> str:
        """Get cluster namespace.

        We assume that the YaookService resource
        has to be deployed in the same namespace as
        YaookCluster resource.

        Returns:
            str: Cluster namespace.

        """
        return self.namespace

    def load_dependencies(self) -> YaookServiceDeps:
        """Load resource dependencies.

        Returns:
            YaookServiceDeps instance.

        """
        return YaookServiceDeps(
            cluster=YaookClusterResource.from_k8s(
                name=self.cluster_name,
                namespace=self.cluster_namespace,
                kubernetes=self.kubernetes,
            )
        )

    def get_config(self) -> dict:
        """Get resource config variables.

        Returns:
            dict: Config composed of resource spec.

        """
        return {
            "k8s-service-layer": self.obj.spec.k8s_service_layer.dict(
                exclude_none=True,
                by_alias=True,
            ),
        }

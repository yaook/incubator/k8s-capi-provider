"""Module comprises settings of k8s-capi-provider project.

Settings inherit from pydantic :class:`BaseSettings` which enables
enhance settings management, see https://pydantic-docs.helpmanual.io/usage/settings/
for details.

"""
from datetime import time, timedelta
from functools import lru_cache
from pathlib import Path
from typing import Union

from pydantic import BaseSettings


class ProviderBase(BaseSettings):
    """Provider base settings."""

    class Config:
        """Config of ProviderBase pydantic model."""

        env_prefix = "provider_"
        env_nested_delimiter = "__"


class State(ProviderBase):
    """State definition."""

    name: str
    path: Path


class SSHMember(ProviderBase):
    """SSH member definition."""

    state: State
    keypair: str


class SSH(ProviderBase):
    """SSH settings."""

    provider: SSHMember = SSHMember(
        state=State(name="provider-ssh-key", path=".ssh/provider_id_rsa"),
        keypair="k8s-provider",
    )
    user: SSHMember = SSHMember(
        state=State(name="user-ssh-key", path=".ssh/user_id_rsa"), keypair="k8s-user"
    )
    keypair_merged = "k8s-merged"


class GPG(ProviderBase):
    """GPG settings."""

    state = State(name="gnupg", path=".gnupg")
    name = "k8s-gpg@example.com"
    email = "k8s-gpg@example.com"

    agent_conf = "auto-expand-secmem"
    agent_conf_file = "gpg-agent.conf"


class Wireguard(ProviderBase):
    """Wireguard settings."""

    state = State(name="wg-key", path=".wireguard/wg.key")
    email = "k8s-wg@example.com"


class Terraform(ProviderBase):
    """Terraform settings."""

    state = State(name="terraform-tfstate", path="terraform/terraform.tfstate")
    state_vars = State(name="terraform-tfvars", path="terraform/config.tfvars.json")


class Ansible(ProviderBase):
    """Ansible settings."""

    state = State(name="inventory", path="inventory")
    kubeconfig: Path = state.path / ".etc" / "admin.conf"
    collections: Path = Path("collections")


class ConfigToml(ProviderBase):
    """Yaook config.toml settings."""

    state = State(name="config-toml", path="config")
    path: Path = Path("config/config.toml")


class YaookGit(ProviderBase):
    """Yaook git settings."""

    url = "https://gitlab.com/yaook/k8s.git"
    version = "fd54cd828d6ea65f16a2a67f0aab9eecb2cf27a5"
    directory: Path = Path("k8s")
    template_path: Path = directory / "templates" / "config.template.toml"


class ClusterLogging(ProviderBase):
    """Cluster logging settings."""

    log2file: bool = True
    path_info: Path = Path("logs") / "info.log"
    path_warning: Path = Path("logs") / "warning.log"
    path_error: Path = Path("logs") / "error.log"
    backup_count: int = 5
    rotation: Union[str, int, time, timedelta] = "1 MB"


class CtrlBase(ProviderBase):
    """Base controller settings."""

    path: Path = Path("clusters")
    logging: ClusterLogging = ClusterLogging()
    # Relative path to the manage k8s submodule
    path_managed: Path = Path("./managed-k8s")
    reconcile_wait_for: float = 120.0  # [s]
    reconcile_interval: float = 1800.0  # [s] -> 0.5h
    reconcile_idle: float = 1800.0  # [s] -> 0.5h
    dependencies_wait_for: float = 5.0  # [s]
    cancellation_polling: float = 15.0  # [s]
    client_timeout: int = 60  # [s]
    server_timeout: int = 60  # [s]


class ClusterCtrl(CtrlBase):
    """Cluster controller settings."""

    name: str = "cluster"
    kopf_prefix: str = name


class ServiceCtrl(CtrlBase):
    """Service controller settings."""

    name: str = "service"
    kopf_prefix: str = name
    reconcile_interval: float = 300.0  # [s] -> 5min
    reconcile_idle: float = 300.0  # [s] -> 5min
    dependencies_wait_for: float = 60.0  # [s] -> 1min


class CRD(ProviderBase):
    """CRD settings."""

    group = "yaook.mk8s.io"
    version = "v1"
    cluster_plural = "yaookclusters"
    control_plane_plural = "yaookcontrolplanes"
    machine_deployment_plural = "yaookmachinedeployments"
    machine_template_plural = "yaookmachinetemplates"
    config_template_plural = "yaookconfigtemplates"
    service_plural = "yaookservices"


class ProviderSettings(ProviderBase):
    """Main provider settings."""

    ssh: SSH = SSH()
    gpg: GPG = GPG()
    wireguard: Wireguard = Wireguard()
    terraform: Terraform = Terraform()
    ansible: Ansible = Ansible()
    config_toml: ConfigToml = ConfigToml()
    yaook_git: YaookGit = YaookGit()
    cluster_ctrl: ClusterCtrl = ClusterCtrl()
    service_ctrl: ServiceCtrl = ServiceCtrl()
    crd: CRD = CRD()


@lru_cache()
def get_settings():
    """Get provider settings.

    Provider settings are cached. Hence, this function
    instantiates :class:`ProviderSettings`only once. Any
    further call for settings is handled from cache.
    (if the :class:`ProviderSettings` is not changed over time).

    """
    return ProviderSettings()

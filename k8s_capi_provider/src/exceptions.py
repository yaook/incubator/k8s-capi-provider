"""Module contains exception definitions."""

import re
from enum import Enum
from typing import Union

import kopf


# FIXME: Provide more "robust" error handling.
#   It probably means changing the error handling in the yaook/k8s scripts.
class ProviderPermanentErrorReason(Enum):
    """Constants for provider permanent error reasons.

    Error reason is parsed from the error message. If the error
    message contains string defined in
    :class:`ProviderPermanentErrorReason` values, then the exception
    with :class:`ProviderPermanentErrorReason` reason is raised.

    """

    YAOOK_K8S_ERROR = "Yaook k8s error"  # Arbitrary yaook/k8s scripts error
    K8S_CAPI_PROVIDER_ERROR = (
        "K8s-capi-provider error"  # Arbitrary k8s-capi-provider error
    )
    NETWORK_NAMESPACE_ERROR = "Network namespace error"
    OPENSTACK_RC_ERROR = "OpenStack RC error"
    OPENSTACK_CLIENT_ERROR = "OpenStack client error"
    OPENSTACK_KEYPAIR_ERROR = "OpenStack keypair error"
    QUOTA_EXCEEDED_ERROR = "Quota exceeded"
    RESOURCE_VALIDATION_ERROR = "Resource validation error"


class ProviderTemporaryErrorReason(Enum):
    """Constants for provider temporary error reasons.

    Error reason is parsed from the error message. If the error
    message contains string defined in
    :class:`ProviderTemporaryErrorReason` values, then the exception
    with :class:`ProviderTemporaryErrorReason` reason is raised.

    """

    TIMEOUT_ERROR = "Timeout"
    GIT_LOCK_ERROR = ".git/index.lock': File exists."
    WAITING_FOR_INSTANCE_WITH_ERROR_STATE_ERROR = "Waiting for instance"
    WAITING_FOR_RESOURCE = "Waiting for resource"
    WAITING_FOR_RECONCILE = "Waiting for reconcile"


class ProviderPermanentError(kopf.PermanentError):
    """Exception raised when provider failed permanently."""

    def __init__(self, reason: str, message: str, *args, **kwargs):
        self.reason = reason
        self.message = message
        super().__init__(*args, **kwargs)

    def __str__(self):
        return f"Error {self.reason}: {self.message}"

    def __repr__(self):
        return f"Error {self.reason}: {self.message}"


class ProviderTemporaryError(kopf.TemporaryError):
    """Exception raised when provider failed temporary."""

    def __init__(self, reason: str, message: str, *args, **kwargs):
        self.reason = reason
        self.message = message
        super().__init__(message, *args, **kwargs)

    def __str__(self):
        return f"Error {self.reason}: {self.message}"

    def __repr__(self):
        return f"Error {self.reason}: {self.message}"


RE_ANSI = re.compile(
    r"""
    \x1B  # ESC
    (?:   # 7-bit C1 Fe (except CSI)
        [@-Z\\-_]
    |     # or [ for CSI, followed by a control sequence
        \[
        [0-?]*  # Parameter bytes
        [ -/]*  # Intermediate bytes
        [@-~]   # Final byte
    )
""",
    re.VERBOSE,
)
RE_ERROR = re.compile(r"error:(.*)$", re.IGNORECASE | re.MULTILINE)
RE_JSON = re.compile(r"(?:\"message\":)[ \"](.*)[\"].*", re.IGNORECASE | re.MULTILINE)


def parse_yaook_error(message: bytes) -> str:
    """Parse error message produced by yaook scripts.

    Removes ANSI characters and tries to find
    clear error messages. Then deduplicate
    s them.

    Args:
        message (bytes): Yaook scripts message

    Returns:
        str: Parsed yaook error message

    """
    message_decoded = message.decode()

    if not isinstance(message_decoded, str):
        message_decoded = str(message_decoded)

    message_escaped = RE_ANSI.sub("", message_decoded)
    message_errors = RE_ERROR.findall(message_escaped)
    message_json = RE_JSON.findall(message_escaped)

    if message_json:
        return "\n".join(set(message_json))

    if message_errors:
        return "\n".join(set(message_errors))

    return message_escaped


def get_yaook_reason(
    message: str,
) -> Union[ProviderTemporaryErrorReason, ProviderPermanentErrorReason]:
    """Get error message reason from error message.

    Try to find best fitted reason, otherwise returns
    an arbitrary yaook/k8s error reason.

    Args:
        message (str): Parse yaook error message

    Returns:
        Error message reason

    """
    for reason_temporary in ProviderTemporaryErrorReason:
        if reason_temporary.value.lower() in message.lower():
            return reason_temporary

    for reason_permanent in ProviderPermanentErrorReason:
        if reason_permanent.value.lower() in message.lower():
            return reason_permanent

    return ProviderPermanentErrorReason.YAOOK_K8S_ERROR

"""This module defines helper functions for cluster controller handlers."""
import copy
import json
import os
from ipaddress import IPv4Network
from typing import Dict, List, Optional, Union

import kopf

from ...data.base import ResourceAction
from ...data.cluster import (
    YaookClusterResource,
    YaookClusterStage,
    YaookControlPlaneResource,
    YaookMachineDeploymentResource,
)
from ...exceptions import (
    ProviderPermanentError,
    ProviderPermanentErrorReason,
    ProviderTemporaryError,
    ProviderTemporaryErrorReason,
)
from ...settings import CRD, ProviderSettings, get_settings
from ...utils.common import raise_not_handled_resource
from ...utils.kubernetes import Kubernetes
from ...utils.network import create_network_namespace, get_all_local_networks
from ...utils.openstack import Flavor, OpenStack, Quota
from ...utils.reconcile import execute

SETTINGS: ProviderSettings = get_settings()
CRDS: CRD = SETTINGS.crd
LAST_HANDLED_ANNOTATION = (
    SETTINGS.cluster_ctrl.kopf_prefix + "/last-handled-configuration"
)


def is_updatable(diff: kopf.Diff, **_) -> bool:
    """Evaluate if resource is updatable.

    We do not want to update, when there is only "ok" annotation change.

    Args:
        diff (kopf.Diff): Kopf diff.

    Returns:
        bool: True if there is not only "ok" annotation change, otherwise False.
    """
    plurals = (
        CRDS.control_plane_plural,
        CRDS.machine_deployment_plural,
        CRDS.machine_template_plural,
        CRDS.config_template_plural,
    )
    for op, field, old, new in diff:
        # field = ('notmetadata', ...)
        if field[:1] != ("metadata",):
            return True

        # field = ('metadata',)
        # `new` is a dict and if there is more than 1 key
        # change is definitely also in other field than only in 'annotations'
        if len(field) == 1 and (len(new) > 1 or "annotations" not in new):
            return True

        # annotations did not exist before
        if len(field) == 1 and (
            any(k not in plurals for k in new["annotations"])
            or any(v != "ok" for v in new["annotations"].values())
        ):
            return True

        if len(field) > 1 and field[1] != "annotations":
            return True

        # annotation did not exist before
        # e.g. diff = (
        #               (
        #                 'add',
        #                 ('metadata', 'annotations'),
        #                 None,
        #                 {'yaookmachinedeployments': 'example-cluster-md-0_created'}
        #               ),
        #             )
        if len(field) == 2 and (
            any(k not in plurals for k in new) or any(v != "ok" for v in new.values())
        ):
            return True

        # annotation existed before
        if len(field) == 3 and (field[2] not in plurals or new != "ok"):
            return True

    return False


def not_handled_configuration(resource: dict) -> bool:
    """Return True if the resource hasn't been handled by kopf yet, False otherwise.

    Args:
        resource (dict): K8s resource as a dict.

    Returns:
        bool: Resource hasn't been handled by kopf.
    """
    return (
        LAST_HANDLED_ANNOTATION in resource["metadata"].get("annotations", {})
        and json.loads(resource["metadata"]["annotations"][LAST_HANDLED_ANNOTATION])[
            "spec"
        ]
        != resource["spec"]
    )


def patch_annotation(
    k8s: Kubernetes,
    key: str,
    value: str,
    plural: str,
    name: str,
    namespace: str,
) -> None:
    """Patch resource annotation.

    Args:
        k8s (Kubernetes): Kubernetes utils instance.
        key (str): Annotation key.
        value (str): Annotation value.
        plural (str): Resource plural.
        name (str): Resource name.
        namespace (str): Resource namespace.

    """
    resource = k8s.get_cr(
        namespace=namespace,
        plural=plural,
        name=name,
    )
    resource["metadata"].setdefault("annotations", {})[key] = value
    k8s.patch_cr(
        namespace=namespace,
        plural=plural,
        name=name,
        body=resource,
    )


def has_owner_reference(meta, **_):
    """Evaluate if resource contains owner reference."""
    return meta.get("ownerReferences") is not None


def valid_annotations(diff, **_):
    """Evaluate if resource annotations are valid."""
    version = "yaook-k8s-version"
    meta_path = ("metadata",)
    anno_path = meta_path + ("annotations",)
    for _, field, old, new in diff:
        if old is None and field in (meta_path, anno_path):
            continue
        if (
            field[:2] == anno_path
            and (field[2] not in version or (field[2] in version and new != "{}"))
        ) or field[:2] != anno_path:
            return True
    return False


def validate_and_update_terraform_config(
    terraform_config: dict,
    flavors: Dict[str, Flavor],
    available_quotas: Quota,
    cluster: YaookClusterResource,
    control_plane: YaookControlPlaneResource,
    machine_deployments: List[YaookMachineDeploymentResource],
):
    """Validate the quota and update terraform config.

    OpenStack's quotas are validated before workload cluster creation/update which
    prevents the OpenStack project system capacities from being exhausted.
    Validation implements a "best-effort" strategy. It means, that `k8s-capi-provider`
    allows the creation of some workload resources up to quota limits.

    This function validates the following OpenStack's quotas:
    - Number of server groups per project
    - Number of floating IP addresses allowed per project
    - Number of instances allowed per project
    - Number of instance cores (VCPUs) allowed per project
    - Megabytes of instance RAM allowed per project

    The validation strategy with examples:

    The Yaook workload cluster (usually) consists of multiple gateways, a control plane
    and worker nodes. This function validates the quotas in the following order:
    1. Gateways
      - Validate whether the OpenStack project contains enough resources to
        scale gateways.

      !Attention!: Gateway is considered a resource that **should** be scaled to the
      desired state. If there is an attempt to scale gateway nodes over the quota
      limits the `k8s-capi-provider` does not allow any scaling of gateways nodes
      at all (best-effort strategy is not allowed here). If it happens during
      cluster creation, it raises a :class:`ProviderPermanentError`
      with "Quota exceeded" error message.

      TODO: Gateway's best-effort strategy is discussed
       in yaook/incubator/k8s-capi-provider#72

      Example:
        a) Existing cluster contains **1** gateway node
        b) Current OpenStack number of instances allowed is **1**
        c) There is an attempt to scale gateways to **3** nodes
        d) `k8s-capi-provider` does not allow any scaling of gateways nodes

    2. Control plane nodes
      - Validate whether the OpenStack project contains enough resources to
        scale the control plane.

      !Attention!: Control plane is considered a resource that **should** be scaled to
      the desired state. If there is an attempt to scale control plane nodes over the
      quota limits the `k8s-capi-provider` does not allow any scaling of control plane
      nodes at all (best-effort strategy is not allowed here). If it happens during
      cluster creation, it raises a :class:`ProviderPermanentError`
      with "Quota exceeded" error message.

      The "best-effort" strategy is not implemented for the control plane, because
      the number of control plane nodes should be uneven (1,3,5, ...). From the etcd's
      raft protocol (and its failure tolerance) point of view, it does not make sense
      to have some non-desired number of control plane nodes.

      Example:
        a) Existing cluster contains **1** control plane node
        b) Current OpenStack number of instances allowed is **1**
        c) There is an attempt to scale the control plane to **3** nodes
        d) `k8s-capi-provider` does not allow any scaling of control plane nodes

    3. Worker nodes
      - Validate whether the OpenStack project contains enough resources to scale
        worker nodes.

      The "best-effort" strategy is implemented here. If there is an attempt to scale
      worker nodes over the quota limits, the `k8s-capi-provider` calculates the
      maximum number of replicas, which do not exceed quotas.

      Example:
        a) Existing cluster contains **1** worker node
        b) Current OpenStack number of instances allowed is **1**
        c) There is an attempt to scale the worker nodes to **3**
        d) `k8s-capi-provider` scales worker nodes to **2**

    Args:
        terraform_config (dict): Terraform config to be modified.
        flavors (Dict[str, Flavor]): OpenStack flavors.
        available_quotas (Quota): OpenStack available quotas.
        cluster (YaookClusterResource): YaookCluster resource.
        control_plane (YaookControlPlaneResource): YaookControlPlane resource.
        machine_deployments (List[YaookMachineDeploymentResource]):
         List of YaookMachineDeployment resources.

    Raises:
        ProviderPermanentError: Quota exceeded.

    """
    # server groups
    if (
        desired_server_groups := cluster.get_server_groups()
    ) - cluster.get_server_groups(current=True) > available_quotas.server_groups:
        raise ProviderPermanentError(
            reason=ProviderPermanentErrorReason.QUOTA_EXCEEDED_ERROR.name,
            message="Server groups quota exceeded. "
            f"You have requested {desired_server_groups} server groups and "
            f"there is {available_quotas.server_groups} available.",
        )

    # gateways
    current_gateway = cluster.get_gateway_quota(flavors, current=True)
    diff_gateway = cluster.get_gateway_quota(flavors) - current_gateway
    if all(d <= 0 for d in diff_gateway):
        # increase the available quota
        available_quotas -= diff_gateway

    # masters
    current_masters = control_plane.get_quota(flavors, current=True)
    diff_masters = control_plane.get_quota(flavors) - current_masters
    if all(d <= 0 for d in diff_masters):
        # increase the available quota
        available_quotas -= diff_masters

    # workers
    current_workers = Quota()
    for md in machine_deployments:
        current_md = md.get_quota(flavors, current=True)
        diff_md = md.get_quota(flavors) - current_md
        if all(d <= 0 for d in diff_md):
            # increase the available quota
            available_quotas -= diff_md

        current_workers += Quota(
            instances=current_md.instances, ram=current_md.ram, vcpus=current_md.vcpus
        )

    # find how many workers were deleted, so we can increase the quota
    # deleted_workers = current_instances - current_(gateways + masters + workers)
    available_quotas += Quota(
        instances=cluster.body.get("status", {})
        .get("openstack", {})
        .get("instances", 0)
        - current_gateway.instances
        - current_masters.instances
        - current_workers.instances,
        ram=cluster.body.get("status", {}).get("openstack", {}).get("ram", 0)
        - current_gateway.ram
        - current_masters.ram
        - current_workers.ram,
        vcpus=cluster.body.get("status", {}).get("openstack", {}).get("vcpus", 0)
        - current_gateway.vcpus
        - current_masters.vcpus
        - current_workers.vcpus,
    )

    # at this point, all increasing quotas have been done
    # move on decreasing

    # gateways
    if not all(d <= 0 for d in diff_gateway):
        # if there is quota problem with gateways
        if diff_gateway > available_quotas:
            if not current_gateway.instances:
                raise ProviderPermanentError(
                    reason=ProviderPermanentErrorReason.QUOTA_EXCEEDED_ERROR.name,
                    message="Gateways quota exceeded. "
                    f"You have requested {diff_gateway} quota and "
                    f"there is {available_quotas} quota available.",
                )
            else:
                # go with current values
                terraform_config.update(cluster.get_gateway_config(current=True))
        else:
            # if there is enough quota for desired gateways, decrease the quota
            available_quotas -= diff_gateway

    # masters
    terraform_config.update(control_plane.get_terraform_config())
    if not all(d <= 0 for d in diff_masters):
        # if there is quota problem with masters
        if diff_masters > available_quotas:
            if not current_masters.instances:
                raise ProviderPermanentError(
                    reason=ProviderPermanentErrorReason.QUOTA_EXCEEDED_ERROR.name,
                    message="Masters quota exceeded. "
                    f"You have requested {diff_masters} quota and "
                    f"there is {available_quotas} quota available.",
                )
            else:
                # go with current values
                terraform_config.update(
                    control_plane.get_terraform_config(current=True)
                )
        else:
            # if there is enough quota for desired masters, decrease the quota
            available_quotas -= diff_masters

    # workers
    YaookClusterResource.reset_workers_terraform_config(terraform_config)
    for md in machine_deployments:
        current_md = md.get_quota(flavors, current=True)
        diff_md = (desired_md := md.get_quota(flavors)) - current_md
        replicas = desired_md.instances
        if not all(d <= 0 for d in diff_md):
            # if there is quota problem with workers
            if diff_md > available_quotas:
                # calculate the largest possible number of workers
                replicas = md.calculate_max_number_of_workers(flavors, available_quotas)
                diff_md = md.get_quota(flavors, replicas=replicas) - current_md
            # decrease the quota
            available_quotas -= diff_md

        cluster.update_workers_terraform_config(terraform_config, md, replicas)


async def reconcile_resource(
    body: kopf.Body,
    logger: kopf.Logger,
    reason: Union[kopf.Reason, ResourceAction],
    **kwargs,
) -> None:
    """Reconcile cluster resource.

    Args:
        body (kopf.Body): Resource body.
        logger (kopf.Logger): Kopf logger.
        reason (Union[kopf.Reason, ResourceAction]): Action reason.
        kwargs (dict): All options given by the kopf.

    """
    kubernetes = Kubernetes(logger=logger)
    cluster = YaookClusterResource.from_kopf(body=body, kubernetes=kubernetes)
    logger.info(
        f"{reason} of cluster {cluster.name} in {cluster.namespace} namespace started"
    )

    if reason == ResourceAction.UPDATE and cluster.is_reconciling():
        raise ProviderTemporaryError(
            reason=ProviderTemporaryErrorReason.WAITING_FOR_RECONCILE.name,
            message=ProviderTemporaryErrorReason.WAITING_FOR_RECONCILE.value,
            delay=SETTINGS.cluster_ctrl.reconcile_wait_for,
        )

    openstack = OpenStack(cluster.get_openstack_rc(), logger=logger)

    config = cluster.get_config()

    deps = cluster.get_dependencies()

    # YaookMachineDeployment
    for md in deps.machine_deployments:
        md.set_owner(cluster.body)
        # YaookMachineDeployment --> YaookMachineTemplate
        mt = md.get_dependencies().machine_template
        if reason in (ResourceAction.CREATE, ResourceAction.UPDATE):

            # if the machine deployment was just created but not yet handled
            # we need to force not handled condition here because
            # on.create(md) handler will notify the cluster later
            md.body["metadata"].setdefault("annotations", {}).setdefault(
                LAST_HANDLED_ANNOTATION, '{"spec":{}}'
            )

            if not_handled_configuration(mt.body) or not_handled_configuration(md.body):
                raise_not_handled_resource(CRDS.machine_deployment_plural, md.name)

    # YaookControlPlane
    control_plane = deps.control_plane
    control_plane.set_owner(cluster.body)

    # YaookControlPlane --> YaookMachineTemplate
    control_plane_machine_template = control_plane.get_dependencies().machine_template

    # YaookControlPlane --> YaookConfigTemplate
    control_plane_config_template = control_plane.get_dependencies().config_template

    if reason == ResourceAction.UPDATE and (
        not_handled_configuration(control_plane_config_template.body)
        or not_handled_configuration(control_plane_machine_template.body)
        or not_handled_configuration(control_plane.body)
    ):
        raise_not_handled_resource(CRDS.control_plane_plural, control_plane.name)

    cluster_from_k8s = YaookClusterResource.from_k8s(
        name=cluster.name, namespace=cluster.namespace, kubernetes=kubernetes
    )
    for plural in (
        CRDS.control_plane_plural,
        CRDS.machine_deployment_plural,
    ):
        if (
            cluster_from_k8s.body["metadata"].get("annotations", {}).get(plural, "ok")
            != "ok"
        ):
            patch_annotation(
                kubernetes,
                plural,
                "ok",
                CRDS.cluster_plural,
                cluster.name,
                cluster.namespace,
            )

    config.update(control_plane_config_template.get_config())
    validate_and_update_terraform_config(
        config["terraform"],
        openstack.flavors,
        openstack.get_available_quotas(),
        cluster,
        control_plane,
        deps.machine_deployments,
    )

    version, version_annotation = None, None
    annotations = control_plane_config_template.body["metadata"].get("annotations", {})
    if "yaook-k8s-version" in annotations:
        version_annotation = json.loads(annotations["yaook-k8s-version"])
        if version_annotation:
            config["kubernetes"]["version"] = version_annotation["current"]

    if os.environ.get("ENVIRONMENT") == "kubernetes":
        namespace_name = "-".join((cluster.namespace, cluster.name))
        ip_netns = f"ip netns exec {namespace_name} "
        if reason in (
            ResourceAction.CREATE,
            ResourceAction.RESUME,
        ) and not os.path.exists(f"/var/run/netns/{namespace_name}"):
            wg_ip_cidr = IPv4Network(cluster.obj.spec.wireguard.ip_cidr)
            tf_subnet_cidr = IPv4Network(cluster.obj.spec.terraform.subnet_cidr)
            # In the worst case scenario (when each user/cluster pick 10.0.0.0/8 and
            # 172.16.0.0/12 networks) we should be able to create 2^14 - 1 different
            # network namespaces from 192.168.0.0/16 (default subnet_mask is /30)
            await create_network_namespace(
                namespace_name,
                [wg_ip_cidr, tf_subnet_cidr] + get_all_local_networks(),
            )
    else:
        ip_netns = ""

    for item in (
        YaookClusterStage.init,
        YaookClusterStage.terraform,
        YaookClusterStage.stage2,
        YaookClusterStage.stage3,
        YaookClusterStage.upgrade,
    ):
        stage = YaookClusterStage(
            item
        )  # We want to be exact in for loop, hence we instantiate stage again.

        cmd_base_path = SETTINGS.cluster_ctrl.path_managed
        cmd_namespace: Optional[str] = ip_netns

        if stage == YaookClusterStage.init:
            cmd_base_path = SETTINGS.yaook_git.directory.absolute()
            cmd_namespace = None

        if stage in (YaookClusterStage.terraform, YaookClusterStage.stage2):
            cmd_namespace = None

        if stage == YaookClusterStage.upgrade:
            if not version_annotation:
                continue

            current_version = version_annotation["current"]
            new_version = version_annotation["new"]
            if current_version == new_version:
                continue

            version = new_version

            logger.info(
                f"upgrade of kubernetes version from "
                f"{current_version} to {new_version} started"
            )

        if status_before := stage.status_before(ResourceAction(reason)):
            cluster.set_status(status=status_before.dict(exclude_none=True))

        await execute(
            cmd=stage.path(
                cmd_base_path, namespace=cmd_namespace, version=version
            ).split(),
            cluster_name=cluster.name,
            cluster_namespace=cluster.namespace,
            config=copy.deepcopy(config),
            lcm=cluster.get_lcm(),
            action=ResourceAction(reason),
            kubernetes=kubernetes,
            settings_ctrl=SETTINGS.cluster_ctrl,
            openstack=openstack,
            adopt_state=True,  # YaookCluster resource owns state resources.
            managed_provider_identity=cluster.managed_provider_identity,
        )

        if status_after := stage.status_after(ResourceAction(reason)):
            cluster.set_status(status=status_after.dict(exclude_none=True))

        if stage == YaookClusterStage.terraform:
            cluster.set_workers_status(config["terraform"], deps.machine_deployments)
            control_plane.set_masters_status(config["terraform"])
            cluster.set_overall_openstack_cluster_status(
                config["terraform"], openstack.flavors
            )

    if version:
        patch_annotation(
            kubernetes,
            "yaook-k8s-version",
            "{}",
            CRDS.config_template_plural,
            control_plane_config_template.name,
            cluster.namespace,
        )

    logger.info(
        f"{reason} of cluster {cluster.name} in {cluster.namespace} namespace finished"
    )

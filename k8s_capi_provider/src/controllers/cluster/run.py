"""This module defines cluster controller handlers."""
import asyncio
import json
import os
import shutil
import time
from configparser import NoSectionError
from typing import Any

import git
import kopf
from git.exc import GitCommandError
from kubernetes.client.rest import ApiException
from novaclient.exceptions import ClientException as NovaClientException

from ...data.base import ResourceAction, ResourcePhase
from ...data.cluster import YaookClusterResource, YaookClusterStage
from ...data.service import YaookServiceResource
from ...exceptions import ProviderPermanentError
from ...models.yaookcluster import Status as YaookClusterStatus
from ...settings import CRD, ProviderSettings, get_settings
from ...utils import logging
from ...utils.common import (
    cancel_tasks,
    error_status,
    raise_not_handled_resource,
    wait_for_dependencies,
)
from ...utils.kubernetes import Kubernetes
from ...utils.network import delete_network_namespace
from ...utils.openstack import OpenStack
from ...utils.reconcile import execute, set_controller
from .reconcile import (
    has_owner_reference,
    is_updatable,
    not_handled_configuration,
    patch_annotation,
    reconcile_resource,
    valid_annotations,
)

SETTINGS: ProviderSettings = get_settings()
CRDS: CRD = SETTINGS.crd


# ~~~~~~~ Cluster controller handlers ~~~~~~~


@kopf.on.startup()
def on_startup(settings: kopf.OperatorSettings, logger: kopf.Logger, **_):
    """Set controller on startup.

    Args:
        settings (kopf.OperatorSettings):  Kopf specific controller settings.
        logger (kopf.Logger): Kopf logger.

    """
    set_controller(
        settings_kopf=settings, settings_ctrl=SETTINGS.cluster_ctrl, logger=logger
    )


@kopf.on.daemon(
    CRDS.cluster_plural,
)
async def on_cancellation(name: str, namespace: str, logger: kopf.Logger, **_):
    """Cancel all running/pending cluster resource related tasks.

    Any long-running resource operation is canceled here when
    the deletion of the resource is requested. It should be done that way because
    kopf processes resource-specific events sequentially.
    This caused that the `delete` handler has to wait for the long-running
    creation/update/resume/reconcile tasks.
    See: https://github.com/zalando-incubator/kopf/issues/94#issuecomment-497989915

    Note:
        :func:`on_delete` handler may run 2 times, if the
        :func:`on_reconcile` handler is in progress and cancelled
        by :func:`on_cancellation`. It is caused by kopf.on.timer
        implementation. Kopf tries to exit all timers
        gracefully and periodically (`cancellation_polling` interval
        (15s in our case)) pools their status. As we cancel reconciliation
        task, there is one additional attempt to check timer status,
        which causes the :func:`on_delete` handler is executed 2nd time.

    Args:
        name (str): Resource name.
        namespace (str): Resource namespace.
        logger (kopf.Logger): Kopf logger.

    """
    kubernetes = Kubernetes(logger=logger)
    while True:
        try:
            cluster = YaookClusterResource.from_k8s(
                name=name, namespace=namespace, kubernetes=kubernetes
            )
        except ApiException as e:
            if e.status == 404:
                await cancel_tasks(prefix=f"{namespace}/{name}", logger=logger)
                break

            raise

        else:
            if cluster.is_deleting():
                await cancel_tasks(prefix=f"{namespace}/{name}", logger=logger)
                break

        await asyncio.sleep(SETTINGS.cluster_ctrl.cancellation_polling)


@kopf.on.timer(
    CRDS.cluster_plural,
    interval=SETTINGS.cluster_ctrl.reconcile_interval,
    idle=SETTINGS.cluster_ctrl.reconcile_idle,
)
@error_status(resource_status=YaookClusterStatus)
@wait_for_dependencies(
    resource_status=YaookClusterStatus,
    delay=SETTINGS.cluster_ctrl.dependencies_wait_for,
)
async def on_timer(body: kopf.Body, logger: kopf.Logger, **kwargs):
    """Execute reconciliation periodically.

    Schedules regular handler execution as long as the resource exist.

    Args:
        body (kopf.Body): Resource body.
        logger (kopf.Logger): Kopf logger.
        kwargs (dict): All options given by the kopf.

    """
    status = kwargs.get("status", {})
    if status.get("phase") in (
        ResourcePhase.RUNNING,
        ResourcePhase.FAILED,
    ):
        task = asyncio.create_task(
            reconcile_resource(
                body=body,
                logger=logger,
                reason=ResourceAction.RECONCILE,
                **kwargs,
            ),
            name=f"{body['metadata']['namespace']}/"
            f"{body['metadata']['name']}"
            f"_{ResourceAction.RECONCILE}",
        )

        try:
            await task
        except asyncio.exceptions.CancelledError:
            logger.info("Cluster reconciliation handler has been cancelled.")
            # Kopf expects that CancelledError is re-raised
            # in timers. Then the timer could be gracefully stopped.
            raise

    else:
        logger.info(
            f"Reconciliation of resource {body['metadata']['name']}"
            f" in namespace {body['metadata']['namespace']} "
            f"skipped. Resource is not in the {ResourcePhase.RUNNING} "
            f"or {ResourcePhase.FAILED} state."
        )


@kopf.on.update(CRDS.cluster_plural, when=is_updatable)
@kopf.on.resume(CRDS.cluster_plural)
@kopf.on.create(CRDS.cluster_plural)
@error_status(resource_status=YaookClusterStatus)
@wait_for_dependencies(
    resource_status=YaookClusterStatus,
    delay=SETTINGS.cluster_ctrl.dependencies_wait_for,
)
@logging.register(settings=SETTINGS.cluster_ctrl)
async def on_action(
    body: kopf.Body,
    logger: kopf.Logger,
    reason: kopf.Reason,
    **kwargs,
):
    """Handle cluster resource creation, update and resume.

    Args:
        body (kopf.Body): Resource body.
        logger (kopf.Logger): Kopf logger.
        reason (kopf.Reason): Kopf action reason.
        kwargs (dict): All options given by the kopf.

    """
    task = asyncio.create_task(
        reconcile_resource(
            body=body,
            logger=logger,
            reason=reason,
            **kwargs,
        ),
        name=f"{body['metadata']['namespace']}/"
        f"{body['metadata']['name']}"
        f"_{reason}",
    )
    try:
        await task
    except asyncio.exceptions.CancelledError:
        logger.info(f"Cluster {reason} handler has been cancelled.")
        # We expect that CancelledError is raised when the termination of
        # the resource is requested. Therefore, we ignore it here to let
        # the `delete` handler does its job.
        pass


@kopf.on.delete(CRDS.cluster_plural)
@error_status(resource_status=YaookClusterStatus)
@wait_for_dependencies(
    resource_status=YaookClusterStatus,
    delay=SETTINGS.cluster_ctrl.dependencies_wait_for,
)
@logging.unregister(settings=SETTINGS.cluster_ctrl)
async def on_delete(
    body: kopf.Body,
    logger: kopf.Logger,
    reason: kopf.Reason,
    **kwargs,  # Decorators expect it here.
):
    """Handle cluster resource deletion.

    Args:
        body (kopf.Body): Resource body.
        logger (kopf.Logger): Kopf logger.
        reason (kopf.Reason): Kopf action reason.
        kwargs (dict): All options given by the kopf.

    """
    kubernetes = Kubernetes(logger=logger)
    cluster = YaookClusterResource.from_kopf(body=body, kubernetes=kubernetes)

    logger.info(
        f"{reason} of cluster {cluster.name} in {cluster.namespace} namespace started"
    )

    openstack = OpenStack(cluster.get_openstack_rc(), logger=logger)

    # Remove OS keypair
    for keypair in (
        "-".join((cluster.name, SETTINGS.ssh.provider.keypair)),
        "-".join((cluster.name, SETTINGS.ssh.user.keypair)),
        "-".join((cluster.name, SETTINGS.ssh.keypair_merged)),
    ):
        try:
            openstack.remove_keypair(keypair)
        except NovaClientException:
            pass

    # Remove cluster from OS
    for item in (YaookClusterStage.init, YaookClusterStage.delete):

        stage = YaookClusterStage(item)

        cmd_base_path = SETTINGS.cluster_ctrl.path_managed
        if stage in (YaookClusterStage.init,):
            cmd_base_path = SETTINGS.yaook_git.directory.absolute()

        await execute(
            cmd=stage.path(cmd_base_path).split(),
            cluster_name=cluster.name,
            cluster_namespace=cluster.namespace,
            lcm=cluster.get_lcm(),
            action=ResourceAction(reason),
            kubernetes=kubernetes,
            settings_ctrl=SETTINGS.cluster_ctrl,
            openstack=openstack,
            adopt_state=True,  # YaookCluster resource owns state resources.
        )

    # Remove network namespace
    if os.environ.get("ENVIRONMENT") == "kubernetes":
        namespace_name = "-".join((cluster.namespace, cluster.name))
        try:
            await delete_network_namespace(namespace_name)
        except ProviderPermanentError:
            pass

    cluster_path = SETTINGS.cluster_ctrl.path / cluster.namespace / cluster.name

    # Remove git submodule
    repo_root = git.Repo()
    try:
        repo_root.submodule(f"{cluster_path}/managed-k8s").remove()
        repo_root.git.restore(cluster_path, staged=True)
    except (NoSectionError, GitCommandError, ValueError):
        pass
    shutil.rmtree(f".git/modules/{cluster_path}", ignore_errors=True)

    # Remove cluster files
    shutil.rmtree(cluster_path, ignore_errors=True)

    logger.info(
        f"{reason} of cluster {cluster.name} in {cluster.namespace} namespace finished"
    )


@kopf.on.update(  # type: ignore[arg-type]
    CRDS.config_template_plural,
    when=kopf.all_(  # type: ignore[type-var, arg-type]
        [has_owner_reference, valid_annotations]  # type: ignore[list-item]
    ),
    param=CRDS.config_template_plural,
)
@kopf.on.update(
    CRDS.machine_template_plural,
    when=has_owner_reference,
    param=CRDS.machine_template_plural,
)
async def update_templates(
    name: str, namespace: str, meta: dict, param: str, logger: kopf.Logger, **kwargs
):
    """Handle update of machine and config templates.

    Args:
        name (str): Resource name.
        namespace (str): Resource namespace.
        meta (dict): Resource metadata.
        param (str): Parameter passed from handler options.
        logger (kopf.Logger): Kopf logger.
        kwargs (dict): All options given by the kopf.

    """
    kubernetes = Kubernetes(logger=logger)

    for parent in meta["ownerReferences"]:
        if not parent["controller"]:
            plural = CRDS.machine_deployment_plural
        else:
            plural = CRDS.control_plane_plural
            for _, field, old, new in kwargs["diff"]:
                if field == ("spec", "kubernetes", "version"):
                    version = {"current": old, "new": new}
                    patch_annotation(
                        kubernetes,
                        "yaook-k8s-version",
                        json.dumps(version),
                        param,
                        name,
                        namespace,
                    )

        patch_annotation(
            kubernetes,
            param,
            name + "_modified_" + str(time.time_ns()),
            plural,
            parent["name"],
            namespace,
        )


@kopf.on.update(  # type: ignore[arg-type]
    CRDS.control_plane_plural,
    when=kopf.all_(  # type: ignore[type-var, arg-type]
        [has_owner_reference, is_updatable]  # type: ignore[list-item]
    ),
    param=CRDS.control_plane_plural,
)
@kopf.on.update(
    CRDS.machine_deployment_plural,
    when=kopf.all_(  # type: ignore[type-var, arg-type]
        [has_owner_reference, is_updatable]  # type: ignore[list-item]
    ),
    param=CRDS.machine_deployment_plural,
)
async def update_nodes(
    name: str,
    namespace: str,
    meta: dict,
    spec: dict,
    param: str,
    logger: kopf.Logger,
    **_,
):
    """Handle update of machine and control plane nodes.

    Args:
        name (str): Resource name.
        param (str): Parameter passed from handler options.
        namespace (str): Resource namespace.
        meta (dict): Resource metadata.
        spec (dict): Resource spec.
        logger (kopf.Logger): Kopf logger.

    """
    kubernetes = Kubernetes(logger=logger)

    if param == CRDS.control_plane_plural:
        machine_template_ref = spec["machineTemplate"]["infrastructureRef"]
        bootstrap_ref = spec["bootstrap"]["configRef"]
    else:
        machine_template_ref = spec["template"]["spec"]["infrastructureRef"]
        bootstrap_ref = spec["template"]["spec"]["bootstrap"]["configRef"]

    for plural, ref in zip(
        (CRDS.machine_template_plural, CRDS.config_template_plural),
        (machine_template_ref, bootstrap_ref),
    ):
        cr = kubernetes.get_cr(
            namespace=namespace,
            plural=plural,
            name=ref["name"],
        )
        if not_handled_configuration(cr):
            raise_not_handled_resource(plural, ref["name"])

        if meta.get("annotations", {}).get(plural, "ok") != "ok":
            patch_annotation(
                kubernetes,
                plural,
                "ok",
                param,
                name,
                namespace,
            )

    cluster = meta["ownerReferences"][0]  # only cluster is owner
    patch_annotation(
        kubernetes,
        param,
        name + "_modified_" + str(time.time_ns()),
        CRDS.cluster_plural,
        cluster["name"],
        namespace,
    )


@kopf.on.create(  # type: ignore[arg-type]
    CRDS.control_plane_plural, param=CRDS.control_plane_plural
)
@kopf.on.create(CRDS.machine_deployment_plural, param=CRDS.machine_deployment_plural)
@wait_for_dependencies(
    delay=SETTINGS.cluster_ctrl.dependencies_wait_for,
)
async def create_nodes(
    name: str,
    namespace: str,
    param: str,
    spec: dict,
    logger: kopf.Logger,
    **_,
):
    """Handle creation of machine and control plane nodes.

    Args:
        name (str): Resource name.
        namespace (str): Resource namespace.
        param (str): Parameter passed from handler options.
        spec (dict): Resource spec.
        logger (kopf.Logger): Kopf logger.

    """
    kubernetes = Kubernetes(logger=logger)

    if param == CRDS.control_plane_plural:
        controller = True
        machine_template_ref = spec["machineTemplate"]["infrastructureRef"]
        bootstrap_ref = spec["bootstrap"]["configRef"]
    else:
        controller = False
        machine_template_ref = spec["template"]["spec"]["infrastructureRef"]
        bootstrap_ref = spec["template"]["spec"]["bootstrap"]["configRef"]

    for plural, ref in zip(
        (CRDS.machine_template_plural, CRDS.config_template_plural),
        (machine_template_ref, bootstrap_ref),
    ):
        cr = kubernetes.get_cr(
            namespace=namespace,
            plural=plural,
            name=ref["name"],
        )
        kopf.append_owner_reference(cr, controller=controller)
        kubernetes.patch_cr(
            namespace=namespace,
            plural=plural,
            name=ref["name"],
            body=cr,
        )

    if param == CRDS.machine_deployment_plural:
        # New deployment, we need to inform cluster
        patch_annotation(
            kubernetes,
            param,
            name + "_created",
            CRDS.cluster_plural,
            spec["clusterName"],
            namespace,
        )


@kopf.on.delete(CRDS.machine_deployment_plural)
async def delete_machine_deployment(
    name: str, namespace: str, spec: dict, logger: kopf.Logger, **_
):
    """Handle machine nodes deletion.

    Cluster resource needs to be notified when machine deployment is deleted.

    Args:
        name (str): Resource name.
        namespace (str): Resource namespace.
        spec (dict): Resource spec.
        logger (kopf.Logger): Kopf logger.

    """
    try:
        patch_annotation(
            Kubernetes(logger=logger),
            CRDS.machine_deployment_plural,
            name + "_deleted",
            CRDS.cluster_plural,
            spec["clusterName"],
            namespace,
        )
    except ApiException:
        pass


@kopf.on.field(
    CRDS.service_plural,
    field="status.phase",
    new=kopf.PRESENT,
)
@kopf.on.delete(CRDS.service_plural)
async def update_service_ready(
    body: kopf.Body, new: Any, reason: kopf.Reason, logger: kopf.Logger, **_
):
    """Update cluster status serviceReady field according to the service phase.

    Handler updates cluster resource `status.serviceReady` field to `True` when
    the corresponding service resource is processed by the service controller and
    its `status.phase` is `running`.

    Handler updates cluster resource `status.serviceReady` field to `False` when
    the corresponding service resource is processed by the service controller and
    its `status.phase` is in `failed` or `pending`.
    Handler updates cluster resource `status.serviceReady` field to `False` also
    when the service resource is deleted at all. Unfortunately, we have to detect
    delete event instead of  service `status.phase` transition to `terminating`,
    because we can not ensure that `status.phase` transition will be handled before
    deletion.

    Args:
        body (kopf.Body): Resource body.
        new (Any): New state of the object or a field within the detected changes.
        logger (kopf.Logger): Kopf logger.
        reason (kopf.Reason): Kopf action reason.

    """
    kubernetes = Kubernetes(logger=logger)
    service = YaookServiceResource.from_kopf(kubernetes=kubernetes, body=body)

    if new == ResourcePhase.RUNNING:
        service_ready = True

    elif (
        new
        in (
            ResourcePhase.FAILED,
            ResourcePhase.PENDING,
            ResourcePhase.TERMINATING,
        )
        # The service resource is going to be terminated, but the delete
        # event was triggered first.
        or reason == ResourceAction.DELETE
    ):
        service_ready = False

    else:
        logger.info("The service is in non-transition state. Nothing to do.")
        return

    try:
        deps = service.get_dependencies()
        deps.cluster.set_status(
            status=YaookClusterStatus(serviceReady=service_ready).dict(
                exclude_none=True
            ),
        )
    except ApiException as e:
        if e.status == 404:
            logger.info("The entire cluster seems to be removed. Nothing to do.")
            return

        raise

    logger.info(
        f"Cluster {deps.cluster.name} in {deps.cluster.namespace}"
        f" namespace changed serviceReady status to {service_ready}."
    )

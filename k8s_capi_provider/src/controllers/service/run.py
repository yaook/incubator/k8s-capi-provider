"""This module defines service controller handlers."""
import asyncio
import os
import shutil
from configparser import NoSectionError

import git
import kopf
from git import GitCommandError
from kubernetes.client.rest import ApiException

from ...data.base import ResourceAction, ResourcePhase
from ...data.service import YaookServiceResource
from ...exceptions import ProviderPermanentError
from ...models.yaookservice import Status as YaookServiceStatus
from ...settings import CRD, ProviderSettings, get_settings
from ...utils import logging
from ...utils.common import cancel_tasks, error_status
from ...utils.kubernetes import Kubernetes
from ...utils.network import delete_network_namespace
from ...utils.reconcile import set_controller
from .reconcile import reconcile_resource

SETTINGS: ProviderSettings = get_settings()
CRDS: CRD = SETTINGS.crd


# ~~~~~~~ Service controller handlers ~~~~~~~


@kopf.on.startup()
def on_startup(settings: kopf.OperatorSettings, logger: kopf.Logger, **_):
    """Set controller on startup.

    Args:
        settings (kopf.OperatorSettings):  Kopf specific controller settings.
        logger (kopf.Logger): Kopf logger.

    """
    set_controller(
        settings_kopf=settings, settings_ctrl=SETTINGS.service_ctrl, logger=logger
    )


@kopf.on.daemon(
    CRDS.service_plural,
)
async def on_cancellation(name: str, namespace: str, logger: kopf.Logger, **_):
    """Cancel all running/pending service resource related tasks.

    Any long-running resource operation is canceled here when
    the deletion of the resource is requested. It should be done that way because
    kopf processes resource-specific events sequentially.
    This caused that the `delete` handler has to wait for the long-running
    creation/update/resume/reconcile tasks.
    See: https://github.com/zalando-incubator/kopf/issues/94#issuecomment-497989915

    Note:
        :func:`on_delete` handler may run 2 times, if the
        :func:`on_reconcile` handler is in progress and cancelled
        by :func:`on_cancellation`. It is caused by kopf.on.timer
        implementation. Kopf tries to exit all timers
        gracefully and periodically (`cancellation_polling` interval
        (15s in our case)) pools their status. As we cancel reconciliation
        task, there is one additional attempt to check timer status,
        which causes the :func:`on_delete` handler is executed 2nd time.

    Args:
        name (str): Resource name.
        namespace (str): Resource namespace.
        logger (kopf.Logger): Kopf logger.

    """
    kubernetes = Kubernetes(logger=logger)
    while True:
        try:
            service = YaookServiceResource.from_k8s(
                name=name, namespace=namespace, kubernetes=kubernetes
            )
        except ApiException as e:
            if e.status == 404:
                await cancel_tasks(prefix=f"{namespace}/{name}", logger=logger)
                break

            raise

        else:
            if service.is_deleting():
                await cancel_tasks(prefix=f"{namespace}/{name}", logger=logger)
                break

        await asyncio.sleep(SETTINGS.service_ctrl.cancellation_polling)


@kopf.on.timer(
    CRDS.service_plural,
    interval=SETTINGS.service_ctrl.reconcile_interval,
    idle=SETTINGS.service_ctrl.reconcile_idle,
)
@error_status(resource_status=YaookServiceStatus)
async def on_timer(body: kopf.Body, logger: kopf.Logger, **kwargs):
    """Execute reconciliation periodically.

    Schedules regular handler execution as long as the resource exist.
    Reconciliation is scheduled as a named tasks which could be
    canceled if needed.

    Args:
        body (kopf.Body): Resource body.
        logger (kopf.Logger): Kopf logger.
        kwargs (dict): All options given by the kopf.

    """
    status = kwargs.get("status", {})
    if status.get("phase") in (
        ResourcePhase.RUNNING,
        ResourcePhase.FAILED,
    ):
        task = asyncio.create_task(
            reconcile_resource(
                body=body,
                logger=logger,
                reason=ResourceAction.RECONCILE,
            ),
            name=f"{body['metadata']['namespace']}/"
            f"{body['metadata']['name']}"
            f"_{ResourceAction.RECONCILE}",
        )
        try:
            await task
        except asyncio.exceptions.CancelledError:
            logger.info("Service reconciliation handler has been cancelled.")
            # Kopf expects that CancelledError is re-raised
            # in timers. Then the timer could be gracefully stopped.
            raise

    else:
        logger.info(
            f"Reconciliation of resource {body['metadata']['name']} in"
            f" namespace {body['metadata']['namespace']} "
            f"skipped. Resource is not in the {ResourcePhase.RUNNING} "
            f"or {ResourcePhase.FAILED} state."
        )


@kopf.on.update(CRDS.service_plural)
@kopf.on.resume(CRDS.service_plural)
@kopf.on.create(CRDS.service_plural)
@error_status(resource_status=YaookServiceStatus)
@logging.register(settings=SETTINGS.service_ctrl)
async def on_action(
    body: kopf.Body,
    logger: kopf.Logger,
    reason: kopf.Reason,
    **kwargs,  # Decorators expect it here.
):
    """Handle service resource creation, update and resume.

    Args:
        body (kopf.Body): Resource body.
        logger (kopf.Logger): Kopf logger.
        reason (kopf.Reason): Kopf action reason.
        kwargs (dict): All options given by the kopf.

    """
    task = asyncio.create_task(
        reconcile_resource(
            body=body,
            logger=logger,
            reason=reason,
        ),
        name=f"{body['metadata']['namespace']}/"
        f"{body['metadata']['name']}"
        f"_{reason}",
    )
    try:
        await task
    except asyncio.exceptions.CancelledError:
        logger.info(f"Service {reason} handler has been cancelled.")
        # We expect that CancelledError is raised when the termination of
        # the resource is requested. Therefore, we ignore it here to let
        # the `delete` handler does its job.
        pass


@kopf.on.delete(CRDS.service_plural)
@error_status(resource_status=YaookServiceStatus)
@logging.unregister(settings=SETTINGS.service_ctrl)
async def on_delete(
    body: kopf.Body,
    logger: kopf.Logger,
    reason: kopf.Reason,
    **kwargs,  # Decorators expect it here.
):
    """Handle service resource deletion.

    Args:
        body (kopf.Body): Resource body.
        logger (kopf.Logger): Kopf logger.
        reason (kopf.Reason): Kopf action reason.
        kwargs (dict): All options given by the kopf.

    """
    kubernetes = Kubernetes(logger=logger)
    service = YaookServiceResource.from_kopf(body=body, kubernetes=kubernetes)
    logger.info(
        f"{reason} of service {service.name} in {service.namespace} namespace started"
    )

    service.set_status(
        status=YaookServiceStatus(ready=False, phase=ResourcePhase.TERMINATING).dict(
            exclude_none=True,
        ),
        ignore_errors=True,
    )
    # Remove network namespace
    if os.environ.get("ENVIRONMENT") == "kubernetes":
        namespace_name = "-".join((service.namespace, service.name))
        try:
            await delete_network_namespace(namespace_name)
        except ProviderPermanentError:
            pass

    cluster_path = (
        SETTINGS.service_ctrl.path / service.cluster_namespace / service.cluster_name
    )

    # Remove git submodule
    repo_root = git.Repo()
    try:
        repo_root.submodule(f"{cluster_path}/managed-k8s").remove()
        repo_root.git.restore(cluster_path, staged=True)
    except (NoSectionError, GitCommandError, ValueError):
        pass
    shutil.rmtree(f".git/modules/{cluster_path}", ignore_errors=True)

    # Remove cluster files
    shutil.rmtree(cluster_path, ignore_errors=True)

    logger.info(
        f"{reason} of service {service.name} in {service.namespace} namespace finished"
    )

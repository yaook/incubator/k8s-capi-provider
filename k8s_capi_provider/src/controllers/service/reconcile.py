"""This module defines helper functions for service controller handlers."""
import os
from ipaddress import IPv4Network
from typing import Optional, Union

import kopf

from ...data.base import ResourceAction, ResourcePhase
from ...data.service import YaookServiceResource, YaookServiceStage
from ...exceptions import ProviderTemporaryError, ProviderTemporaryErrorReason
from ...models.yaookservice import Status as YaookServiceStatus
from ...settings import ProviderSettings, get_settings
from ...utils.kubernetes import Kubernetes
from ...utils.network import create_network_namespace, get_all_local_networks
from ...utils.reconcile import execute

SETTINGS: ProviderSettings = get_settings()


async def reconcile_resource(
    body: kopf.Body,
    logger: kopf.Logger,
    reason: Union[kopf.Reason, ResourceAction],
) -> None:
    """Reconcile service resource.

    Args:
        body (kopf.Body): Resource body.
        logger (kopf.Logger): Kopf logger.
        reason (Union[kopf.Reason, ResourceAction]): Action reason.

    """
    kubernetes = Kubernetes(logger=logger)
    service = YaookServiceResource.from_kopf(body=body, kubernetes=kubernetes)
    logger.info(
        f"{reason} of service {service.name} in {service.namespace} namespace started"
    )

    if reason == ResourceAction.UPDATE and service.is_reconciling():
        raise ProviderTemporaryError(
            reason=ProviderTemporaryErrorReason.WAITING_FOR_RECONCILE.name,
            message=ProviderTemporaryErrorReason.WAITING_FOR_RECONCILE.value,
            delay=SETTINGS.service_ctrl.reconcile_wait_for,
        )

    deps = service.get_dependencies()
    service.set_owner(owner_body=deps.cluster.body, owner_controller=False)

    if not deps.cluster.is_running():
        if reason == ResourceAction.CREATE:
            # Initialize the PENDING phase only when the resource is handled first time.
            service.set_status(
                status=YaookServiceStatus(
                    ready=False, phase=ResourcePhase.PENDING
                ).dict(exclude_none=True)
            )
        raise ProviderTemporaryError(
            reason=ProviderTemporaryErrorReason.WAITING_FOR_RESOURCE.name,
            message=f"YaookCluster: {deps.cluster.name} is not ready yet.",
            delay=SETTINGS.service_ctrl.dependencies_wait_for,
        )

    if os.environ.get("ENVIRONMENT") == "kubernetes":
        namespace_name = "-".join((service.namespace, service.name))
        ip_netns = f"ip netns exec {namespace_name} "
        if reason in (
            ResourceAction.CREATE,
            ResourceAction.RESUME,
        ) and not os.path.exists(f"/var/run/netns/{namespace_name}"):
            wg_ip_cidr = IPv4Network(deps.cluster.obj.spec.wireguard.ip_cidr)
            tf_subnet_cidr = IPv4Network(deps.cluster.obj.spec.terraform.subnet_cidr)
            # In the worst case scenario (when each user/cluster pick 10.0.0.0/8 and
            # 172.16.0.0/12 networks) we should be able to create 2^14 - 1 different
            # network namespaces from 192.168.0.0/16 (default subnet_mask is /30)
            await create_network_namespace(
                namespace_name,
                [wg_ip_cidr, tf_subnet_cidr] + get_all_local_networks(),
            )
    else:
        ip_netns = ""

    for item in (
        YaookServiceStage.init,
        YaookServiceStage.stage4,
        YaookServiceStage.stage5,
    ):
        stage = YaookServiceStage(
            item
        )  # We want to be exact in for loop, hence we instantiate stage again.

        cmd_base_path = SETTINGS.service_ctrl.path_managed
        cmd_namespace: Optional[str] = ip_netns

        if stage in (YaookServiceStage.init,):
            cmd_base_path = SETTINGS.yaook_git.directory.absolute()
            cmd_namespace = None

        if status_before := stage.status_before(ResourceAction(reason)):
            service.set_status(status=status_before.dict(exclude_none=True))

        await execute(
            cmd=stage.path(cmd_base_path, namespace=cmd_namespace).split(),
            cluster_name=service.cluster_name,
            cluster_namespace=service.cluster_namespace,
            config=service.get_config(),
            action=ResourceAction(reason),
            kubernetes=kubernetes,
            settings_ctrl=SETTINGS.service_ctrl,
            lcm=deps.cluster.get_lcm(),  # Default values are needed for a smooth run.
            managed_provider_identity=deps.cluster.managed_provider_identity,
        )

        if status_after := stage.status_after(ResourceAction(reason)):
            service.set_status(status=status_after.dict(exclude_none=True))

    logger.info(
        f"{reason} of service {service.name} in {service.namespace} namespace finished"
    )

"""Module contains GPG stage and its dependencies."""

from pathlib import Path
from typing import NamedTuple

import gnupg

from ..exceptions import ProviderPermanentError, ProviderPermanentErrorReason
from ..settings import GPG
from ..utils.kubernetes import Kubernetes
from ..utils.state import ProviderState

GPG_ENCODING = "utf-8"


class PGPKey(NamedTuple):
    """PGP key resource definition."""

    name: str
    email: str
    fingerprint: str


class StageGPG(object):
    """GPG stage class.

    Yaook/k8s uses passwordstore to store e.g. WireGuard key for gateways
    Passwordstore wants to be accessible by operator and also by user.
    Therefore, the GPG key for the provider is created. The GPG public part
    of user's key is inserted and used together with provider's key.
    This ensures that the provider and also user will be able to decrypt
    the passwordstore.

    """

    def __init__(
        self,
        cluster_name: str,
        cluster_namespace: str,
        kubernetes: Kubernetes,
        settings: GPG,
        cluster_path: Path,
        adopt_state: bool,
    ):
        self.cluster_name = cluster_name
        self.cluster_namespace = cluster_namespace
        self.kubernetes = kubernetes
        self.settings = settings
        self.cluster_path = cluster_path
        self.adopt_state = adopt_state

        self.gpg = gnupg.GPG()
        self.gpg.encoding = GPG_ENCODING

        self.name_real = "-".join((self.cluster_name, self.settings.name))
        self.name_email = "-".join((self.cluster_name, self.settings.email))
        self.name = "-".join((self.cluster_name, self.settings.state.name))
        self.path = self.cluster_path / self.settings.state.path

    @staticmethod
    def find_gpg_key(gpg: gnupg.GPG, name: str):
        """Find GPG key based on given name.

        Args:
            gpg (gnupg.GPG): GPG client
            name (str): Name of the GPG key to be found.

        Returns:
            GPG key.

        """
        keys = gpg.list_keys()
        for key in keys:
            if any([name in uid for uid in key["uids"]]):
                return key

    def __enter__(self):
        """Load gpg state from secret."""
        self.state = ProviderState(
            k8s=self.kubernetes,
            name=self.name,
            namespace=self.cluster_namespace,
            path=self.path,
            adopt_state=self.adopt_state,
        )
        self.state.load()
        return self

    def __exit__(self, *exc):
        """Update gpg state secret."""
        self.state.update()

    def get_provider_key(self) -> PGPKey:
        """Get provider GPG key.

        Try to restore GPG key from secret. If GPG key does not exist,
        create GPG keypair and write its private part to the secret.

        Returns:
            PGPKey: Provider PGP key.

        """
        state = self.state

        state.path.mkdir(parents=True, exist_ok=True, mode=0o700)
        with open(state.path / self.settings.agent_conf_file, "w") as agent_conf_f:
            agent_conf_f.write(self.settings.agent_conf)
        self.gpg.gnupghome = state.path
        key = self.find_gpg_key(self.gpg, self.name_real)

        if not key:
            self.gpg.gen_key(
                self.gpg.gen_key_input(
                    name_real=self.name_real,
                    name_email=self.name_email,
                    no_protection=True,
                )
            )
            key = self.find_gpg_key(self.gpg, self.name_real)

        return PGPKey(
            name=self.name_real, email=self.name_email, fingerprint=key["fingerprint"]
        )

    def register_additional_gpg(self, additional_users):
        """Register additional GPG keys.

        Returns:
            tuple(PGPKey, str): Provider PGP key, GPG extra options
        """
        for user in additional_users:
            key = self.gpg.import_keys(user["pub_key"])
            self.gpg.trust_keys(key.fingerprints, "TRUST_ULTIMATE")

    def check_injected_identity(self):
        """Check injected GPG identity assets.

        Raises:
            ProviderPermanentError: When the provider identity related files
                or variables did not exist.

        """
        if not self.state.path.exists():
            raise ProviderPermanentError(
                reason=ProviderPermanentErrorReason.K8S_CAPI_PROVIDER_ERROR.name,
                message=f"Create provider identity flag is set to False."
                " Provider should use injected GPG home directory, which"
                f" was not found in {self.state.path}.",
            )

    def get_environment(self) -> dict:
        """Get GPG environment variables.

        Returns:
            dict: GPG environment variables
        """
        return {"PASSWORD_STORE_GPG_OPTS": f"--homedir {self.path.absolute()}"}

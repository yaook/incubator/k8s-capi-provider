"""Module contains ansible stage and its dependencies."""

from pathlib import Path

from ..settings import Ansible


class StageAnsible(object):
    """Ansible stage class."""

    def __init__(
        self,
        settings: Ansible,
        cluster_path: Path,
        provider_ssh_key: Path,
        duplicate_yaml_dict_key: str = "ignore",
        deprecation_warnings: str = "False",
    ):
        self.settings = settings
        self.cluster_path = cluster_path
        self.provider_ssh_key_path = self.cluster_path / provider_ssh_key
        self.kubeconfig_path = self.cluster_path / self.settings.kubeconfig
        self.collections_path = self.cluster_path / self.settings.collections
        self.duplicate_yaml_dict_key = duplicate_yaml_dict_key
        self.deprecation_warnings = deprecation_warnings

    def get_ssh_options(self) -> str:
        """Compose and return ansible SSH options.

        Returns:
            str: SSH options
        """
        return (
            f"-e ansible_ssh_private_key_file={self.provider_ssh_key_path.absolute()}"
            f" -e ansible_ssh_common_args=-oIdentitiesOnly=yes"
        )

    def __enter__(self):
        """Enter ansible stage context.

        Ansible state is not managed by :class:`StageAnsible` context. Ansible state
        should be managed within :func:`reconcile.state_base` outside the main
        :func:`reconcile.execute`.
        """
        return self

    def __exit__(self, *exc):
        """Exit ansible stage context.

        Ansible state is not managed by :class:`StageAnsible` context. Ansible state
        should be managed within :func:`reconcile.state_base` outside the main
        :func:`reconcile.execute`.
        """
        pass

    def get_environment(self) -> dict:
        """Get ansible environment variables.

        Returns:
            dict: Ansible environment variables
        """
        return {
            "AFLAGS": self.get_ssh_options(),
            # Ansible's community.kubernetes.k8s ignores KUBECONFIG env. variable
            # when executed from container. Therefore, K8S_AUTH_KUBECONFIG env.
            # variable should be configured.
            "K8S_AUTH_KUBECONFIG": str(self.kubeconfig_path.absolute()),
            # A race condition may occur when multiple clusters are provisioned
            # in parallel using ansible collections. Ansible failed when downloads
            # the same collection in parallel to the default directory with the
            # following error:
            #   Unexpected Exception, this is probably a bug:
            #   [Errno 39] Directory not empty
            # The default collections path is overwritten per workload cluster.
            # It causes that the collections are downloaded for every new
            # workload cluster separately. It is worth for a parallel run.
            "ANSIBLE_COLLECTIONS_PATHS": str(self.collections_path.absolute()),
            # FIXME: Warning messages from underlying yaook/k8s scripts
            #  are printed to the STDERR. Therefore, we should redirect
            #  these warnings to the warning logs and avoid log them as
            #  error in our k8s-capi-provider. We suppressed some known
            #  warnings. Currently, it is not possible to better distinguish
            #  warning from errors produced by yaook/k8s, as the ansible
            #  warnings are often multiline.
            #  !Fix all underlying warnings in yaaok/k8s and remove this
            #  ugly workaround!
            "ANSIBLE_DEPRECATION_WARNINGS": self.deprecation_warnings,
            "ANSIBLE_DUPLICATE_YAML_DICT_KEY": self.duplicate_yaml_dict_key,
        }

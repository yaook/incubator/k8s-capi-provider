"""Module contains config stage and its dependencies."""
import os
from pathlib import Path
from typing import MutableMapping

import toml
from mergedeep import merge

from ..settings import ConfigToml, YaookGit
from ..utils.common import encode_to_64
from ..utils.kubernetes import Kubernetes
from ..utils.state import ProviderState


class StageConfig(object):
    """Config stage class."""

    def __init__(
        self,
        cluster_name: str,
        cluster_namespace: str,
        kubernetes: Kubernetes,
        settings: ConfigToml,
        settings_git: YaookGit,
        cluster_path: Path,
        adopt_state: bool,
    ):
        self.cluster_name = cluster_name
        self.cluster_namespace = cluster_namespace
        self.kubernetes = kubernetes
        self.settings = settings
        self.cluster_path = cluster_path
        self.adopt_state = adopt_state

        self.config_path = cluster_path / self.settings.path
        self.config_template_path = settings_git.template_path

        self.name = "-".join((self.cluster_name, self.settings.state.name))
        self.path = self.cluster_path / self.settings.state.path

    def __enter__(self):
        """Load config state from secret."""
        self.state = ProviderState(
            k8s=self.kubernetes,
            name=self.name,
            namespace=self.cluster_namespace,
            path=self.path,
            adopt_state=self.adopt_state,
        )
        self.state.load()
        return self

    def __exit__(self, *exc):
        """Update config state secret."""
        self.state.update()

    @staticmethod
    def inject_fixes(config: MutableMapping):
        """Inject config variables needed for non failed run or integration.

        DO NOT USE IN PRODUCTION!

        This function injects below dev/integration dependencies:
        - Inject LBAAS shared secret.

            Takes given cluster's config and evaluates if the LBAAS shared secret
            is already there. If not, it generates one.

        - Disable Thanos if variable is not set.
            FIXME: Remove default value for use_thanos variable once yaook/k8s#467
             is resolved. See: yaook/incubator/k8s-capi-provider#54 for details.

        Args:
            config (MutableMapping): Cluster configuration which will be applied.

        """
        # 1. Inject LBAAS shared secret
        if "ch-k8s-lbaas" not in config:
            config["ch-k8s-lbaas"] = {}

        if config["ch-k8s-lbaas"].get("shared_secret") in (None, "...", ""):
            config["ch-k8s-lbaas"]["shared_secret"] = encode_to_64(os.urandom(16))

        # 2. Disable Thanos if variable is not set
        if "k8s-service-layer" not in config:
            config["k8s-service-layer"] = {}

        if "prometheus" not in config["k8s-service-layer"]:
            config["k8s-service-layer"]["prometheus"] = {}

        if "use_thanos" not in config["k8s-service-layer"]["prometheus"]:
            config["k8s-service-layer"]["prometheus"]["use_thanos"] = False

    def merge(self, config: dict):
        """Merge config files.

        Merge toml configs from multiple sources.
        As we orchestrate workload clusters by multiple controllers,
        the config file processing is distributed across them.
        E.g. the service layer config file variables located in the
        service resource are not available in cluster controller,
        because only service controller could process service resource.
        Therefore, we should set template variables for the service layer
        in cluster controller, just for passing the yaook/k8s
        mandatory variables checks. Then, in the service controller,
        the cluster variables are loaded from the state (config-secret)
        and merged with up-to-date service layer variables from service
        resource.
          Used merge strategy diagram:
                      `config loaded from resources`
                                    ↓
                         `config loaded from state`
                                    ↓
                            `config template`

        Args:
            config (dict): Config loaded from resources.

        """
        self.config_path.parent.mkdir(parents=True, exist_ok=True)
        config_template = toml.load(self.config_template_path)
        config_state = toml.load(self.config_path) if self.config_path.exists() else {}

        config_template_state = merge(config_template, config_state)
        config_merged = merge(config_template_state, config)

        self.inject_fixes(config_merged)

        # Dump merged config to the config file.
        with open(self.config_path, "w") as fd:
            toml.dump(config_merged, fd)

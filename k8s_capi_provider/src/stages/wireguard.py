"""Module contains wireguard stage and its dependencies."""

from pathlib import Path
from typing import Optional, Tuple

from ..exceptions import ProviderPermanentError, ProviderPermanentErrorReason
from ..settings import Wireguard
from ..utils.common import exec_cmd
from ..utils.kubernetes import Kubernetes
from ..utils.state import ProviderState


async def generate_wg_keypair(private_key: Optional[str] = None) -> Tuple[str, str]:
    """Generate wireguard keypair.

    If private key is given, public key is generated from that key

    Args:
        private_key (str, optional): Private wireguard key

    Returns:
        tuple(str, str): Wireguard keypair
    """
    if private_key is None:
        private_key_encoded, _ = await exec_cmd(["wg", "genkey"], output=True)
        private_key = private_key_encoded.decode().strip()

    public_key_encoded, _ = await exec_cmd(
        ["wg", "pubkey"], cmd_input=private_key.encode(), output=True
    )

    return private_key, public_key_encoded.decode().strip()


class StageWireguard(object):
    """Wireguard stage class.

    WireGuard VPN tool is used as a default option for communication
    with hosts. Provider creates its own WireGuard private key and store
    it in secret. Then the provider is able to access hosts.

    """

    def __init__(
        self,
        cluster_name: str,
        cluster_namespace: str,
        kubernetes: Kubernetes,
        settings: Wireguard,
        cluster_path: Path,
        wg_user: str,
        adopt_state: bool,
    ):
        self.cluster_name = cluster_name
        self.cluster_namespace = cluster_namespace
        self.kubernetes = kubernetes
        self.settings = settings
        self.cluster_path = cluster_path
        self.wg_user = wg_user
        self.adopt_state = adopt_state

        self.provider_user = "-".join((self.cluster_name, self.settings.email))
        self.name = "-".join((self.cluster_name, self.settings.state.name))
        self.path = self.cluster_path / self.settings.state.path

    def __enter__(self):
        """Load wireguard state from secret."""
        self.state = ProviderState(
            k8s=self.kubernetes,
            name=self.name,
            namespace=self.cluster_namespace,
            path=self.path,
            adopt_state=self.adopt_state,
        )
        self.state.load()
        return self

    def __exit__(self, *exc):
        """Update wireguard state secret."""
        self.state.update()

    async def get_provider_key(self) -> Tuple[str, str]:
        """Get provider wireguard public private keypair.

        If wireguard state path (provider private key) does not exist,
        create wireguard key and write its private part to the file.

        Returns:
            tuple(str, str): Provider wireguard public private keypair
        """
        state = self.state
        if not state.path.exists():
            state.path.parent.mkdir(parents=True, exist_ok=True)

            private_key, public_key = await generate_wg_keypair()
            with open(state.path, "w") as fd:
                fd.write(private_key)

            return private_key, public_key

        with open(state.path) as fd:
            return await generate_wg_keypair(private_key=fd.read())

    def check_injected_identity(self):
        """Check injected wireguard identity assets.

        Raises:
            ProviderPermanentError: When the provider identity related files
                or variables did not exist.

        """
        if not self.state.path.exists():
            raise ProviderPermanentError(
                reason=ProviderPermanentErrorReason.K8S_CAPI_PROVIDER_ERROR.name,
                message=f"Create provider identity flag is set to False."
                " Provider should use injected wireguard private key, which"
                f" was not found in {self.state.path}.",
            )

        if not self.wg_user:
            raise ProviderPermanentError(
                reason=ProviderPermanentErrorReason.K8S_CAPI_PROVIDER_ERROR.name,
                message="Create provider identity flag is set to False."
                " Provider should use injected wireguard user, which name"
                " is not defined by variable `wg_user`.",
            )

    async def get_environment(self, managed_provider_identity: bool = True) -> dict:
        """Get wireguard environment variables.

        Args:
            managed_provider_identity (bool, optional):  True if the wg
                identity will be created by k8s-capi-provider. The provider
                identity should not be created by k8s-capi-provider when
                the cluster was injected (by `yaookctl inject`) from yaook/k8s LCM.
                Then, the origin yaook/k8s wg identity (key) is
                used as a provider one and should not be created again.
                Defaults to True.

        Returns:
            dict: wireguard environment variables
        """
        wg_private, wg_public = await self.get_provider_key()

        return {
            "wg_private_key": wg_private,
            "wg_public": wg_public,
            "wg_user": self.provider_user
            if managed_provider_identity
            else self.wg_user,
        }

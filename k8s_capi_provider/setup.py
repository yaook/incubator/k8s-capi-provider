"""A setuptools based setup module.

https://packaging.python.org/guides/distributing-packages-using-setuptools/
"""

import pathlib

from setuptools import find_packages, setup

here = pathlib.Path(__file__).parent.parent.resolve()

# Get the long description from the README file
long_description = (here / "README.md").read_text(encoding="utf-8")

install_requires = [
    "kopf~=1.35",
    "loguru",
    "pydantic",
    "toml~=0.10.0",
    "gitpython~=3.1",
    "python-gnupg~=0.4",
    "ansible~=5.9.0",
    "kubernetes",
    "openshift",
    "kubernetes-validate",
    "python-openstackclient",
    "netifaces~=0.11",
    "mergedeep",
]

setup(
    name="k8s-capi-provider",
    description="k8s-capi-provider is a lifecycle-management "
    "tool for kubernetes cluster",
    long_description=long_description,
    long_description_content_type="text/markdown",
    version="0.1.0",
    license="Apache License 2.0",
    author="Yaook authors",
    url="https://gitlab.com/yaook/incubator/k8s-capi-provider",
    python_requires=">=3.8",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "Topic :: Utilities",
        "License :: OSI Approved :: Apache Software License",
        "Programming Language :: Python :: 3.8",
        "Operating System :: POSIX :: Linux",
    ],
    keywords="yaook, k8s, capi, provider",
    packages=find_packages(),
    install_requires=install_requires,
    extras_require={
        "dev": [
            "mypy",
            "mock",
            "black",
            "flake8",
            "pylint",
            "pre-commit",
            "pytest",
            "pytest-cov",
            "pytest-kind~=21.1",
            "pytest-mock",
            "pytest-repeat",
            "pytest-asyncio",
        ]
    },
)
